package com.bca.bit.bromo.config;

import com.bca.bit.bromo.mobile.service.DeleteService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@EnableAsync
public class SchedulerConfig {
    @Bean
    public DeleteService deleteService(){
        return new DeleteService();
    }
}
