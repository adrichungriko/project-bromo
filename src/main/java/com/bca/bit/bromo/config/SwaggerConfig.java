package com.bca.bit.bromo.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private ArrayList<ResponseMessage> responseMessageBuilders = new ArrayList<>();

    @Bean
    public Docket apiDocket() {
        // Adding Header
        java.util.List<Parameter> aParameters = new ArrayList<>();
        ParameterBuilder aParameterBuilder = new ParameterBuilder();

        aParameterBuilder.name("Content-Type")
                .description("Content Type")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue(MediaType.APPLICATION_JSON.toString())
                .required(false).build();
        aParameters.add(aParameterBuilder.build());

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, getResponseMessageBuilders())
                .globalResponseMessage(RequestMethod.POST, getResponseMessageBuilders())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.bca.bit.bromo.mobile"))
                .paths(PathSelectors.any()).build()
                .globalOperationParameters(aParameters).enable(true);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("BROMO API")
                .description("Bromo Backend API Services")
                .version("v1.0")
                .build();
    }

    private ArrayList<ResponseMessage> getResponseMessageBuilders() {
        // 200 global success message has not work yet
        responseMessageBuilders
                .add(new ResponseMessageBuilder().code(200).message("200 global success message").build());
        responseMessageBuilders.add(new ResponseMessageBuilder().code(404).message("404 global error message").build());
        responseMessageBuilders.add(new ResponseMessageBuilder().code(403).message("403 global error message").build());

        return responseMessageBuilders;
    }

}