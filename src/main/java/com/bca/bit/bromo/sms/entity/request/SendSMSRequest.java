package com.bca.bit.bromo.sms.entity.request;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class SendSMSRequest {
    @JsonProperty("Recipient")
    private String recipient = "";
    @JsonProperty("Division")
    private String division = "";
    @JsonProperty("TemplateID")
    private String templateID = "";
    @JsonProperty("Data")
    private String data = "";
    @JsonProperty("UseTemplate")
    private String useTemplate = "";
    @JsonProperty("TemplateParameters")
    private List<String> templateParameters = new ArrayList<String>();

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getDivision() {
        return division;
    }

    public void setTemplateID(String templateID) {
        this.templateID = templateID;
    }

    public String getTemplateID() {
        return templateID;
    }


    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setUseTemplate(String useTemplate) {
        this.useTemplate = useTemplate;
    }

    public String getUseTemplate() {
        return useTemplate;
    }

    public void setTemplateParameters(List<String> templateParameters) {
        this.templateParameters = templateParameters;
    }

    public List<String> getTemplateParameters() {
        return templateParameters;
    }
}
