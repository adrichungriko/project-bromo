package com.bca.bit.bromo.sms.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

public class OutputSchema {
    @JsonProperty(value = "ReferenceNo")
    private String referenceNo;
    @JsonProperty(value = "SendDate")
    private String sendDate;
    @JsonProperty(value = "Type")
    private String type;
    @JsonProperty(value = "OperationType")
    private String operationType;

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getOperationType() {
        return operationType;
    }
}

