package com.bca.bit.bromo.sms.entity;

import com.fasterxml.jackson.annotation.JsonProperty;


public class SMSResponse {
    @JsonProperty("ErrorSchema")
    private ErrorSchema errorSchema;
    @JsonProperty("OutputSchema")
    private OutputSchema outputSchema;
    public void setErrorSchema(ErrorSchema errorSchema) {
        this.errorSchema = errorSchema;
    }

    public ErrorSchema getErrorSchema() {
        return errorSchema;
    }

    public void setOutputSchema(OutputSchema outputSchema) {
        this.outputSchema = outputSchema;
    }

    public OutputSchema getOutputSchema() {
        return outputSchema;
    }
}

