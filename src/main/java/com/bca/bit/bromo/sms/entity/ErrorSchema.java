package com.bca.bit.bromo.sms.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorSchema {
    @JsonProperty(value = "ErrorCode", required = true)
    private String errorCode;
    @JsonProperty(value = "ErrorMessage", required = true)
    private ErrorMessage errorMessage;


    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorMessage(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }
}

