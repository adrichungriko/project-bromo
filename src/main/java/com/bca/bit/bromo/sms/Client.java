package com.bca.bit.bromo.sms;

import com.bca.bit.bromo.sms.entity.SMSResponse;
import com.bca.bit.bromo.sms.entity.request.SendSMSRequest;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.xml.ws.Response;
import java.net.URI;

@Service
public class Client {
    private static Logger logger = Logger.getLogger("BCASMSClient");
    private String serverUrl = "http://10.20.200.140:9405/bcasms";
    private String serverReadTimeout = "3000";
    private String serverConnectTimeout = "3000";

    public SMSResponse sendSMS(String hashCode, String clientId, String referenceNumber, String operationType, SendSMSRequest sendSMSRequest, Logger logger) throws Exception {
        long start = System.currentTimeMillis();

        logger.info(hashCode + "Start REST BCASMS_sendSMS Client");
        SMSResponse sendSMSResponse = new SMSResponse();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serverUrl).path("/" + referenceNumber).path("/" + operationType);

        try {
            ObjectMapper mapper = new ObjectMapper();
            String inputREST = "";
            String outputREST = "";
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("ClientID", clientId);
            logger.debug(hashCode + "Checking clientID: " + clientId);
            headers.set("HashCode", hashCode);
            HttpEntity<SendSMSRequest> request = new HttpEntity<>(sendSMSRequest, headers);
            Gson gson = new Gson();

            ResponseEntity<String> responseEntity = restTemplate.postForEntity(builder.toUriString(), sendSMSRequest, String.class);

            logger.debug(hashCode + "Checking response: " + responseEntity.getBody());

            sendSMSResponse = gson.fromJson(responseEntity.getBody(), SMSResponse.class);
        }catch (Exception ex){
            logger.error(hashCode + "Error: " + ex.getMessage());
        }

        return sendSMSResponse;
    }
}
