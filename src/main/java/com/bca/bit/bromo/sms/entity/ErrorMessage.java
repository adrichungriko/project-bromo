package com.bca.bit.bromo.sms.entity;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ErrorMessage {
    @JsonProperty(value = "Indonesian", required = true)
    private String indonesian;
    @JsonProperty(value = "English", required = true)
    private String english;

    public void setIndonesian(String indonesian) {
        this.indonesian = indonesian;
    }

    public String getIndonesian() {
        return indonesian;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getEnglish() {
        return english;
    }
}
