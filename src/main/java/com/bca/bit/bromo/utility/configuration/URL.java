package com.bca.bit.bromo.utility.configuration;

import lombok.Data;
import org.springframework.stereotype.Service;

@Service
@Data
public class URL {
    private String FintechURL = "http://10.20.215.18:9003/fintech";
    private String coreBankURL = "http://10.20.215.18:9003/corebank";
}
