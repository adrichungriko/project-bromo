package com.bca.bit.bromo.utility.configuration;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class HashCode {


    private String numbers = "0123456789";
    private Random random = new Random();

    public String getHashCode(){
        String hashCode = "";
        int length = 6;
        char [] result = new char[length];
        for(int i = 0; i < length; i++){
            result[i] = numbers.charAt(random.nextInt(numbers.length()));
        }

        hashCode = new String(result);

        hashCode = "(" + hashCode + ")";

        return hashCode;
    }
}
