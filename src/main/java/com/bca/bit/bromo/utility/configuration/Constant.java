package com.bca.bit.bromo.utility.configuration;

import lombok.Data;

@Data
public class Constant {
    private String OVOID = "1000";
    private String DANAID = "2000";
    private String SAKUKUID = "3000";
    private String GOPAYID = "4000";
    private String BROMOAPP = "bromo_app";
    private String URL_MERCHANT_RECOMMENDATION = "http://10.20.215.18:9010/promo_recommendation";
}
