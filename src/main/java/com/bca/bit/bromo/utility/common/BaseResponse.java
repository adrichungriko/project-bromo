package com.bca.bit.bromo.utility.common;

import com.bca.bit.bromo.utility.message.MessageMapEnglish;
import com.bca.bit.bromo.utility.message.MessageMapIndonesian;
import lombok.Data;

@Data
public class BaseResponse {
    private ErrorSchema error_schema;
    public BaseResponse() {
        super();
    }

    public BaseResponse(String returnCode) {
        String english = MessageMapEnglish.getMessage(returnCode);
        String indonesian = MessageMapIndonesian.getMessage(returnCode);
        error_schema = new ErrorSchema(returnCode,indonesian,english);
    }

    public BaseResponse(String returnCode, String message){
        error_schema = new ErrorSchema(returnCode, message, message);
    }

    @Data
    public static class ErrorSchema{
        public ErrorSchema(String error_code, String indonesian, String english){
            this.error_code = error_code;
            error_message = new ErrorMessage(indonesian,english);
        }

        private String error_code;
        private ErrorMessage error_message;

        @Data
        public static class ErrorMessage{
            public ErrorMessage(String indonesian, String english){
                this.english = english;
                this.indonesian = indonesian;
            }
            private String english;
            private String indonesian;
        }
    }
}
