package com.bca.bit.bromo.utility.generate;


import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.sun.javafx.binding.StringFormatter;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Base64;
import java.util.UUID;

@Service
public class QRGenerator {
    private static final String QRCodeImagePath = "./QRRudy.png";

    public String generateQRCodeImage(String text){
        String output = "";
        String merchant_id = "04ef5962-bcd6-448c-a6c3-c935afc1dbe5";
        String promo_id = "cac366c7-bf91-42e6-9aab-70b1bb122ab4";
        String customer_id = "";
        String random = UUID.randomUUID().toString();
        String amount = "60000";
        		
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new Gson();
        jsonObject.setCustomer_id(customer_id);
        jsonObject.setRandom(random);
        jsonObject.setAmount(amount);
        jsonObject.setMerchant_id(merchant_id);
        jsonObject.setPromo_id(promo_id);
        String test = gson.toJson(jsonObject);
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            output = Base64.getEncoder().encodeToString(test.getBytes());
            System.out.println("checking output: " + output);
            BitMatrix bitMatrix = qrCodeWriter.encode(output, BarcodeFormat.QR_CODE, 500, 500);
            Path path = FileSystems.getDefault().getPath(QRCodeImagePath);
            MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
        }catch (Exception ex){
            ex.getMessage();
        }
        return output;
    }

    public void decodeQRCodeString(String text){
        byte[] textbyte = Base64.getDecoder().decode(text);
        String output = new String(textbyte);
        System.out.println("Checking decode: " + output);
    }

    @Data
    public class JsonObject{
        private String merchant_id;
        private String promo_id;
        private String customer_id;
        private String random;
        private String amount;
    }
}
