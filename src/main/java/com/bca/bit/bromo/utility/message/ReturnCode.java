package com.bca.bit.bromo.utility.message;

import lombok.Data;

@Data
public class ReturnCode {
    public static final String BROMO_SERVICE_SUCCESS = "BIT-200";
    public static final String BROMO_BAD_INPUT = "BIT-400";
    public static final String BROMO_BAD_CREDS = "BIT-401";
    public static final String BROMO_DATA_NOT_FOUND = "BIT-410";
    public static final String BROMO_SERVICE_GENERAL_ERROR = "BIT-499";
    public static final String BROMO_WRONG_INPUT = "BIT-402";
    public static final String BROMO_MISSING_INPUT = "BIT-404";
    public static final String BROMO_USER_REGISTERED = "BIT-303";
    public static final String BROMO_TOP_UP_FAIL = "BIT-202";
    public static final String BROMO_ALREADY_LIKED = "BIT-201";
    public static final String BROMO_QR_USED = "BIT-203";
}
