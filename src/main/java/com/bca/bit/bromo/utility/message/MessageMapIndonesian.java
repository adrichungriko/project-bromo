package com.bca.bit.bromo.utility.message;

import lombok.Data;

import java.util.HashMap;

@Data
public class MessageMapIndonesian {
    private static HashMap<String, String> messageMap;
    public static final String LANGUAGE_CODE = "ID";

    static {
        messageMap = new HashMap<String, String>();
        messageMap.put(ReturnCode.BROMO_SERVICE_SUCCESS, "Berhasil");
        messageMap.put(ReturnCode.BROMO_BAD_INPUT, "Format input tidak sesuai");
        messageMap.put(ReturnCode.BROMO_BAD_CREDS, "Akses tidak dikenali");
        messageMap.put(ReturnCode.BROMO_DATA_NOT_FOUND, "Data tidak ditemukan");
        messageMap.put(ReturnCode.BROMO_SERVICE_GENERAL_ERROR, "Terjadi kesalahan dalam proses");
        messageMap.put(ReturnCode.BROMO_WRONG_INPUT, "Input Salah");
        messageMap.put(ReturnCode.BROMO_MISSING_INPUT, "Input tidak ada");
        messageMap.put(ReturnCode.BROMO_USER_REGISTERED, "User sudah Terdaftar");
        messageMap.put(ReturnCode.BROMO_TOP_UP_FAIL, "Top Up Gagal");
        messageMap.put(ReturnCode.BROMO_ALREADY_LIKED, "User sudah Like Promo ini");
        messageMap.put(ReturnCode.BROMO_QR_USED, "QR Sudah terpakai");
    }

    public static String getMessage(String messageCode){return messageMap.get(messageCode);}
}
