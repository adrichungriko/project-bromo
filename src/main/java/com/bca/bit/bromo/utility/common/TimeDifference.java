package com.bca.bit.bromo.utility.common;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.Log;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

@Service
public class TimeDifference {

    public boolean doTimeDifference(String logName, String code, Timestamp oldTime, long difference){
        Logger logger = Logger.getLogger(logName);
        Date date = new Date(System.currentTimeMillis());
        Timestamp currentTime = new Timestamp(date.getTime());
        long result = currentTime.getTime() - oldTime.getTime();
        boolean verify;
        if (result >= difference){
            verify = true;
        }else{
            verify = false;
        }
        return verify;
    }
}
