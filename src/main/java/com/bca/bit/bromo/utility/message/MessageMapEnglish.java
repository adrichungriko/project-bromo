package com.bca.bit.bromo.utility.message;

import lombok.Data;

import java.util.HashMap;

@Data
public class MessageMapEnglish {
    private static HashMap<String, String> messageMap;
    public static final String LANGUAGE_CODE = "EN";

    static {
        messageMap = new HashMap<String, String>();
        messageMap.put(ReturnCode.BROMO_SERVICE_SUCCESS, "Success");
        messageMap.put(ReturnCode.BROMO_BAD_INPUT, "Invalid input format");
        messageMap.put(ReturnCode.BROMO_BAD_CREDS, "Unauthorized access");
        messageMap.put(ReturnCode.BROMO_DATA_NOT_FOUND, "Data not found");
        messageMap.put(ReturnCode.BROMO_SERVICE_GENERAL_ERROR, "Error in processing service");
        messageMap.put(ReturnCode.BROMO_WRONG_INPUT, "Incorrect Input");
        messageMap.put(ReturnCode.BROMO_MISSING_INPUT, "Missing Input");
        messageMap.put(ReturnCode.BROMO_USER_REGISTERED, "User Already Registered");
        messageMap.put(ReturnCode.BROMO_TOP_UP_FAIL, "Top Up Fail");
        messageMap.put(ReturnCode.BROMO_ALREADY_LIKED, "User Already Liked the Promo");
        messageMap.put(ReturnCode.BROMO_QR_USED, "QR Used Already");
    }

    public static String getMessage(String messageCode){return messageMap.get(messageCode);}
}
