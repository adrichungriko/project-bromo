package com.bca.bit.bromo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class BromoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BromoApplication.class, args);
	}

}
