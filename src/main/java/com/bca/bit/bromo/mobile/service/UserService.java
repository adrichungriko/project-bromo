package com.bca.bit.bromo.mobile.service;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import com.bca.bit.bromo.mobile.entity.jpa.*;
import com.bca.bit.bromo.mobile.entity.model.UserInterestResponseModel;
import com.bca.bit.bromo.mobile.entity.request.InsertUserInterestRequest;
import com.bca.bit.bromo.mobile.entity.response.TrxPromoUsedResponse;
import com.bca.bit.bromo.mobile.repo.*;
import com.bca.bit.bromo.utility.configuration.Constant;
import com.bca.bit.bromo.utility.configuration.HashCode;
import org.apache.kafka.common.protocol.types.Field;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bca.bit.bromo.mobile.entity.response.InitPrepareHomeResponse;
import com.bca.bit.bromo.mobile.entity.response.InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MySlider;
import com.bca.bit.bromo.mobile.entity.response.InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard;
import com.bca.bit.bromo.mobile.entity.response.InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyWallet;
import com.bca.bit.bromo.mobile.entity.response.InquirySaldoResponse;
import com.bca.bit.bromo.mobile.entity.response.InquiryUserInterestResponse;
import com.bca.bit.bromo.mobile.entity.response.InsertUserInterestResponse;
import com.bca.bit.bromo.mobile.entity.response.InquirySaldoResponse.InquirySaldoOutputSchema.balance;
import com.bca.bit.bromo.utility.message.ReturnCode;

@Service
public class UserService {

	private static Logger logger = Logger.getLogger("UserService");

	@Autowired
	private InquirySaldoService inquirySaldoSvc;

	@Autowired
	private MerchantCategoryRepo merchantCategoryRepo;

	@Autowired
	private MerchantRepo merchantRepo;

	@Autowired
	private CustomerRepo customerRepo;

	@Autowired
	private MerchantPromoRepo merchantPromoRepo;

	@Autowired
	private CategoryRepo categoryRepo;

	@Autowired
	private TrxPromoUsedRepo trxPromoUsedRepo;

	@Autowired
	private HashCode hashCode;

	@Autowired
	private PromoRepo promoRepo;
	
	@Autowired
	private HomeService homeService;

	public InquiryUserInterestResponse inquiryUserInterest() {

		try {
			List<Category> listCategoryParent = categoryRepo.findByParentCategoryIsNull();
			List<UserInterestResponseModel> listInterest = new ArrayList<UserInterestResponseModel>();

			System.out.println("Category Parent " + listCategoryParent);

			if (listCategoryParent.size() > 0) {
				for (Category c : listCategoryParent) {
					UserInterestResponseModel userInterest = new UserInterestResponseModel(
							"category_" + c.getCategoryId() + ".png", c.getCategoryName(),
							Integer.toString(c.getCategoryId()));
					listInterest.add(userInterest);
				}

				return new InquiryUserInterestResponse(ReturnCode.BROMO_SERVICE_SUCCESS, listInterest);
			} else {
				return new InquiryUserInterestResponse(ReturnCode.BROMO_DATA_NOT_FOUND, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new InquiryUserInterestResponse(ReturnCode.BROMO_SERVICE_GENERAL_ERROR, null);
		}
	}

	public InitPrepareHomeResponse initPrepareHomeDummy(String custId) {
		InitPrepareHomeResponse initPrepHomeResp = new InitPrepareHomeResponse();
		InitPrepareHomeResponse.InitPrepareHomeOutputSchema outputSchema = new InitPrepareHomeResponse.InitPrepareHomeOutputSchema();
		InitPrepareHomeResponse.ErrorSchema errSchema = new InitPrepareHomeResponse.ErrorSchema();
		InitPrepareHomeResponse.ErrorSchema.ErrorMessage errMsg = new InitPrepareHomeResponse.ErrorSchema.ErrorMessage();

		List<MyWallet> listMyWallet = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyWallet>();
		List<MySlider> listMySlider = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MySlider>();
		List<MyViewCard> listMyViewCard = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard>();
		List<MyViewCard> listMyViewCard2 = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard>();

		try {

			MyWallet myWalletGopay = new MyWallet();
			myWalletGopay.setFintech_cust_name("Djaja Sudjaja");
			myWalletGopay.setBalance("20000");
			myWalletGopay.setFintech_logo_url("https://i.ibb.co/HHqNj4d/go-pay.png");
			myWalletGopay.setFintech_name("Gopay");
			myWalletGopay.setFintech_cust_id("085468272673");

			MyWallet myWalletOvo = new MyWallet();
			myWalletOvo.setFintech_cust_name("Djaja Sudjaja");
			myWalletOvo.setBalance("50000");
			myWalletOvo.setFintech_logo_url("https://i.ibb.co/z5kDVf6/OVO.png");
			myWalletOvo.setFintech_name("Ovo");
			myWalletOvo.setFintech_cust_id("085468272673");

			MyWallet myWalletLinkAja = new MyWallet();
			myWalletLinkAja.setFintech_cust_name("Djaja Sudjaja");
			myWalletLinkAja.setBalance("30500");
			myWalletLinkAja.setFintech_logo_url("https://i.ibb.co/ZzhWw57/link-aja.png");
			myWalletLinkAja.setFintech_name("Link Aja");
			myWalletLinkAja.setFintech_cust_id("085468272673");

			MyWallet myWalletSakuku = new MyWallet();
			myWalletSakuku.setBalance("90000");
			myWalletSakuku.setFintech_cust_name("Djaja Sudjaja");
			myWalletSakuku.setFintech_logo_url("https://i.ibb.co/9sDg0FW/logo-sakuku.png");
			myWalletSakuku.setFintech_name("Sakuku");
			myWalletSakuku.setFintech_cust_id("085468272673");

			listMyWallet.add(myWalletGopay);
			listMyWallet.add(myWalletOvo);
			listMyWallet.add(myWalletLinkAja);
			listMyWallet.add(myWalletSakuku);

			MySlider mySliderDana = new MySlider();
			mySliderDana.setPromo_id("012345");
			mySliderDana.setPromo_name("Cashback DANA 50%");
			mySliderDana.setSlider_image_url("https://i.ibb.co/zZdFPFQ/slider-dana.png");

			MySlider mySliderGopay = new MySlider();
			mySliderGopay.setPromo_id("66005");
			mySliderGopay.setPromo_name("Cashback GOPAY 20%");
			mySliderGopay.setSlider_image_url("https://i.ibb.co/sspc2dG/slider-gopay.png");

			MySlider mySliderOvo = new MySlider();
			mySliderOvo.setPromo_id("66007");
			mySliderOvo.setPromo_name("Cashback OVO 60%");
			mySliderOvo.setSlider_image_url("https://i.ibb.co/Px24nKW/slider-ovo.jpg");

			listMySlider.add(mySliderOvo);
			listMySlider.add(mySliderGopay);
			listMySlider.add(mySliderDana);

			// merchant F&B

			MyViewCard myViewCard1 = new MyViewCard();
			myViewCard1.setMerchant_id("00800129233");
			myViewCard1.setMerchant_logo_url("https://i.ibb.co/vzXN0zq/logo-bakmigm.png");
			myViewCard1.setMerchant_name("Bakmi GM");
			listMyViewCard.add(myViewCard1);

			MyViewCard myViewCard2 = new MyViewCard();
			myViewCard2.setMerchant_id("00800129234");
			myViewCard2.setMerchant_logo_url("https://i.ibb.co/r2wxGbC/logo-hokben.png");
			myViewCard2.setMerchant_name("HokBen");
			listMyViewCard.add(myViewCard2);

			MyViewCard myViewCard3 = new MyViewCard();
			myViewCard3.setMerchant_id("00800129235");
			myViewCard3.setMerchant_logo_url("https://i.ibb.co/cTj3tKD/logo-kfc.png");
			myViewCard3.setMerchant_name("KFC");
			listMyViewCard.add(myViewCard3);

			MyViewCard myViewCard4 = new MyViewCard();
			myViewCard4.setMerchant_id("00800129236");
			myViewCard4.setMerchant_logo_url("https://i.ibb.co/GJJgG9f/logo-solaria.jpg");
			myViewCard4.setMerchant_name("Solaria");
			listMyViewCard.add(myViewCard4);

			MyViewCard myViewCard5 = new MyViewCard();
			myViewCard5.setMerchant_id("00800129237");
			myViewCard5.setMerchant_logo_url("https://i.ibb.co/GMcbDbm/logo-mcd.jpg");
			myViewCard5.setMerchant_name("MC Donalds");
			listMyViewCard.add(myViewCard5);

			// merchant sport

			MyViewCard myViewCard21 = new MyViewCard();
			myViewCard21.setMerchant_id("10800129233");
			myViewCard21.setMerchant_logo_url("https://i.ibb.co/3BkhrRC/logo-adidas.png");
			myViewCard21.setMerchant_name("Adidas");
			listMyViewCard2.add(myViewCard21);

			MyViewCard myViewCard22 = new MyViewCard();
			myViewCard22.setMerchant_id("20800129233");
			myViewCard22.setMerchant_logo_url("https://i.ibb.co/Hh9ygtT/logo-eiger.png");
			myViewCard22.setMerchant_name("Eiger");
			listMyViewCard2.add(myViewCard22);

			MyViewCard myViewCard23 = new MyViewCard();
			myViewCard23.setMerchant_id("30800129233");
			myViewCard23.setMerchant_logo_url("https://i.ibb.co/jMJ07QR/logo-miniso.png");
			myViewCard23.setMerchant_name("Miniso");
			listMyViewCard2.add(myViewCard23);

			MyViewCard myViewCard24 = new MyViewCard();
			myViewCard24.setMerchant_id("40800129233");
			myViewCard24.setMerchant_logo_url("https://i.ibb.co/m5tGT8G/logo-consina.png");
			myViewCard24.setMerchant_name("Consina");
			listMyViewCard2.add(myViewCard24);

			MyViewCard myViewCard25 = new MyViewCard();
			myViewCard25.setMerchant_id("50800129233");
			myViewCard25.setMerchant_logo_url("https://i.ibb.co/FJDCMk5/logo-levis.jpg");
			myViewCard25.setMerchant_name("Levis");
			listMyViewCard2.add(myViewCard25);

			errMsg.setEnglish("Success");
			errMsg.setIndonesian("Sukses");
			errSchema.setError_code("BIT-200");
			errSchema.setError_message(errMsg);
			outputSchema.setMerchant_category_view_card("Food and Beverage");
			outputSchema.setMerchant_category_view_card_id("1");
			outputSchema.setMerchant_category_view_card_2("Fashion");
			outputSchema.setMerchant_category_view_card_2_id("8");
			outputSchema.setList_my_wallet(listMyWallet);
			outputSchema.setList_my_slider(listMySlider);
			outputSchema.setList_my_view_card(listMyViewCard);
			outputSchema.setList_my_view_card_2(listMyViewCard2);

		} catch (Exception e) {
			errMsg.setEnglish("Internal Server Error");
			errMsg.setIndonesian("Internal Server Error");
			errSchema.setError_code("BIT-500");
			errSchema.setError_message(errMsg);
			initPrepHomeResp.setOutput_schema(null);
		}

		initPrepHomeResp.setError_schema(errSchema);
		initPrepHomeResp.setOutput_schema(outputSchema);
		return initPrepHomeResp;
	}

	public List<MyViewCard> getTopMerchantByCategory(int categoryId, Customer cust) {

		List<MyViewCard> listViewCard = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard>();
		List<Merchant> merchantCat1 = categoryRepo.getMerchantInCategoryId(categoryId);

		List<Merchant> merchantCentralCat1 = merchantCat1.stream().filter(x -> {
			if (x.getMerchantCentralId().equals("") || x.getMerchantCentralId() == null) {
				return true;
			} else {
				return false;
			}
		}).limit(5).collect(Collectors.toList());
		System.out.println("merchant Cat 1 " + merchantCentralCat1);

		for (Merchant m : merchantCentralCat1) {

			List<MyViewCard.CategoryMerchant> listCategoryOfMerchant = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard.CategoryMerchant>();

			MyViewCard myViewCard = new MyViewCard();
			myViewCard.setMerchant_id(m.getMerchantId().toString());
			myViewCard.setMerchant_name(m.getMerchantName());
			myViewCard.setMerchant_logo_url(m.getMerchantImage());

			try {
				List<MerchantCategory> mc = (List<MerchantCategory>) merchantCategoryRepo
						.findByMerchantId(m.getMerchantId());

				for (MerchantCategory mc_item : mc) {
					MyViewCard.CategoryMerchant mcm = new InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard.CategoryMerchant();
					mcm.setCategory_id(mc_item.getCategoryId());

					Category cat = categoryRepo.findByCategoryId(mc_item.getCategoryId());

					mcm.setCategory_name(cat.getCategoryName());
					listCategoryOfMerchant.add(mcm);
				}

				myViewCard.setMerchant_category(listCategoryOfMerchant);
				listViewCard.add(myViewCard);
			} catch (Exception e) {
				e.printStackTrace();
				myViewCard.setMerchant_category(listCategoryOfMerchant);
			}
		}

		return listViewCard;
	}
		
	public InitPrepareHomeResponse initPrepareHome(String customerId) {

		InitPrepareHomeResponse initPrepHomeResp = new InitPrepareHomeResponse();
		InitPrepareHomeResponse.InitPrepareHomeOutputSchema outputSchema = new InitPrepareHomeResponse.InitPrepareHomeOutputSchema();
		InitPrepareHomeResponse.ErrorSchema.ErrorMessage errMsg = new InitPrepareHomeResponse.ErrorSchema.ErrorMessage();
		InitPrepareHomeResponse.ErrorSchema errSchema = new InitPrepareHomeResponse.ErrorSchema();

		try {

			List<MySlider> listMySlider = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MySlider>();
			List<MyViewCard> listViewCardRecommend = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard>();

			Customer cust = customerRepo.findByCustomerID(UUID.fromString(customerId));

			if (cust != null) {
				
				CompletableFuture<List<MyWallet>> listFutureWallet = homeService.getMyWallet(customerId);
				CompletableFuture<List<MyViewCard>> listFutureMerchantRecommendation = homeService.getTopMerchantByRecommendation("6", "106", customerId);
				CompletableFuture<List<MySlider>> listFutureSlider = homeService.getSlider();
				
				outputSchema.setList_my_wallet(listFutureWallet.get());

				
				System.out.println("My Slider " + listFutureSlider.get());
				outputSchema.setList_my_slider(listFutureSlider.get());

				if (cust.getListCategoryUserInterest().size() > 1) {
					Category category_1 = cust.getListCategoryUserInterest().get(0);
					outputSchema.setList_my_view_card(getTopMerchantByCategory(category_1.getCategoryId(), cust));
					outputSchema.setMerchant_category_view_card(category_1.getCategoryName());
					outputSchema.setMerchant_category_view_card_id(Integer.toString(category_1.getCategoryId()));
					outputSchema
							.setMerchant_category_view_card_picture("category_" + category_1.getCategoryId() + ".png");

					Category category_2 = cust.getListCategoryUserInterest().get(1);
					outputSchema.setList_my_view_card_2(getTopMerchantByCategory(category_2.getCategoryId(), cust));
					outputSchema.setMerchant_category_view_card_2_id(Integer.toString(category_2.getCategoryId()));
					outputSchema.setMerchant_category_view_card_2(category_2.getCategoryName());
					outputSchema.setMerchant_category_view_card_2_picture(
							"category_" + category_2.getCategoryId() + ".png");

				} else {
					// set default category
					Category category_1 = categoryRepo.findByCategoryId(1);
					outputSchema.setList_my_view_card(getTopMerchantByCategory(category_1.getCategoryId(), cust));
					outputSchema.setMerchant_category_view_card(category_1.getCategoryName());
					outputSchema.setMerchant_category_view_card_id(Integer.toString(category_1.getCategoryId()));
					outputSchema
							.setMerchant_category_view_card_picture("category_" + category_1.getCategoryId() + ".png");

					Category category_2 = categoryRepo.findByCategoryId(8);
					outputSchema.setList_my_view_card_2(getTopMerchantByCategory(category_2.getCategoryId(), cust));
					outputSchema.setMerchant_category_view_card_2_id(Integer.toString(category_2.getCategoryId()));
					outputSchema.setMerchant_category_view_card_2(category_2.getCategoryName());
					outputSchema.setMerchant_category_view_card_2_picture(
							"category_" + category_2.getCategoryId() + ".png");

				}

				/** TO DO INTEGRASI DATA SCIENCE **/
				outputSchema.setMerchant_category_view_card_recommend("Special For You");
				outputSchema.setMerchant_category_view_card_recommend_picture("category_recommend.png");
				//outputSchema.setList_my_view_card_recommend(listViewCardRecommend);
				outputSchema.setList_my_view_card_recommend(listFutureMerchantRecommendation.get());

				initPrepHomeResp.setError_schema(errSchema);
				initPrepHomeResp.setOutput_schema(outputSchema);

				errMsg.setEnglish("Success");
				errMsg.setIndonesian("Sukses");
				errSchema.setError_message(errMsg);
				errSchema.setError_code("BIT-200");

			} else {
				// customer id null
				errMsg.setEnglish("Customer ID Not found");
				errMsg.setIndonesian("Customer ID tidak ditemukan");
				errSchema.setError_code("BIT-404");
				initPrepHomeResp.setError_schema(errSchema);
				initPrepHomeResp.setOutput_schema(null);
			}

		} catch (Exception e) {
			errMsg.setEnglish("Internal Server Error");
			errMsg.setIndonesian("Internal SErver Error");
			errSchema.setError_code("BIT-500");
			initPrepHomeResp.setError_schema(errSchema);
			initPrepHomeResp.setOutput_schema(null);
			e.printStackTrace();
		}

		return initPrepHomeResp;

	}

	public TrxPromoUsedResponse saveTrxUser(String customerId, String merchantId, String promoId, String random, double amount) {
		long start = System.currentTimeMillis();
		String code = hashCode.getHashCode();
		TrxPromoUsed trxPromoUsed = new TrxPromoUsed();
		Customer customer = new Customer();
		Merchant merchant = new Merchant();
		TrxPromoUsedResponse trxPromoUsedResponse;
		String trxId = "";
		String transactionDate = "";

		logger.info("------------------------- Start Service Save TRX user -----------------------------");

		logger.debug(code + "Find merchant name and promo name by using ID");
		
		System.out.println(merchantId);
		
		merchant = merchantRepo.findByMerchantId(UUID.fromString(merchantId));
		
		Iterable<MerchantPromo> merchantPromoIterable = merchantPromoRepo.findByPromoId(UUID.fromString(promoId));
		MerchantPromo merchantPromo = merchantPromoIterable.iterator().next();

		System.out.println(merchantPromo.getPromoName());
		
		if (merchant.getMerchantName().isEmpty() || merchantPromo.getPromoName().isEmpty()) {
			logger.error(code + "no such merchant or promo id avaiable");
			trxPromoUsedResponse = new TrxPromoUsedResponse("BIT-410");
			return trxPromoUsedResponse;
		}
		try {
			logger.debug(code + "Check if the QR Generated already used");
			TrxPromoUsed output = trxPromoUsedRepo.findByIsUsed(UUID.fromString(random));
			if (output != null){
				logger.error(code + "QR Already Used");
				return new TrxPromoUsedResponse("BIT-203");
			}
			logger.debug(code + "save data to database");
			trxPromoUsed.setCustomerID(UUID.fromString(customerId));
			trxPromoUsed.setMerchantID(UUID.fromString(merchantId));
			trxPromoUsed.setPromoID(UUID.fromString(promoId));
			trxPromoUsed.setTrxID(UUID.randomUUID());
			trxPromoUsed.setIsUsed(UUID.fromString(random));
			logger.debug(code + "Checking: " + trxPromoUsed);
			TrxPromoUsed trx = trxPromoUsedRepo.save(trxPromoUsed);
			trxId = trx.getTrxID().toString();
			transactionDate = trx.getDateUsed().toString();

			logger.debug(code + "Add loyalty point to customer");
			customer = customerRepo.findByCustomerID(UUID.fromString(customerId));
			double loyalty = customer.getLoyaltyPoint();
			loyalty = loyalty + (amount/100);
			customer.setLoyaltyPoint(loyalty);
			customerRepo.save(customer);

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(code + "Get error: " + ex.getMessage());
			trxPromoUsedResponse = new TrxPromoUsedResponse("BIT-499");
			return trxPromoUsedResponse;
		}

		trxPromoUsedResponse = new TrxPromoUsedResponse(merchant.getMerchantName(), merchantId, transactionDate, trxId,
				merchantPromo.getPromoName(), merchantPromo.getPromoId().toString(), customerId, true, "BIT-200");

		logger.info(
				"-------------------------------------- Finish Service Save TRX User -------------------------------");
		logger.info("Service Time: " + (System.currentTimeMillis() - start));

		return trxPromoUsedResponse;
	}

	public InsertUserInterestResponse insertUserInterest(InsertUserInterestRequest request) {

		try {
			if (request.getCustomer_id().equals("") || request.getList_user_interest_id().size() == 0) {
				return new InsertUserInterestResponse(ReturnCode.BROMO_MISSING_INPUT);
			} else {
				Customer cust = customerRepo.findByCustomerID(UUID.fromString(request.getCustomer_id()));
				if (cust != null) {
					List<Integer> listCategoryId = request.getList_user_interest_id().stream()
							.map(s -> Integer.parseInt(s)).collect(Collectors.toList());
					List<Category> listCategoryInsert = categoryRepo.findByCategoryIdIn(listCategoryId);
					cust.setListCategoryUserInterest(listCategoryInsert);

					List<String> listNameCategoryInsert = listCategoryInsert.stream().map(c -> c.getCategoryName())
							.collect(Collectors.toList());

					customerRepo.save(cust);

					return new InsertUserInterestResponse(ReturnCode.BROMO_SERVICE_SUCCESS,
							cust.getCustomerID().toString(), listNameCategoryInsert);
				} else {
					return new InsertUserInterestResponse(ReturnCode.BROMO_DATA_NOT_FOUND);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new InsertUserInterestResponse(ReturnCode.BROMO_SERVICE_GENERAL_ERROR);
		}

	}

}
