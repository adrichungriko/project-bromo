package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.RequestOTP;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.UUID;

public interface RequestOTPRepo extends PagingAndSortingRepository<RequestOTP, UUID> {

    RequestOTP findByRequestID(UUID RequestID);
    List<RequestOTP> findAll();

}
