package com.bca.bit.bromo.mobile.entity.jpa;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "ms_category_merchant", schema = "app_bromo")
@Data
public class CategoryMerchant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Integer categoryId;

    @Column(name = "category_name")
    private String categoryName;

    @Column(name = "parent_category")
    private String parentCategory;
}
