package com.bca.bit.bromo.mobile.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.bca.bit.bromo.mobile.entity.jpa.Category;
import com.bca.bit.bromo.mobile.entity.jpa.Merchant;

public interface CategoryRepo extends PagingAndSortingRepository<Category, Integer> {

	Category findByCategoryId(int categoryId);

	@Query("SELECT c.listMerchantInCategory FROM Category c WHERE c.id = :id")
	List<Merchant> getMerchantInCategoryId(@Param("id") int categoryId);
	
	List<Category> findByParentCategoryIsNull();
	
	List<Category> findByCategoryIdIn(List<Integer> categoryListId);
}
