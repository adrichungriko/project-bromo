package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.MerchantCategory;
import com.bca.bit.bromo.mobile.entity.jpa.MerchantPromo;
import com.bca.bit.bromo.mobile.entity.jpa.Promo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MerchantPromoRepo extends CrudRepository<MerchantPromo,Integer> {
    Iterable<MerchantPromo> findByPromoId(UUID promoId);

    @Query("Select mp from com.bca.bit.bromo.mobile.entity.jpa.MerchantPromo mp "+
            "where mp.promoId in (:promoId)"
    )
    Iterable<MerchantPromo> findAllByPromoId(@Param("promoId") ArrayList<UUID> promoId);

    @Query("Select mp from com.bca.bit.bromo.mobile.entity.jpa.MerchantPromo mp "+
            "where mp.merchantId in (:merchantId)"
    )
    Iterable<MerchantPromo> findAllByMerchantId(@Param("merchantId") ArrayList<UUID> merchantId);

    List<MerchantPromo> findByMerchantId(UUID merchantId);
}
