package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface CustomerRepo extends PagingAndSortingRepository<Customer, UUID> {

    Customer findByCustomerID (UUID customerID);
    Customer findByCardNumber(String cardNo);
}
