package com.bca.bit.bromo.mobile.service;

import ch.qos.logback.core.pattern.parser.OptionTokenizer;
import com.bca.bit.bromo.mobile.entity.jpa.*;
import com.bca.bit.bromo.mobile.entity.model.SearchResponseModel;
import com.bca.bit.bromo.mobile.entity.response.MerchantPromoResponse;
import com.bca.bit.bromo.mobile.entity.response.PromoDetailResponse;
import com.bca.bit.bromo.mobile.entity.response.PromoLikeResponse;
import com.bca.bit.bromo.mobile.repo.*;
import com.bca.bit.bromo.utility.configuration.Constant;
import com.bca.bit.bromo.utility.configuration.HashCode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.*;

@Service(value = "promoService")
public class PromoService {
    @Autowired
    PromoRepo promoRepo;
    @Autowired
    MerchantPromoRepo merchantPromoRepo;
    @Autowired
    MerchantRepo merchantRepo;
    @Autowired
    CategoryMerchantService categoryMerchantService;
    @Autowired
    CategoryMerchantRepo categoryMerchantRepo;
    @Autowired
    MerchantCategoryRepo merchantCategoryRepo;
    @Autowired
    MerchantCategoryService merchantCategoryService;
    @Autowired
    MerchantService merchantService;
    @Autowired
    MerchantPromoService merchantPromoService;
    @Autowired
    HashCode hashCode;
    @Autowired
    ActionLogRepo actionLogRepo;

    Constant constant = new Constant();

    static Logger logger = Logger.getLogger("PromoService");

    public ArrayList<Promo> getAllPromo() {
        ArrayList<Promo> l_promo = new ArrayList<Promo>();
        promoRepo.findAll().forEach(l_promo::add);
        return l_promo;
    }

    public ArrayList<Promo> getAllPromoByPromoId(ArrayList<UUID> promoId) {
        ArrayList<Promo> l_promo = new ArrayList<>();
        for (Promo promo : promoRepo.findAllById(promoId)) {
            l_promo.add(promo);
        }
        return l_promo;
    }

    public ArrayList<Promo> getPromoByPromoName(String promoName) {
        ArrayList<Promo> l_promo = new ArrayList<>();
        Iterable<Promo> i_promo = promoRepo.findByPromoNameIgnoreCaseContaining(promoName);

        if (i_promo.iterator().hasNext()) {
            for (Promo promo : i_promo) {
                Date currDate = new Date();
                if(promo.getIsActive() && new Date(promo.getEndDate().getTime()).compareTo(currDate) >= 0) {
                    l_promo.add(promo);
                }
            }
        }

        return l_promo;
    }

    public Promo getPromoById(UUID promoId) {
        Promo result = new Promo();
        System.out.println("Promo Id: " + promoId);
        if (promoRepo.findById(promoId).isPresent()) {
            result = promoRepo.findById(promoId).get();
        }
        return result;
    }

    public ArrayList<SearchResponseModel> getPromoByCategoryId(Integer categoryId){
        String code = hashCode.getHashCode();
        ArrayList<CategoryMerchant>sc_catMerch;
        ArrayList<MerchantCategory>sc_merchCat;
        ArrayList<UUID>sc_merchantId = new ArrayList<>();
        ArrayList<Merchant> l_merchant = new ArrayList<>();
        ArrayList<SearchResponseModel> result = new ArrayList<>();

        String keyWord = categoryMerchantService.getCategoryById(categoryId).getCategoryName();
        logger.debug(code + "  Matching keyword \"" + keyWord + "\" with categoryName");
        sc_catMerch = categoryMerchantService.getCategoryByCategoryName(keyWord);
        System.out.println("[C NAME] List category : " + sc_catMerch);
        if (!sc_catMerch.isEmpty()) {
            sc_merchCat = merchantCategoryService.getAllMerchantByCategory(sc_catMerch);
            System.out.println("[C NAME] List merchant category : " + sc_merchCat);

            if(sc_merchCat != null) {
                for (MerchantCategory merchantCategory : sc_merchCat) {
                    sc_merchantId.add(merchantCategory.getMerchantId());
                }

                System.out.println("[C NAME] List merchant id : " + sc_merchantId);
                l_merchant.addAll(merchantService.getAllMerchantPromoByMerchantId(sc_merchantId));

                System.out.println("[C NAME] Promo that includes merchant within : " + l_merchant);
                result = generateSearchResponse(l_merchant,code);
            }
        }

        return result;
    }

    public ArrayList<SearchResponseModel> merchantRecommendation(String userLat, String userLong, String customerId) {
        String code = hashCode.getHashCode();
        logger.info("------------------ Start Service Merchant Recommendation ------------------------");
        //GET RESULT FROM DATA SCIENCE
        logger.debug("Prepare to call REST Service from data science");
        String strUri = constant.getURL_MERCHANT_RECOMMENDATION() + "?user_lat=%s&user_lon=%s&user_id=%s";
        final String uri = String.format(strUri, userLat, userLong, customerId);
        logger.debug("URI : " + uri);

        ArrayList<SearchResponseModel> result = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();

        logger.debug("Get HTTP Response from Data Science REST Service");
        try{
            logger.info("Status Ok");
            HashMap m_result = restTemplate.getForObject(uri, HashMap.class);
            ArrayList<String> l_merchantId = (ArrayList<String>) ((HashMap) m_result.get("output_schema")).get("merchant_id");
            //PREPARING RESPONSE
            ArrayList<UUID> merchantId = new ArrayList<>();
            ArrayList<Merchant> l_merchant = new ArrayList<>();
            l_merchantId.forEach(mid -> {
                l_merchant.add(merchantService.getMerchantById(UUID.fromString(mid)).get());
            });
            if(l_merchant != null) {
                result = generateSearchResponse(l_merchant, code);
            }
        }
        catch (Exception e){
            logger.error("Exception! " + e.getStackTrace());
        }
        finally {
            return result;
        }
    }

    public ArrayList<SearchResponseModel> searchPromo(String search) {
        ArrayList<SearchResponseModel> l_response = new ArrayList<>();
        ArrayList<MerchantCategory> sc_merchCat = new ArrayList<>();
        ArrayList<CategoryMerchant> sc_catMerch = new ArrayList<>();
        ArrayList<UUID> sc_merchantId = new ArrayList<>();
        ArrayList<Merchant> sc_merchant = new ArrayList<>();
        ArrayList<Merchant> l_merchant = new ArrayList<>();
        ArrayList<String> sc_search = new ArrayList<>(Arrays.asList(search.split(" ")));
        ArrayList<Promo> sc_promo = new ArrayList<>();
        ArrayList<UUID> sc_promoId = new ArrayList<>();
        ArrayList<MerchantPromo> sc_merchantPromo = new ArrayList<>();
        String code = hashCode.getHashCode();
        long start = System.currentTimeMillis();
        logger.info("------------------ Start Service Search Promo ------------------------");
        logger.debug(code + "  Parsing search key word: " + sc_search);

        for (String keyWord : sc_search) {

            //SEARCH BY CATEGORY NAME
            logger.debug(code + "  Matching keyword \"" + keyWord + "\" with categoryName");
            sc_catMerch = categoryMerchantService.getCategoryByCategoryName(keyWord);
            System.out.println("[C NAME] List category : " + sc_catMerch);
            if (!sc_catMerch.isEmpty()) {
                sc_merchCat = merchantCategoryService.getAllMerchantByCategory(sc_catMerch);
                System.out.println("[C NAME] List merchant category : " + sc_merchCat);

                for (MerchantCategory merchantCategory : sc_merchCat) {
                    sc_merchantId.add(merchantCategory.getMerchantId());
                }
                System.out.println("[C NAME] List merchant id : " + sc_merchantId);
                l_merchant.addAll(merchantService.getAllMerchantPromoByMerchantId(sc_merchantId));

                System.out.println("[C NAME] Promo that includes merchant within : " + l_merchant);
            }

            //SEARCH BY MERCHANT NAME
            logger.debug(code + "  Matching keyword \"" + keyWord + "\" with merchantName");
            sc_merchant = merchantService.getMerchantByMerchantName(keyWord);
            System.out.println("[M NAME] List merchant : " + sc_merchant);
            if (!sc_merchant.isEmpty()) {
                sc_merchantId.clear();
                for (Merchant merchant : sc_merchant) {
                    sc_merchantId.add(merchant.getMerchantId());
                }
                System.out.println("[M NAME] List merchant id : " + sc_merchantId);
                l_merchant.addAll(merchantService.getAllMerchantPromoByMerchantId(sc_merchantId));

                System.out.println("[M NAME] Promo that includes merchant within : " + l_merchant);
            }

            //SEARCH BY PROMO NAME
            logger.debug(code + "  Matching keyword \"" + keyWord + "\" with promoName");
            sc_promo = getPromoByPromoName(keyWord);
            System.out.println("[P NAME] List promo : " + sc_promo);
            if (!sc_promo.isEmpty()) {
                sc_merchantPromo = merchantPromoService.getAllMerchantByPromo(sc_promo);

                if (!sc_merchantPromo.isEmpty()) {
                    sc_merchantId.clear();
                    for (MerchantPromo merchant : sc_merchantPromo) {
                        sc_merchantId.add(merchant.getMerchantId());
                    }
                    System.out.println("[P NAME] List merchant id : " + sc_merchantId);
                    l_merchant.addAll(merchantService.getAllMerchantPromoByMerchantId(sc_merchantId));
                }
                l_merchant.addAll(merchantService.getAllMerchantPromoByMerchantId(sc_merchantId));
                System.out.println("[P NAME] Promo that includes merchant within : " + l_merchant);
            }
        }
        logger.debug(code + "  Collecting all result and distinct the result.");
        Set<Merchant> s_merchant = new HashSet<>(l_merchant);
        l_merchant.clear();
        l_merchant.addAll(s_merchant);
        logger.debug(code + "  Re-filtering result");
        l_merchant = satisfySearch(search, l_merchant, logger, code);

        logger.debug(code + "  Preparing response");
        l_response = generateSearchResponse(l_merchant, code);
        logger.info("------------------ Finish Service Search Promo ------------------------");
        logger.debug("Service time: " + (System.currentTimeMillis() - start));
        return l_response;
    }

    ArrayList<Merchant> satisfySearch(String search, ArrayList<Merchant> l_merchant, Logger logger, String code) {
        ArrayList<Merchant> r_merchant = new ArrayList<>();
        ArrayList<String> sc_search = new ArrayList<>(Arrays.asList(search.split(" ")));
        ArrayList<Boolean> sc_result = new ArrayList<>();
        ArrayList<MerchantPromo> l_merchantPromo = new ArrayList<>();
        ArrayList<Promo> l_promo = new ArrayList<>();
        ArrayList<UUID> l_promoId = new ArrayList<>();
        ArrayList<CategoryMerchant> l_categoryMerchant = new ArrayList<>();
        ArrayList<MerchantCategory> l_merchantCategory = new ArrayList<>();
        ArrayList<Integer> l_catId = new ArrayList<>();
        ArrayList<Boolean> b_result = new ArrayList<>();
        long start = System.currentTimeMillis();
        logger.info("------------------ Start Service Satisfy Search ------------------------");
        logger.debug(code + "  get List of Merchant data");

        for (Merchant merchant : l_merchant) {
            b_result.clear();
            boolean row_result = true;
            for (String keyWord : sc_search) {
                System.out.println("NOW, KEYWORD IS " + keyWord);
                l_merchantPromo.clear();
                l_promoId.clear();
                l_merchantCategory.clear();
                l_catId.clear();
                boolean b1, b2, b3;
                b1 = b2 = b3 = false;

                //CHECK PROMO CONTAINS KEYWORD

                l_merchantPromo.addAll(merchantPromoService.getPromoByMerchant(merchant));

                for (MerchantPromo promo : l_merchantPromo) {
                    l_promoId.add(promo.getPromoId());
                }
                l_promo = getAllPromoByPromoId(l_promoId);
                for (Promo promo : l_promo) {
                    System.out.println("Promo name ==> " + promo.getPromoName());
                    if (promo.getPromoName().toLowerCase().contains(keyWord.toLowerCase())) {
                        b1 = true;
                        break;
                    }
                }

                //CHECK MERCHANT CONTAINS KEYWORD
                System.out.println("Merchant name ==> " + merchant.getMerchantName());
                if (merchant.getMerchantName().toLowerCase().contains(keyWord.toLowerCase())) {
                    b2 = true;
                }

                //CHECK CATEGORY CONTAINS KEYWORD
                l_merchantCategory.addAll(merchantCategoryService.getCategoryByMerchant(merchant));

                for (MerchantCategory merchantCategory : l_merchantCategory) {
                    l_catId.add(merchantCategory.getCategoryId());
                }
                l_categoryMerchant = categoryMerchantService.getAllCategoryByCategoryId(l_catId);
                for (CategoryMerchant categoryMerchant : l_categoryMerchant) {
                    System.out.println("Category name ==> " + categoryMerchant.getCategoryName());
                    if (categoryMerchant.getCategoryName().toLowerCase().contains(keyWord.toLowerCase())) {
                        b3 = true;
                        break;
                    }
                }
                b_result.add(b1 || b2 || b3);
                System.out.println("Keyword " + keyWord + ", result : " + b_result);
            }
            for (Boolean b : b_result) {
                row_result = b && row_result;
            }
            logger.debug(code + "  Checking whether the result satisfy the search keyword");
            if (row_result == true) {
                r_merchant.add(merchant);
            }
            System.out.println();
            System.out.println();
            System.out.println();
        }
        logger.info("------------------ Finish Service Satisfy Search ------------------------");
        logger.info("Service time: " + (System.currentTimeMillis() - start));
        return r_merchant;
    }

    ArrayList<SearchResponseModel> generateSearchResponse(ArrayList<Merchant> l_merchant, String code) {
        ArrayList<SearchResponseModel> l_response = new ArrayList<>();
        for (Merchant merchant : l_merchant) {
            SearchResponseModel responseModel = new SearchResponseModel();
            SearchResponseModel.Promo s_promo = new SearchResponseModel.Promo();
            ArrayList<SearchResponseModel.Category> l_responseCat = new ArrayList<>();
            ;
            Double percent_cashback = 0.0;

            //GET PROMO BY MERCHANT
            logger.debug(code + "  Get best promo deal per-merchant");
            for (MerchantPromo mp : merchantPromoService.getPromoByMerchant(merchant)) {
                Promo promo = new Promo();
                System.out.println("merchant id: " + mp.getMerchantId());
                promo = getPromoById(mp.getPromoId());
                if (promo.getPromoId() == null || "".equals(promo.getPromoId())) {
                    System.out.println("Nothing's here");
                } else {
                    if (promo.getCashbackPercent() > percent_cashback) {
                        percent_cashback = promo.getCashbackPercent();
                        s_promo.setPromoId(promo.getPromoId());
                        s_promo.setPromoName(promo.getPromoName());
                        s_promo.setCashbackPercent(promo.getCashbackPercent());
                    }
                }
            }

            //GET CATEGORY BY MERCHANT
            logger.debug(code + "  Get merchant category");
            for (MerchantCategory mc : merchantCategoryService.getCategoryByMerchant(merchant)) {
                SearchResponseModel.Category s_cat = new SearchResponseModel.Category();
                CategoryMerchant category = new CategoryMerchant();
                category = categoryMerchantService.getCategoryById(mc.getCategoryId());
                s_cat.setCategoryId(category.getCategoryId());
                s_cat.setCategoryName(category.getCategoryName());
                l_responseCat.add(s_cat);
            }

            responseModel.setMerchantId(merchant.getMerchantId());
            responseModel.setMerchantName(merchant.getMerchantName());
            responseModel.setMerchantImage(merchant.getMerchantImage());
            responseModel.setPromo(s_promo);
            responseModel.setCategory(l_responseCat);

            l_response.add(responseModel);
        }
        return l_response;
    }

    public MerchantPromoResponse getPromoByMerchantID(String merchantID, UUID customerId) {
        MerchantPromoResponse merchantPromoResponse = new MerchantPromoResponse();
        String code = hashCode.getHashCode();
        Promo promo = new Promo();
        Boolean liked = false;
        List<Promo> promoList = new ArrayList<>();
        MerchantPromo merchantPromo = new MerchantPromo();
        long start = System.currentTimeMillis();
        logger.info("------------------ Start Service Get Promo List By Merchant ID ------------------------");
        logger.debug(code + "get Merchant data from database");

        List<MerchantPromo> merchantPromoList = merchantPromoRepo.findByMerchantId(UUID.fromString(merchantID));

        logger.debug(code + "Configure the response");

        for (MerchantPromo res : merchantPromoList) {
            if (promoRepo.findById(res.getPromoId()).isPresent()) {
                promo = promoRepo.findById(res.getPromoId()).get();
                promo.setLiked(false);
            }
            List<ActionLog> l_actionLog = actionLogRepo.findByPromoIdAndCustomerId(res.getPromoId().toString(), customerId.toString());
            for (ActionLog actLog : l_actionLog) {
                if (actLog.getAction().toLowerCase().equals("like")) {
                    promo.setLiked(true);
                    break;
                } else {
                    promo.setLiked(false);
                }
            }

            Date currDate = new Date();
            if(promo.getIsActive() && new Date(promo.getEndDate().getTime()).compareTo(currDate) >= 0) {
                promoList.add(promo);
            }
        }

        merchantPromoResponse = new MerchantPromoResponse(merchantID, promoList, "BIT-200");

        logger.info("------------------------ Finish Service Get Promo list By Merchant ID ----------------------");
        logger.info("Service time: " + (System.currentTimeMillis() - start));

        return merchantPromoResponse;
    }

    public PromoDetailResponse getPromoDetailById(String promoId, String customerId, String search) {
        PromoDetailResponse promoDetailResponse;
        List<ActionLog> actionLogList = new ArrayList<>();
        Promo result = new Promo();
        long start = System.currentTimeMillis();
        ActionLog actionLog = new ActionLog();
        String code = hashCode.getHashCode();
        logger.info("---------------------- Start Service Get Promo Detail ------------------");
        logger.debug(code + "Get promo detail from database");
        try {
            result = getPromoById(UUID.fromString(promoId));
            actionLogList = actionLogRepo.findByPromoIdAndCustomerId(promoId, customerId);
            logger.debug(code + "Check if promo liked by customer or not");
            for (ActionLog actionLog1 : actionLogList){
                if (actionLog1.getAction().equals("like")){
                    result.setLiked(true);
                }
            }
            if (result.getLiked() == null){
                result.setLiked(false);
            }
            logger.debug(code + "Check output: "+ result);
        }catch (Exception ex){
            logger.error(code + "Get Error: " + ex.getMessage());
            promoDetailResponse = new PromoDetailResponse(result, "BIT-499");
        }
        logger.debug(code + "Get if the ");
        if (result.getPromoId().toString().isEmpty() || result.getPromoId() == null) {
            promoDetailResponse = new PromoDetailResponse("BIT-410");
        } else {
            logger.debug(code + "Save view data to database");
            try {
                if (search.equals("")) {

                    logger.debug(code + "User dont user search");

                    actionLog.setAction("view");
                    actionLog.setCustomerId(customerId);
                    actionLog.setPromoId(promoId);
                    actionLog.setSearch(search);
                    actionLogRepo.save(actionLog);
                    promoDetailResponse = new PromoDetailResponse(result, "BIT-200");
                } else {

                    logger.debug(code + "User using search");

                    actionLog.setAction("search");
                    actionLog.setCustomerId(customerId);
                    actionLog.setPromoId(promoId);
                    actionLog.setSearch(search);
                    actionLogRepo.save(actionLog);

                    actionLog = new ActionLog();

                    actionLog.setAction("view");
                    actionLog.setCustomerId(customerId);
                    actionLog.setPromoId(promoId);
                    actionLog.setSearch("");
                    actionLogRepo.save(actionLog);

                    promoDetailResponse = new PromoDetailResponse(result, "BIT-200");
                }
            } catch (Exception ex) {
                logger.error(code + "Get Error: " + ex.getMessage());
                promoDetailResponse = new PromoDetailResponse("BIT-499");
            }
        }

        logger.info("---------------------- Finish Service Get Promo Detail -----------------");
        logger.info("Service time: " + (System.currentTimeMillis() - start));
        return promoDetailResponse;
    }

    public PromoLikeResponse doSavePromoLike(String promoId, String customerId, String search, boolean like, boolean thumbnail) {
        String code = hashCode.getHashCode();
        PromoLikeResponse promoLikeResponse = new PromoLikeResponse();
        ActionLog actionLog = new ActionLog();
        List<ActionLog> actionLogsList = new ArrayList<>();
        long start = System.currentTimeMillis();

        logger.info("----------------------------------- Start Service Promo Like --------------------------------------");
        actionLogsList = actionLogRepo.findByPromoIdAndCustomerId(promoId, customerId);
        logger.debug(code + "Check to dislike or like");
        logger.debug(code + "Find the action log to dislike");
        for (ActionLog output : actionLogsList) {
            if (output.getAction().equals("like")) {
                actionLog = output;
                logger.debug(code + "Checking output: " + actionLog.toString());
            }
        }
        if (like == true && thumbnail == true && (actionLog.getAction() == null)) {
            logger.debug(code + "Save Like to database");
            try {
                actionLog.setSearch(search);
                actionLog.setPromoId(promoId);
                actionLog.setCustomerId(customerId);
                actionLog.setAction("like");
                actionLogRepo.save(actionLog);

                logger.debug(code + "Also save it as view");
                actionLog = new ActionLog();
                actionLog.setSearch(search);
                actionLog.setPromoId(promoId);
                actionLog.setCustomerId(customerId);
                actionLog.setAction("view");
                actionLogRepo.save(actionLog);

                promoLikeResponse = new PromoLikeResponse(true, "BIT-200");
            } catch (Exception ex) {
                logger.error(code + "Get error: " + ex.getMessage());
                promoLikeResponse = new PromoLikeResponse("BIT-499");
            }
        } else if (like == true && thumbnail == false && (actionLog.getAction() == null)) {
            logger.debug(code + "Save Like to database");
            logger.debug(code + "dont need to save view");
            try {
                actionLog.setSearch(search);
                actionLog.setPromoId(promoId);
                actionLog.setCustomerId(customerId);
                actionLog.setAction("like");
                actionLogRepo.save(actionLog);


                promoLikeResponse = new PromoLikeResponse(true, "BIT-200");
            } catch (Exception ex) {
                logger.error(code + "Get error: " + ex.getMessage());
                promoLikeResponse = new PromoLikeResponse("BIT-499");
            }
        } else if (like == false) {
            logger.debug(code + "Dislike a promo");
            logger.debug(code + "find the data in database");
            logger.debug(code + "Check action log: " + actionLog.getAction());
            try {
                if (actionLog.getAction() == null) {
                    logger.debug(code + "No Promo to dislike");
                    promoLikeResponse = new PromoLikeResponse("BIT-410");
                } else {
                    actionLogRepo.delete(actionLog);
                    promoLikeResponse = new PromoLikeResponse(true, "BIT-200");
                }
            } catch (Exception ex) {
                logger.error(code + "Get error: " + ex.getMessage());
                promoLikeResponse = new PromoLikeResponse("BIT-499");
            }
        } else {
            promoLikeResponse = new PromoLikeResponse("BIT-201");
        }

        logger.info("--------------------------------- Finish Service Like Promo ------------------------------");
        logger.info("Service Time: " + (System.currentTimeMillis() - start));

        return promoLikeResponse;
    }

}
