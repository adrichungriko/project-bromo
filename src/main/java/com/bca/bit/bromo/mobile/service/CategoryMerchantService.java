package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.CategoryMerchant;
import com.bca.bit.bromo.mobile.repo.CategoryMerchantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service(value = "categoryMerchantService")
public class CategoryMerchantService {
    @Autowired
    CategoryMerchantRepo categoryMerchantRepo;

    public ArrayList<CategoryMerchant> getCategoryByCategoryName(String catName){
        Iterable<CategoryMerchant> catMerchant = categoryMerchantRepo.findByCategoryNameIgnoreCaseContaining(catName);
        ArrayList<CategoryMerchant> l_catMerchant= new ArrayList<>();

        for (CategoryMerchant category:catMerchant) {
            if (category.getParentCategory() == null || "".equals(category.getParentCategory())){
                l_catMerchant.add(category);
                for(CategoryMerchant childCat:categoryMerchantRepo.findByParentCategory(category.getCategoryId().toString())){
                    l_catMerchant.add(childCat);
                }
            }
            else {
                l_catMerchant.add(category);
            }
        }
        return l_catMerchant;
    }

    public ArrayList<CategoryMerchant> getAllCategoryByCategoryId(ArrayList<Integer> catId){
        ArrayList<CategoryMerchant> l_categoryMerchant = new ArrayList<>();
        Iterable<CategoryMerchant > i_categoryMerchant = categoryMerchantRepo.findAllById(catId);
        for(CategoryMerchant categoryMerchant : i_categoryMerchant){
            l_categoryMerchant.add(categoryMerchant);
        }

        return l_categoryMerchant;
    }

    public CategoryMerchant getCategoryById(Integer catId){
        CategoryMerchant result = new CategoryMerchant();
        if(categoryMerchantRepo.findById(catId).isPresent()){
            result =        categoryMerchantRepo.findById(catId).get();
        }
        return result;
    }
}
