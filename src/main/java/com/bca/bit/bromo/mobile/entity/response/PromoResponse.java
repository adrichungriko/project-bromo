package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.mobile.entity.jpa.Promo;
import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

import java.util.ArrayList;

@Data
public class PromoResponse extends BaseResponse {
    private PromoResponseOutputSchema output_schema;

    public PromoResponse(){}
    public PromoResponse(ArrayList<Promo> l_promo, String error_code){
        super(error_code);
        output_schema = new PromoResponseOutputSchema(l_promo);
    }
    public PromoResponse(String error_code){
        super(error_code);
    }

    @Data
    private class PromoResponseOutputSchema{
        private ArrayList<Promo> promo;
        public PromoResponseOutputSchema(ArrayList<Promo> l_promo){
            promo = l_promo;
        }

    }
}
