package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.mobile.entity.jpa.Promo;
import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

@Data
public class PromoLikeResponse extends BaseResponse {
    private PromoLikeResponseOutputSchema output_schema;

    public PromoLikeResponse(String errorCode){
        super(errorCode);
    }

    public PromoLikeResponse(){}

    public PromoLikeResponse(boolean verify, String errorCode){
        super(errorCode);
        output_schema = new PromoLikeResponseOutputSchema(verify);
    }

    @Data
    public static class PromoLikeResponseOutputSchema{
        private boolean verify;

        public PromoLikeResponseOutputSchema(boolean verify) {
            this.verify = verify;
        }
    }
}
