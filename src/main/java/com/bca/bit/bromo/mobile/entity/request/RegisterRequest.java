package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class RegisterRequest {
	
	private String cred_no;
	private String phone_num;
	private String device_data;

}
