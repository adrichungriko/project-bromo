package com.bca.bit.bromo.mobile.controller;


import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.model.MerchantResponseModel;
import com.bca.bit.bromo.mobile.entity.model.SearchResponseModel;
import com.bca.bit.bromo.mobile.entity.response.MerchantResponse;
import com.bca.bit.bromo.mobile.entity.response.SearchResponse;
import com.bca.bit.bromo.mobile.service.MerchantService;
import com.bca.bit.bromo.mobile.service.PromoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.UUID;

@RestController
@RequestMapping("/mobile")
public class MerchantController {
    @Autowired
    MerchantService merchantService;

    @Autowired
    PromoService promoService;

    @GetMapping(value = "/merchant/catId/{catId}", produces = "application/json")
    public SearchResponse getMerchantByCategory(@PathVariable Integer catId){
        ArrayList<SearchResponseModel> l_merchant = new ArrayList<>();
        SearchResponse response;

        try {
            ArrayList<SearchResponseModel> i_merchant = promoService.getPromoByCategoryId(catId);
            if (i_merchant.iterator().hasNext()) {
                l_merchant.addAll((ArrayList) i_merchant);
                response = new SearchResponse(l_merchant,"BIT-200");
            }
            else {
                response = new SearchResponse("BIT-410");
            }

        }
        catch (Exception e){
            response = new SearchResponse("BIT-400");
            e.printStackTrace();
        }

        return response;
    }

    /*@GetMapping(value = "/merchant/catId/{catId}", produces = "application/json")
    public MerchantResponse getMerchantByCategory(@PathVariable Integer catId){
        ArrayList<MerchantResponseModel> l_merchant = new ArrayList<>();
        MerchantResponse response;

        try {
            Iterable<MerchantResponseModel> i_merchant = merchantService.getMerchantAndOpeningHoursByCatId(catId);
            if (i_merchant.iterator().hasNext()) {
                l_merchant.addAll((ArrayList) i_merchant);
                response = new MerchantResponse(l_merchant,"BIT-200");
            }
            else {
                response = new MerchantResponse("BIT-410");
            }

        }
        catch (Exception e){
            response = new MerchantResponse("BIT-400");
            e.printStackTrace();
        }

        return response;
    }*/

    @GetMapping(value = "/merchant/merchId/{merchId}", produces = "application/json")
    public MerchantResponse getMerchantDetailByMerchantId(@PathVariable UUID merchId){
        ArrayList<MerchantResponseModel> l_merchant = new ArrayList<>();
        MerchantResponse response;

        try {
            Iterable<MerchantResponseModel> i_merchant = merchantService.getMerchantAndOpeningHoursByCentrailId(merchId);
            if (i_merchant.iterator().hasNext()) {
                l_merchant.addAll((ArrayList<MerchantResponseModel>) i_merchant);
                response = new MerchantResponse(l_merchant,"BIT-200");
            }
            else {
                response = new MerchantResponse("BIT-410");
            }

        }
        catch (Exception e){
            response = new MerchantResponse("BIT-400");
            e.printStackTrace();
        }

        return response;
    }
}
