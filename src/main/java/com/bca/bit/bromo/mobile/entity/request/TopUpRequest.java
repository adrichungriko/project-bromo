package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class TopUpRequest {
    private String customer_id;
    private int amount;
    private String fintech_id;
}
