package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.ActionLog;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ActionLogRepo extends PagingAndSortingRepository<ActionLog, Integer> {
    List<ActionLog> findAll();

    List<ActionLog> findByPromoIdAndCustomerId(String promoId, String customerId);
}
