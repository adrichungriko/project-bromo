package com.bca.bit.bromo.mobile.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import org.springframework.stereotype.Repository;

import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.UUID;

@Repository
public interface MerchantRepo extends PagingAndSortingRepository<Merchant, UUID> {

    //Get list of merchant yang terdaftar pada promo yang aktif
    @Query("Select distinct(m) from com.bca.bit.bromo.mobile.entity.jpa.Promo p "+
            "join com.bca.bit.bromo.mobile.entity.jpa.MerchantPromo mp on p.promoId = mp.promoId " +
            "join com.bca.bit.bromo.mobile.entity.jpa.Merchant m on mp.merchantId = m.merchantId " +
            "where mp.merchantId in (:merchantId) and p.isActive=true"
    )
    Iterable<Merchant> findMerchantPromoByMerchantId(@Param("merchantId") ArrayList<UUID> merchantId);

    Iterable<Merchant> findByMerchantNameIgnoreCaseContaining(String merchName);

    @Query("select m from com.bca.bit.bromo.mobile.entity.jpa.Merchant m "+
    "where (m.merchantCentralId = '' or m.merchantCentralId is null) and m.merchantId in (:merchantId)")
    Iterable<Merchant> findAllMerchantCentralByMerchantId(@Param("merchantId") ArrayList<UUID> merchantId);

    Iterable<Merchant> findMerchantByMerchantCentralId(String merchantId);
    
    Merchant findByMerchantNameAndMerchantType(String name,String type);

    Merchant findByMerchantId(UUID merchantId);
    
}
