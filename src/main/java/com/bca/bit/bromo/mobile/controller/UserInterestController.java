package com.bca.bit.bromo.mobile.controller;

import com.bca.bit.bromo.mobile.entity.request.InsertUserInterestRequest;
import com.bca.bit.bromo.mobile.entity.request.TrxPromoUsedRequest;
import com.bca.bit.bromo.mobile.entity.response.TrxPromoUsedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.bca.bit.bromo.mobile.entity.response.InquiryUserInterestResponse;
import com.bca.bit.bromo.mobile.entity.response.InsertUserInterestResponse;
import com.bca.bit.bromo.mobile.service.UserService;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/mobile")
public class UserInterestController {
	
	@Autowired
	private UserService userSvc;

	@GetMapping("/inquiry/interest")
	public InquiryUserInterestResponse loadUserInterest() {
		InquiryUserInterestResponse inquiryUserInterestResponse;
		try{
			inquiryUserInterestResponse = userSvc.inquiryUserInterest();
		}catch (Exception ex){
			inquiryUserInterestResponse = new InquiryUserInterestResponse("BIT-499");
		}
		return inquiryUserInterestResponse;
	}
	
	@PostMapping("/save/interest")
	public InsertUserInterestResponse insertUserInterest(@RequestBody InsertUserInterestRequest request) {
		InsertUserInterestResponse InsertUserInterestResponse;
		try{
			InsertUserInterestResponse = userSvc.insertUserInterest(request);
		}catch (Exception ex){
			InsertUserInterestResponse = new InsertUserInterestResponse("BIT-499", ex.getMessage());
		}
		return InsertUserInterestResponse;
	}

	@PostMapping("/savetrx")
	public TrxPromoUsedResponse saveTrxUser(@RequestBody TrxPromoUsedRequest trxPromoUsedRequest){
		String customerID = trxPromoUsedRequest.getCustomer_id()== null ? "" : trxPromoUsedRequest.getCustomer_id();
		String merchantID = trxPromoUsedRequest.getMerchant_id()== null ? "" : trxPromoUsedRequest.getMerchant_id();
		String promoID = trxPromoUsedRequest.getPromo_id()== null ? "" : trxPromoUsedRequest.getPromo_id();
		String random = trxPromoUsedRequest.getRandom() == null ? "" : trxPromoUsedRequest.getRandom();
		TrxPromoUsedResponse trxPromoUsedResponse = new TrxPromoUsedResponse();

		if (customerID == ""){
			trxPromoUsedResponse = new TrxPromoUsedResponse("BIT-404");
			return trxPromoUsedResponse;
		} else if (merchantID == "") {
			trxPromoUsedResponse = new TrxPromoUsedResponse("BIT-404");
			return trxPromoUsedResponse;
		}else if (promoID == ""){
			trxPromoUsedResponse = new TrxPromoUsedResponse("BIT-404");
			return trxPromoUsedResponse;
		} else {
			if (random == "") {
				random = UUID.randomUUID().toString();
			}
			try {
				trxPromoUsedResponse = userSvc.saveTrxUser(customerID, merchantID, promoID, random, trxPromoUsedRequest.getAmount());
			}catch (Exception ex){
				ex.printStackTrace();
				trxPromoUsedResponse = new TrxPromoUsedResponse("BIT-499", ex.getMessage());
			}
		}
		return trxPromoUsedResponse;
	}
	
}
