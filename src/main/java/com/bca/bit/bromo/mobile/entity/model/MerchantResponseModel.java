package com.bca.bit.bromo.mobile.entity.model;

import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.jpa.MerchantOpeningHours;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.UUID;

@Data
public class MerchantResponseModel {
    private Merchant merchant;

    @JsonIgnoreProperties(value = {"id","merchantId"})
    private ArrayList<MerchantOpeningHours> openingHours;
}