package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class OTPRequest {
    private String request_id;
    private String otp;
}
