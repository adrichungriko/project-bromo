package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class LogoutRequest {
    private String customer_id;
    private String device_data;
}
