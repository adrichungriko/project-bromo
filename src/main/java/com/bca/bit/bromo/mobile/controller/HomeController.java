package com.bca.bit.bromo.mobile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bca.bit.bromo.mobile.entity.response.InitPrepareHomeResponse;
import com.bca.bit.bromo.mobile.entity.response.InquirySaldoResponse;
import com.bca.bit.bromo.mobile.service.InquirySaldoService;
import com.bca.bit.bromo.mobile.service.LoadImageService;
import com.bca.bit.bromo.mobile.service.UserService;

@RestController
@RequestMapping("/mobile")
public class HomeController {
	
	@Autowired
	private UserService userSvc;
	
	@Autowired
	private InquirySaldoService inquirySaldoSvc;

	@GetMapping("/user/inquiry/dummy")
	public InitPrepareHomeResponse inquiryUserInit(@RequestParam String customer_id) {
		InitPrepareHomeResponse initPrepareHomeResponse = new InitPrepareHomeResponse();
		InitPrepareHomeResponse.ErrorSchema errorSchema = new InitPrepareHomeResponse.ErrorSchema();
		InitPrepareHomeResponse.ErrorSchema.ErrorMessage errorMessage = new InitPrepareHomeResponse.ErrorSchema.ErrorMessage();
		InitPrepareHomeResponse.InitPrepareHomeOutputSchema initPrepareHomeOutputSchema = new InitPrepareHomeResponse.InitPrepareHomeOutputSchema();
		if (customer_id.isEmpty() || customer_id == null){
			errorMessage.setEnglish("Missing Input");
			errorMessage.setIndonesian("Input Kurang");
			errorSchema.setError_code("BIT-404");
			errorSchema.setError_message(errorMessage);
			initPrepareHomeResponse.setError_schema(errorSchema);
		}else{
			try {
				initPrepareHomeResponse = userSvc.initPrepareHomeDummy(customer_id);
			}catch (Exception ex){
				errorSchema.setError_code("BIT-499");
				initPrepareHomeResponse.setError_schema(errorSchema);
			}
		}
		return initPrepareHomeResponse;
	}
	
	@GetMapping("/user/inquiry")
	public InitPrepareHomeResponse inquiryUser(@RequestParam String customer_id) {
		InitPrepareHomeResponse initPrepareHomeResponse = new InitPrepareHomeResponse();
		InitPrepareHomeResponse.ErrorSchema errorSchema = new InitPrepareHomeResponse.ErrorSchema();
		InitPrepareHomeResponse.ErrorSchema.ErrorMessage errorMessage = new InitPrepareHomeResponse.ErrorSchema.ErrorMessage();
		InitPrepareHomeResponse.InitPrepareHomeOutputSchema initPrepareHomeOutputSchema = new InitPrepareHomeResponse.InitPrepareHomeOutputSchema();
		if (customer_id.isEmpty() || customer_id == null){
			errorMessage.setEnglish("Missing Input");
			errorMessage.setIndonesian("Input Kurang");
			errorSchema.setError_code("BIT-404");
			errorSchema.setError_message(errorMessage);
			initPrepareHomeResponse.setError_schema(errorSchema);
		}else{
			try {
				initPrepareHomeResponse = userSvc.initPrepareHome(customer_id);
			}catch (Exception ex){
				errorSchema.setError_code("BIT-499");
				initPrepareHomeResponse.setError_schema(errorSchema);
			}
		}
		return initPrepareHomeResponse;
	}
	
	@GetMapping("/user/saldo")
	public InquirySaldoResponse inqSaldo(@RequestParam String customer_id) {
		InquirySaldoResponse inquirySaldoResponse = new InquirySaldoResponse();
		InquirySaldoResponse.ErrorSchema errorSchema = new InquirySaldoResponse.ErrorSchema();
		InquirySaldoResponse.ErrorSchema.ErrorMessage errorMessage = new InquirySaldoResponse.ErrorSchema.ErrorMessage();
		if (customer_id.isEmpty() || customer_id == null){
			errorMessage.setEnglish("Missing input");
			errorMessage.setIndonesian("input kurang");
			errorSchema.setError_code("BIT-404");
			errorSchema.setError_message(errorMessage);
			inquirySaldoResponse.setError_schema(errorSchema);
		}else {
			try{
				inquirySaldoResponse = inquirySaldoSvc.doInquiry(customer_id);
			}catch (Exception ex){
				errorMessage.setEnglish(ex.getMessage());
				errorSchema.setError_message(errorMessage);
				errorSchema.setError_code("BIT-499");
			}
		}
		return inquirySaldoResponse;
	}
	
	
	
}
