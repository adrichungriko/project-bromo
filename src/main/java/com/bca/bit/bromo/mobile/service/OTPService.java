package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.RequestOTP;
import com.bca.bit.bromo.mobile.entity.response.OTPResponse;
import com.bca.bit.bromo.mobile.repo.RequestOTPRepo;
import com.bca.bit.bromo.sms.Client;
import com.bca.bit.bromo.sms.entity.SMSResponse;
import com.bca.bit.bromo.sms.entity.request.SendSMSRequest;
import com.bca.bit.bromo.utility.configuration.HashCode;
import com.google.gson.Gson;
import com.nexmo.client.NexmoClient;
import com.nexmo.client.sms.messages.TextMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class OTPService {

    static Logger logger = Logger.getLogger("OTPService");

    @Autowired
    private HashCode hashCode;

    @Autowired
    private RequestOTPRepo requestOTPRepo;


    public Hashtable<String, String> doGenerateOTP(String phoneNum, String customerID, boolean verify){
        String output;
        Hashtable<String, String> hashResult = new Hashtable<>();
        RequestOTP requestOTP = new RequestOTP();
        String otp = "";
        String numbers = "0123456789";
        Random random = new Random();
        int length = 6;
        char [] result = new char[length];
        RestTemplate restTemplate = new RestTemplate();
        String message = "OTP:";
        long start = System.currentTimeMillis();
        String code = hashCode.getHashCode();
        String test = "";
        Gson gson = new Gson();

        logger.info("------------- Start Generate and Send OTP Service --------------");
        logger.debug(code + "Generate OTP");

        for(int i = 0; i < length; i++){
            result[i] = numbers.charAt(random.nextInt(numbers.length()));
        }
        otp = new String(result);
        message += otp;


        logger.debug(code + "Encode OTP: " + otp);

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String otphash = passwordEncoder.encode(otp);

        logger.debug(code +"Send OTP to SMS");
       /* try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("http://10.20.214.63:8008/sms").queryParam("api_key", "968cc7fd")
                    .queryParam("api_secret", "bwQU45tVughlgGF9").queryParam("to", "6281388999898").queryParam("from", "BCA")
                    .queryParam("text", message);
            ResponseEntity<String> response = restTemplate.getForEntity(builder.toUriString(), String.class);

            logger.debug(code + "Checking response: " + response);
        }catch (Exception ex){
            logger.error(code + "Get error: " + ex.getMessage());
        }*/
        if (verify) {
            logger.debug(code + "Save OTP Hash to database");
            output = doSaveOTP(otphash, customerID);
            hashResult.put("requestID", output);
            hashResult.put("otp", otp);
        }else {
            logger.debug(code + "just send OTP");
        }
        logger.info("------------- Finish Generate and Send OTP Service --------------");
        logger.info("Service time: " + (System.currentTimeMillis() - start));

        return hashResult;
    }

    public String doSaveOTP(String otpHash, String customerId){
        String output = "";

        RequestOTP requestOTP = new RequestOTP();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = java.util.Calendar.getInstance().getTime();

        requestOTP.setOtpCreated(formatter.format(date));
        requestOTP.setOtpHash(otpHash);
        requestOTP.setCustomerID(UUID.fromString(customerId));
        RequestOTP reqotp = requestOTPRepo.save(requestOTP);

        output = reqotp.getRequestID().toString();

        return output;

    }

    public OTPResponse doDummyOTP(String customerID, String otp){
        OTPResponse otpResponse = new OTPResponse();
        OTPResponse.OTPResponseOutputSchema outputSchema = new OTPResponse.OTPResponseOutputSchema();
        OTPResponse.ErrorSchema errorSchema = new OTPResponse.ErrorSchema();
        OTPResponse.ErrorSchema.ErrorMessage errorMessage = new OTPResponse.ErrorSchema.ErrorMessage();

        errorSchema.setError_code("BIT-200");
        errorMessage.setEnglish("Success");
        errorMessage.setIndonesian("Berhasil");
        errorSchema.setError_message(errorMessage);
        outputSchema.setCustomer_id(customerID);
        outputSchema.setStatus(true);
        otpResponse.setError_schema(errorSchema);
        otpResponse.setOutput_schema(outputSchema);

        return otpResponse;
    }

    public OTPResponse doOTP(String requestID, String otp){
        OTPResponse otpResponse = new OTPResponse();
        OTPResponse.OTPResponseOutputSchema outputSchema = new OTPResponse.OTPResponseOutputSchema();
        OTPResponse.ErrorSchema errorSchema = new OTPResponse.ErrorSchema();
        OTPResponse.ErrorSchema.ErrorMessage errorMessage = new OTPResponse.ErrorSchema.ErrorMessage();
        BCrypt bCrypt = new BCrypt();
        RequestOTP requestOTP = new RequestOTP();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        long startTime = 0;
        Date d = new Date();
        boolean verify = false;
        String otpSendTime ="";
        long start = System.currentTimeMillis();

        String code = hashCode.getHashCode();

        logger.info("---------- Start Verification OTP -----------");

        logger.debug(code + "Get user from database");

        requestOTP = requestOTPRepo.findByRequestID(UUID.fromString(requestID));

        logger.debug(code + "Get the time difference to check if the OTP already timeout or not");
        logger.debug(code + "Checking the User: " + requestOTP.getOtpCreated());

        otpSendTime = requestOTP.getOtpCreated();
        long current = System.currentTimeMillis();

        try {
            d = formatter.parse(otpSendTime);
            startTime = d.getTime();

            logger.debug(code + "Checking the start Time: " + startTime);
        }catch (Exception ex){
            ex.getMessage();
        }

        long output = (current - startTime);

        logger.debug(code + "Checking the time difference: " + output);

        if(output <= 120500) {
            logger.debug(code + "OTP Not Yet Timeout");
            logger.debug(code + "Verifying OTP");
            verify = bCrypt.checkpw(otp, requestOTP.getOtpHash());
        }else{
            logger.error(code + "OTP Timeout");
            errorSchema.setError_code("BIT-400");
            errorMessage.setEnglish("OTP Timeout");
            errorMessage.setIndonesian("OTP Sudah tidak berlaku");
            errorSchema.setError_message(errorMessage);
            otpResponse.setError_schema(errorSchema);
            return otpResponse;
        }

        if(verify){
            logger.debug(code + "Success");

            errorSchema.setError_code("BIT-200");
            errorMessage.setEnglish("Success");
            errorMessage.setIndonesian("Berhasil");
            errorSchema.setError_message(errorMessage);
            outputSchema.setCustomer_id(requestOTP.getCustomerID().toString());
            outputSchema.setStatus(true);
            otpResponse.setError_schema(errorSchema);
            otpResponse.setOutput_schema(outputSchema);
        }else{
            logger.error(code + "Wrong OTP");
            errorMessage.setIndonesian("OTP salah");
            errorMessage.setEnglish("Wrong OTP");
            errorSchema.setError_code("BIT-404");
            errorSchema.setError_message(errorMessage);
            otpResponse.setError_schema(errorSchema);
        }

        logger.info("------------ Finish OTP Verification Service ------------");
        logger.info("Service Time: " + (System.currentTimeMillis() - start));

        return otpResponse;
    }


}
