package com.bca.bit.bromo.mobile.entity.jpa;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Data
@Table(name = "merchant_promo", schema = "app_bromo")
public class MerchantPromo {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "merchant_id")
    private UUID merchantId;

    @Column(name = "promo_id")
    private UUID promoId;

    @Column(name = "promo_name")
    private String promoName;
}
