package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

@Data
public class BindFintechResponse extends BaseResponse {
    private BindOutputSchema output_schema;

    public BindFintechResponse(){}
    public BindFintechResponse(String phoneNum, String fintechResponse, boolean verify, String error_code){
        super(error_code);
        this.output_schema = new BindOutputSchema(phoneNum, verify, fintechResponse);
    }
    public BindFintechResponse(String errorCode){
        super(errorCode);
    }

    public BindFintechResponse(String errorCode, String message){
        super(errorCode, message);
    }

    @Data
    public static class BindOutputSchema{
        private String phone_num;
        private boolean verify;
        private String fintech_response;

        public BindOutputSchema(String phone_num, boolean verify, String fintechResponse) {
            this.phone_num = phone_num;
            this.verify = verify;
            this.fintech_response = fintechResponse;
        }
    }
}
