package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.MerchantOpeningHours;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MerchantOpeningHoursRepo extends CrudRepository<MerchantOpeningHours,Integer> {
    Iterable<MerchantOpeningHours> findByMerchantId(UUID merchantId);
}