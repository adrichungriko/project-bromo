package com.bca.bit.bromo.mobile.entity.request;

import java.util.List;

import lombok.Data;

@Data
public class InsertUserInterestRequest {
	
	private String customer_id;
	private List<String> list_user_interest_id;

}
