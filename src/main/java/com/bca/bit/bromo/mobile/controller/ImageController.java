package com.bca.bit.bromo.mobile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bca.bit.bromo.mobile.service.LoadImageService;

@RestController
@RequestMapping("/mobile")
public class ImageController {
	
	@Autowired
	private LoadImageService loadImgSvc;

	@ResponseBody
	@GetMapping(value="/load/image/{fileName}",produces = {MediaType.IMAGE_PNG_VALUE,MediaType.IMAGE_JPEG_VALUE})
	public Resource getImageMerchantAsResource(@PathVariable String fileName) {
	   return loadImgSvc.loadFileAsResource(fileName);
	}
}
