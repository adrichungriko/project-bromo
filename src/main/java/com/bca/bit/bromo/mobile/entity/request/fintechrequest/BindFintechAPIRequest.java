package com.bca.bit.bromo.mobile.entity.request.fintechrequest;

import lombok.Data;

@Data
public class BindFintechAPIRequest {
    private String fintech_id;
    private String fintech_name;
    private String app_code;
    private String bind_app_cust_id;

    public BindFintechAPIRequest(String fintech_id, String fintech_name, String app_code, String bind_app_cust_id) {
        this.fintech_id = fintech_id;
        this.fintech_name = fintech_name;
        this.app_code = app_code;
        this.bind_app_cust_id = bind_app_cust_id;
    }
}
