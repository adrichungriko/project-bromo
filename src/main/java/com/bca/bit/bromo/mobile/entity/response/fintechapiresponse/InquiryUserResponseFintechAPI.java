package com.bca.bit.bromo.mobile.entity.response.fintechapiresponse;

import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
public class InquiryUserResponseFintechAPI extends BaseResponse{

	private InquiryUserResponseFintechAPIOutputSchema output_schema;

	public InquiryUserResponseFintechAPI(String error_code) {
		super(error_code);
	}
	
	public InquiryUserResponseFintechAPI(String error_code,String fintech_id,String fintech_name,String customer_name,String customer_id) {
		super(error_code);
		output_schema = new InquiryUserResponseFintechAPIOutputSchema(fintech_id,fintech_name,customer_name,customer_id);
	}
	
	@Setter @Getter @ToString
	public class InquiryUserResponseFintechAPIOutputSchema {
		
		public InquiryUserResponseFintechAPIOutputSchema(String fintech_id,String fintech_name,String customer_name,String customer_id) {
			this.fintech_id = fintech_id;
			this.fintech_name = fintech_name;
			this.customer_id = customer_id;
			this.customer_name = customer_name;
		}
		
		private String fintech_id;
		private String fintech_name;
		private String customer_name;
		private String customer_id;
	}
	
}
