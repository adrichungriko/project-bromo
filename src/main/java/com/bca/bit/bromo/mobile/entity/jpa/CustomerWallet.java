package com.bca.bit.bromo.mobile.entity.jpa;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Data
@Table(name = "ms_customer_wallet", schema = "app_bromo")
public class CustomerWallet {

    @Id
    @Column(name = "customer_id")
    private UUID customerID;

    @Column(name = "ovo_id")
    private String ovoID;

    @Column(name = "dana_id")
    private String danaID;

    @Column(name = "gopay_id")
    private String gopayID;

    @Column(name = "sakuku_id")
    private String sakukuID;
}
