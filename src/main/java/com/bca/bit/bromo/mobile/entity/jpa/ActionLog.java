package com.bca.bit.bromo.mobile.entity.jpa;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

@Entity
@Data
@Table(name = "ms_action_log", schema = "app_bromo")
public class ActionLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "action_log_id")
    private int actionLogId;

    @Column(name = "promo_id")
    private String promoId;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "action")
    private String action;

    @Column(name = "search")
    private String search;

    @CreationTimestamp
    @Column(name = "date_created")
    private Timestamp dateCreated;
}
