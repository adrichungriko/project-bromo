package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.ActionLog;
import com.bca.bit.bromo.mobile.entity.jpa.LoginSession;
import com.bca.bit.bromo.mobile.entity.jpa.RequestOTP;
import com.bca.bit.bromo.mobile.entity.jpa.TrxPromoUsed;
import com.bca.bit.bromo.mobile.repo.ActionLogRepo;
import com.bca.bit.bromo.mobile.repo.LoginSessionRepo;
import com.bca.bit.bromo.mobile.repo.RequestOTPRepo;
import com.bca.bit.bromo.mobile.repo.TrxPromoUsedRepo;
import com.bca.bit.bromo.utility.common.TimeDifference;
import com.bca.bit.bromo.utility.configuration.HashCode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@EnableScheduling
public class DeleteService {

    private static final String logName = "DeleteService";

    @Autowired
    private TrxPromoUsedRepo trxPromoUsedRepo;

    @Autowired
    private RequestOTPRepo requestOTPRepo;

    @Autowired
    private TimeDifference timeDifference;

    @Autowired
    private HashCode hashCode;

    @Autowired
    private ActionLogRepo actionLogRepo;

    @Autowired
    private LoginSessionRepo loginSessionRepo;

    @Scheduled(cron = "0 0 23 1 * *")
    public void doDeleteTrxUsed(){
        List<TrxPromoUsed> trxPromoUsedList = trxPromoUsedRepo.findAll();
        org.apache.log4j.Logger logger = Logger.getLogger(logName);
        boolean verify;
        long difference = 31556952000L;
        String code = hashCode.getHashCode();
        logger.info("------------------------------------ Start Delete Trx Service -----------------------------");
        try {
            for (TrxPromoUsed trxPromoUsed : trxPromoUsedList) {
                verify = timeDifference.doTimeDifference(logName, code, trxPromoUsed.getDateUsed(), difference);
                if (verify) {
                    trxPromoUsedRepo.delete(trxPromoUsed);
                }
            }
        }catch (Exception ex){
            logger.error(code + "Get error: " + ex.getMessage());
        }

        logger.info("-------------------------------- Finish Delete Trx Service ----------------------------------");
    }

    @Scheduled(cron = "0 5 23 1 * *")
    public void doDeleteRequestOTP(){
        List<RequestOTP> requestOTPList = requestOTPRepo.findAll();
        org.apache.log4j.Logger logger = Logger.getLogger(logName);
        boolean verify;
        long difference = 3600000;
        String code = hashCode.getHashCode();
        logger.info("------------------------------------ Start Delete Request OTP Service -----------------------------");
        try {
            for (RequestOTP requestOTP : requestOTPList) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
                Date parsedDate = dateFormat.parse(requestOTP.getOtpCreated());
                Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
                verify = timeDifference.doTimeDifference(logName, code, timestamp, difference);
                if (verify) {
                    requestOTPRepo.delete(requestOTP);
                }
            }
        }catch (Exception ex){
            logger.error(code + "Get error: " + ex.getMessage());
        }

        logger.info("-------------------------------- Finish Delete Request OTP Service ----------------------------------");
    }

    @Scheduled(cron = "0 10 23 1 * *")
    public void doDeleteLogginSession(){
        List<LoginSession> loginSessionList = loginSessionRepo.findAll();
        Logger logger = Logger.getLogger(logName);
        boolean verify;
        long difference = 86400000;
        String code = hashCode.getHashCode();
        logger.info("------------------------------------ Start Delete Login Session Service -----------------------------");
        try {
            for (LoginSession loginSession : loginSessionList) {
                verify = timeDifference.doTimeDifference(logName, code, loginSession.getDateLogin(), difference);
                if (verify) {
                    loginSessionRepo.delete(loginSession);
                }
            }
        }catch (Exception ex){
            logger.error(code + "Get error: " + ex.getMessage());
        }

        logger.info("-------------------------------- Finish Delete Login Session Service ----------------------------------");
    }

    @Scheduled(cron = "0 15 23 1 * *")
    public void doDeleteActionLog(){
        List<ActionLog> actionLogList = actionLogRepo.findAll(); org.apache.log4j.Logger logger = Logger.getLogger(logName);
        boolean verify;
        long difference = 31556952000L;
        String code = hashCode.getHashCode();
        logger.info("------------------------------------ Start Delete Action Log Service -----------------------------");
        try {
            for (ActionLog actionLog : actionLogList) {
                verify = timeDifference.doTimeDifference(logName, code, actionLog.getDateCreated(), difference);
                if (verify) {
                    actionLogRepo.delete(actionLog);
                }
            }
        }catch (Exception ex){
            logger.error(code + "Get error: " + ex.getMessage());
        }

        logger.info("-------------------------------- Finish Delete Action Log Service ----------------------------------");

    }
}
