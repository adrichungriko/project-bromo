package com.bca.bit.bromo.mobile.service;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

@Service
public class LoadImageService {

	@Value("${path.image.merchant}")
	private String pathImageMerchant;

	@Value("${path.image.promo}")
	private String pathImagePromo;
	
	@Value("${path.image.category}")
	private String pathImageCategory;
	
	@Value("${path.image.avatar}")
	private String pathImageAvatar;

	public Resource loadFileAsResource(String fileName) {

		Path fileStorageLocation;
		try {

			if (fileName.contains("merchant_")) {
				fileStorageLocation = Paths.get(pathImageMerchant).toAbsolutePath().normalize();
			} else if (fileName.contains("promo_")) {
				fileStorageLocation = Paths.get(pathImagePromo).toAbsolutePath().normalize();
			} else if (fileName.contains("category_")) {
				fileStorageLocation = Paths.get(pathImageCategory).toAbsolutePath().normalize();
			} else if (fileName.contains("avatar_")) {
				fileStorageLocation = Paths.get(pathImageAvatar).toAbsolutePath().normalize();
			} else {
				throw new Exception("Flag Image doesn't match");
			}

			Path filePath = fileStorageLocation.resolve(fileName.substring(fileName.indexOf('_')+1)).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				System.out.println("File Not Found");
				return null;
			}
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
}
