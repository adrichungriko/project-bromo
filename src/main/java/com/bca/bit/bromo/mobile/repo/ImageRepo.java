package com.bca.bit.bromo.mobile.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.bca.bit.bromo.mobile.entity.jpa.Image;

public interface ImageRepo extends PagingAndSortingRepository<Image, String> {

	Image findByKeyImage(String keyImage);
	
}
