package com.bca.bit.bromo.mobile.entity.model;

import lombok.Data;

@Data
public class CategoryMerchantResponseModel {
    private Integer categoryId;
    private String categoryName;
}
