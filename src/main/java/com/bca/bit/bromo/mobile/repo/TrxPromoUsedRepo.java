package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.TrxPromoUsed;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;
import java.util.UUID;



public interface TrxPromoUsedRepo extends PagingAndSortingRepository<TrxPromoUsed, Integer> {
    TrxPromoUsed findByTransID(int transID);
    List<TrxPromoUsed> findByCustomerID(UUID customerId);
    List<TrxPromoUsed> findAll();
    TrxPromoUsed findByIsUsed(UUID rand);
}
