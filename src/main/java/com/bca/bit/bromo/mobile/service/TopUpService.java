package com.bca.bit.bromo.mobile.service;


import com.bca.bit.bromo.mobile.entity.jpa.Customer;
import com.bca.bit.bromo.mobile.entity.request.backendrequest.BackEndTopUpRequest;
import com.bca.bit.bromo.mobile.entity.request.fintechrequest.BindFintechAPIRequest;
import com.bca.bit.bromo.mobile.entity.request.fintechrequest.TopUpAPIRequest;
import com.bca.bit.bromo.mobile.entity.response.TopUpResponse;
import com.bca.bit.bromo.mobile.repo.CustomerRepo;
import com.bca.bit.bromo.utility.configuration.Constant;
import com.bca.bit.bromo.utility.configuration.HashCode;
import com.bca.bit.bromo.utility.configuration.URL;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.UUID;

@Service
public class TopUpService {
    static final Logger logger = Logger.getLogger("TopUpService");

    @Autowired
    HashCode hashCode;

    @Autowired
    CustomerRepo customerRepo;


    public TopUpResponse doTopUp(String customerID, int debitAmount, String fintechID){
        TopUpResponse topUpResponse = new TopUpResponse();
        TopUpAPIRequest topUpAPIRequest = new TopUpAPIRequest();
        Constant constant = new Constant();
        URL url = new URL();
        RestTemplate restTemplate = new RestTemplate();
        Gson gson = new Gson();
        String code = hashCode.getHashCode();
        String cardNo = "";
        String virtualAccount = constant.getOVOID() + fintechID;
        Customer customer = new Customer();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<TopUpAPIRequest> requestEntity;
        HttpEntity<String> response;
        BackEndTopUpRequest backEndTopUpRequest = new BackEndTopUpRequest();

        long start = System.currentTimeMillis();
        logger.info("------------------ Start Verification Service ---------------");
        logger.debug(code + "Get Customer data from database");

        customer = customerRepo.findByCustomerID(UUID.fromString(customerID));

        logger.debug(code + "Get Cred No from database");

        cardNo = customer.getCardNumber();

        logger.debug(code + "Check if its possible to do top up to core bank");

        backEndTopUpRequest.setAmountDebit(debitAmount);
        backEndTopUpRequest.setCard(cardNo);
        backEndTopUpRequest.setVirtualAccount(virtualAccount);

        HttpEntity<BackEndTopUpRequest> requestHttpEntity = new HttpEntity<>(backEndTopUpRequest, headers);
        UriComponentsBuilder corebankBuilder = UriComponentsBuilder.fromUriString(url.getCoreBankURL()).path("/corebank/inquiry/balance");

        response = restTemplate.exchange(corebankBuilder.toUriString(), HttpMethod.PUT, requestHttpEntity, String.class);

        logger.debug(code + "checking the output: " + response.getBody());

        topUpResponse = gson.fromJson(response.getBody(),TopUpResponse.class);

        logger.debug(code + "Checking the response: " + topUpResponse.getOutput_schema());

        if (topUpResponse.getOutput_schema().getStatus().equals("Success")){
            logger.debug(code + "Top Up Successfull");
            logger.debug(code + "Give Notification to Fintech");

            //hit api fintech buat notify top up berhasil
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url.getFintechURL()).path("/fintech/inquiry").path("/topup");
            topUpAPIRequest.setAmount(debitAmount);
            topUpAPIRequest.setFintech_id(fintechID);

            requestEntity = new HttpEntity<>(topUpAPIRequest, headers);

            response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT,requestEntity, String.class);

            topUpResponse = gson.fromJson(response.getBody(), TopUpResponse.class);

        }else {
            logger.debug(code + "Top Up Gagal");
            topUpResponse = new TopUpResponse(topUpResponse.getOutput_schema().getStatus(), "BIT-202");
        }

        logger.info("------------------- Finish Top Up Service -------------------");
        logger.info("Service Time: " + (System.currentTimeMillis() - start));

        return topUpResponse;
    }
}
