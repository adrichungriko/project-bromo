package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class BindFintechRequest {
    private String fintech_id;
    private String customer_id;
    private String fintech_name;
}
