package com.bca.bit.bromo.mobile.entity.jpa;

import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Table(name = "ms_category_merchant", schema = "app_bromo")
@Entity
@Data
public class Category {
	
	@Id
	@Column(name="category_id")	
	private int categoryId;
	
	@Column(name="category_name")
	private String categoryName;

	@Column(name="parent_category")
	private Integer parentCategory;
	
	@ManyToMany
    @JoinTable(
      schema = "app_bromo",
      name = "merchant_category", 
      joinColumns = @JoinColumn(name = "category_id"), 
      inverseJoinColumns = @JoinColumn(name = "merchant_id"))
    private List<Merchant> listMerchantInCategory;

}
