package com.bca.bit.bromo.mobile.entity.model;

import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.UUID;

@Data
public class SearchResponseModel {
    private UUID merchantId;
    private String merchantName;
    private String merchantImage;
    private Promo promo;
    private ArrayList<Category> category;

    @Data
    public static class Category{
        private Integer categoryId;
        private String categoryName;
    }

    @Data
    public static class Promo{
        private UUID promoId;
        private String promoName;
        private Double cashbackPercent;
    }
}
