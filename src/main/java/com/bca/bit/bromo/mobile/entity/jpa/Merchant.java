package com.bca.bit.bromo.mobile.entity.jpa;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "ms_merchant", schema = "app_bromo")
public class Merchant {
	
	@Id
    @GeneratedValue(generator = "uuid")
    @Column(name = "merchant_id")
    private UUID merchantId;

    @Column(name = "merchant_name")
    private String merchantName;

    @Column(name = "merchant_address1")
    private String merchantAddress1;

    @Column(name = "merchant_address2")
    private String merchantAddress2;

    @Column(name = "merchant_address3")
    private String merchantAddress3;

    @Column(name = "merchant_address4")
    private String merchantAddress4;

    @Column(name = "merchant_longitude")
    private String merchantLongitude;

    @Column(name = "merchant_latitude")
    private String merchantLatitude;

    @Column(name = "merchant_city")
    private String merchantCity;

    @Column(name = "merchant_contact_person")
    private String merchantContactPerson;

    @Column(name = "merchant_primary_phone")
    private String merchantPrimaryPhone;

    @Column(name = "merchant_status_cd")
    private String merchantStatusCd;

    @Column(name = "merchant_status_desc")
    private String merchantStatusDesc;

    @Column(name = "merchant_sic_cd")
    private String merchantSicCd;

    @Column(name = "merchant_sic_nm")
    private String merchantSicNm;

    @Column(name = "merchant_owner")
    private String merchantOwner;

    @Column(name = "merchant_zip_cd")
    private String merchantZipCd;

    @Column(name = "merchant_kecamatan")
    private String merchantKecamatan;

    @Column(name = "merchant_kelurahan")
    private String merchantKelurahan;

    @Column(name = "merchant_type")
    private String merchantType;

    @Column(name = "merchant_central_id")
    private String merchantCentralId;

    @Column(name = "merchant_image")
    private String merchantImage;
 
}