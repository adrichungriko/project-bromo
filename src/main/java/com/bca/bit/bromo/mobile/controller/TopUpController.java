package com.bca.bit.bromo.mobile.controller;

import com.bca.bit.bromo.mobile.entity.request.TopUpRequest;
import com.bca.bit.bromo.mobile.entity.request.backendrequest.BackEndTopUpRequest;
import com.bca.bit.bromo.mobile.entity.response.TopUpResponse;
import com.bca.bit.bromo.mobile.service.TopUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mobile")
public class TopUpController {

    @Autowired
    private TopUpService topUpService;

    @PutMapping("/topup")
    public TopUpResponse doTopUp(@RequestBody TopUpRequest topUpRequest){
        String customerId = topUpRequest.getCustomer_id().isEmpty() ? "" : topUpRequest.getCustomer_id();
        TopUpResponse topUpResponse;
        int amount = topUpRequest.getAmount() == 0 ? 0 : topUpRequest.getAmount();
        String fintechId = topUpRequest.getFintech_id().isEmpty() ? "" : topUpRequest.getFintech_id();
        if (customerId == "" || amount == 0 || fintechId == ""){
            topUpResponse = new TopUpResponse("BIT-404");
        }else{
            try {
                topUpResponse = topUpService.doTopUp(customerId, amount, fintechId);
            }catch (Exception ex){
                topUpResponse = new TopUpResponse("BIT-499", ex.getMessage(), true);
            }
        }

        return topUpResponse;
    }
}
