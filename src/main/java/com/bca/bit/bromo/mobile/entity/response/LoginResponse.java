package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

@Data
public class LoginResponse extends BaseResponse {
    public LoginResponseOutputSchema output_schema;

    public LoginResponse(){}
    public LoginResponse(String customerID, boolean verify, String error_code) {
        super(error_code);
        this.output_schema = new LoginResponseOutputSchema(customerID, verify);
    }
    public LoginResponse(String errorCode){
        super(errorCode);
    }
    public LoginResponse(String errorCode, String message){
        super(errorCode, message);
    }

    @Data
    public static class LoginResponseOutputSchema{
        private String customer_id;
        private boolean verify;

        public LoginResponseOutputSchema(String customer_id, boolean verify) {
            this.customer_id = customer_id;
            this.verify = verify;
        }
    }
}
