package com.bca.bit.bromo.mobile.entity.response.fintechapiresponse;

import lombok.Data;

@Data
public class InquirySaldoResponseFintechAPI {
    private ErrorSchema error_schema;
    private OutputSchema output_schema;

    @Data
    public static class ErrorSchema{
        private String error_code;
        private ErrorMessage error_message;

        @Data
        public static class ErrorMessage{
            private String english;
            private String indonesian;
        }
    }

    @Data
    public static class OutputSchema{
        private String fintech_id;
        private String fintech_name;
        private String balance;
        private String fintech_cust_name;
    }
}
