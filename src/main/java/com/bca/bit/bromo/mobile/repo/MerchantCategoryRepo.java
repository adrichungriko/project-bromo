package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.jpa.MerchantCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public interface MerchantCategoryRepo extends CrudRepository<MerchantCategory, Integer> {
    /*@Query(value = "select mc from com.bca.bit.bromo.mobile.entity.jpa.MerchantCategory mc " +
            "where mp.merchantId in (:merchantId)")*/
    Iterable<MerchantCategory> findByMerchantId(UUID merchantId);
    Iterable<MerchantCategory> findDistinctByCategoryId(Integer catId);


    @Query("Select mc from com.bca.bit.bromo.mobile.entity.jpa.MerchantCategory mc "+
            "where mc.categoryId in (:catId)"
    )
    Iterable<MerchantCategory> findAllByCategoryId(@Param("catId") ArrayList<Integer> catId);
    

}
