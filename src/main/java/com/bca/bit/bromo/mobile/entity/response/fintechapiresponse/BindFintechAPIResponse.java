package com.bca.bit.bromo.mobile.entity.response.fintechapiresponse;

import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

@Data
public class BindFintechAPIResponse extends BaseResponse {
    private OutputSchema output_schema;

    public BindFintechAPIResponse(){}
    public BindFintechAPIResponse (String successFlag, String statusCustomer, String errorCode){
        super(errorCode);

        this.output_schema = new OutputSchema(successFlag, statusCustomer);

    }

    @Data
    public static class OutputSchema{
        private String success_flag;
        private String status_customer;

        public OutputSchema(String success_flag, String status_customer) {
            this.success_flag = success_flag;
            this.status_customer = status_customer;
        }
    }
}
