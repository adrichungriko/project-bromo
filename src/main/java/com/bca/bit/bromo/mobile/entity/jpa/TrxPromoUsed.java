package com.bca.bit.bromo.mobile.entity.jpa;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Data
@Table(name = "trx_promo_used", schema = "app_bromo")
public class TrxPromoUsed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trans_id")
    private int transID;

    @Column(name = "customer_id")
    private UUID customerID;

    @Column(name = "merchant_id")
    private UUID merchantID;

    @CreationTimestamp
    @Column(name = "date_used")
    private Timestamp dateUsed;

    @Column(name = "promo_id")
    private UUID promoID;

    @Column(name = "trx_id")
    @GeneratedValue(generator = "uuid")
    private UUID trxID;

    @Column(name = "is_used")
    private UUID isUsed;
}
