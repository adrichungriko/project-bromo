package com.bca.bit.bromo.mobile.entity.jpa;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
@Table(name="ms_user", schema = "app_bromo")
public class User {

    @Id
    @Column(name="user_credential")
    private String userCredential;

    @Column(name = "password")
    private String passwordHash;

    @Column(name = "user_role")
    private String userRole;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created")
    private Date dateCreated;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_updated")
    private Date dateUpdated;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "user_detail_id")
    private UUID userDetailId;

    @Column(name = "is_active")
    private boolean isActive;

}
