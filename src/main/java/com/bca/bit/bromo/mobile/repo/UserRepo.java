package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface UserRepo extends PagingAndSortingRepository<User, UUID> {

    User findByUserCredential(String userCredential);

    User findByuserDetailId(UUID userDetailId);
}
