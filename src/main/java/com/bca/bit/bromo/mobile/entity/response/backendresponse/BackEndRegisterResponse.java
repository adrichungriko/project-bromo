package com.bca.bit.bromo.mobile.entity.response.backendresponse;

import lombok.Data;

@Data
public class BackEndRegisterResponse {
    private ErrorSchema error_schema;
    private OutputSchema output_schema;

    @Data
    public static class ErrorSchema{
        private String errorCode;
        private ErrorMessage errorMessage;

        @Data
        public static class ErrorMessage{
            private String english;
            private String indonesian;
        }
    }

    @Data
    public static class OutputSchema{
        private String card_no;
        private String status;
        private String name;
        private String gender;
        private String phone_no;
        private String account_no;

    }
}
