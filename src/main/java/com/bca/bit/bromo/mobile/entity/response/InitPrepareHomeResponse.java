package com.bca.bit.bromo.mobile.entity.response;

import java.util.ArrayList;
import java.util.List;

import com.bca.bit.bromo.mobile.entity.jpa.Category;
import com.bca.bit.bromo.mobile.entity.model.SearchResponseModel;

import lombok.Data;

@Data
public class InitPrepareHomeResponse {
	
	private ErrorSchema error_schema;
    private InitPrepareHomeOutputSchema output_schema;

    @Data
    public static class ErrorSchema{
        private String error_code;
        private ErrorMessage error_message;

        @Data
        public static class ErrorMessage{
            private String english;
            private String indonesian;
        }
    }

    @Data
    public static class InitPrepareHomeOutputSchema{
        private List<MyWallet> list_my_wallet;
        private List<MySlider> list_my_slider;
    	private String merchant_category_view_card;
    	private String merchant_category_view_card_id;
    	private String merchant_category_view_card_picture;
        private List<MyViewCard> list_my_view_card;
        private String merchant_category_view_card_2;
        private String merchant_category_view_card_2_id;
        private String merchant_category_view_card_2_picture;
        private List<MyViewCard> list_my_view_card_2;
        private String merchant_category_view_card_recommend;
        private String merchant_category_view_card_recommend_picture;
        private List<MyViewCard> list_my_view_card_recommend;
        
        @Data
        public static class MyWallet{
        	private String fintech_logo_url;
        	private String fintech_name;
        	private String fintech_cust_name;
        	private String balance;
        	private String fintech_cust_id;
        }
        
        @Data
        public static class MySlider{
        	private String slider_image_url;
        	private String promo_name;
        	private String promo_id;
        }
        
        @Data
        public static class MyViewCard{
        	private String merchant_logo_url;
        	private String merchant_id;
        	private String merchant_name;
        	private List<CategoryMerchant> merchant_category;
        	
            @Data
            public static class CategoryMerchant{
                private Integer category_id;
                private String category_name;
            }
        }
        
    }

}
