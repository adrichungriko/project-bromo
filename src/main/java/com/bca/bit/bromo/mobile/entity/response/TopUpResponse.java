package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

@Data
public class TopUpResponse extends BaseResponse {
    private TopUpResponseOutputSchema output_schema;

    public TopUpResponse(String errorCode){
        super(errorCode);
    }
    public TopUpResponse(String status, String errorCode){
        super(errorCode);
        output_schema = new TopUpResponseOutputSchema(status);
    }
    public TopUpResponse(String errorCode, String message, boolean verify){
        super(errorCode, message);
    }

    public TopUpResponse(){}

    @Data
    public static class TopUpResponseOutputSchema{
        private String status;

        public TopUpResponseOutputSchema(String status) {
            this.status = status;
        }
    }
}
