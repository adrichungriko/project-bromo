package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.CategoryMerchant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryMerchantRepo extends CrudRepository<CategoryMerchant, Integer> {
    Iterable<CategoryMerchant> findByCategoryNameIgnoreCaseContaining(String catName);
    Iterable<CategoryMerchant> findByParentCategory(String catParent);
}
