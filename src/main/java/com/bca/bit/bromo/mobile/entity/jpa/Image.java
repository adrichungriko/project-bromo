package com.bca.bit.bromo.mobile.entity.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="ms_image",schema="app_bromo")
public class Image {
	
	@Id
	@Column(name="key_image")
	private String keyImage;
	
	@Column(name="url_image")
	private String urlImage;

}
