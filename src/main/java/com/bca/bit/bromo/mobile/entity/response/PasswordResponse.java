package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

@Data
public class PasswordResponse extends BaseResponse {
    private PasswordResponseOutputSchema output_schema;

    public PasswordResponse(){}
    public PasswordResponse(boolean verify, String customerID, String error_code){
        super(error_code);
        output_schema = new PasswordResponseOutputSchema(customerID, verify);
    }
    public PasswordResponse(String errorCode){
        super(errorCode);
    }

    public PasswordResponse(String errorCode, String message){
        super(errorCode, message);
    }

    @Data
    public static class PasswordResponseOutputSchema{
        public PasswordResponseOutputSchema(String customer_id, boolean verify) {
            this.customer_id = customer_id;
            this.verify = verify;
        }

        private String customer_id;
        private boolean verify;
    }
}
