package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.LoginSession;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface LoginSessionRepo extends PagingAndSortingRepository<LoginSession, String> {

    List<LoginSession> findAll();

}
