package com.bca.bit.bromo.mobile.entity.jpa;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@Table(name = "ms_request_otp", schema = "app_bromo")
public class RequestOTP {

    @Id
    @GeneratedValue(generator = "uuid")
    @Column(name = "request_id")
    private UUID requestID;

    @Column(name = "otp_hash")
    private String otpHash;

    @Column(name = "otp_created")
    private String otpCreated;

    @Column(name = "customer_id")
    private UUID customerID;
}
