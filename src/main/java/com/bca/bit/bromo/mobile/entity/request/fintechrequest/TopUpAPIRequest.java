package com.bca.bit.bromo.mobile.entity.request.fintechrequest;

import lombok.Data;

@Data
public class TopUpAPIRequest {
    private String fintech_id;
    private int amount;
}
