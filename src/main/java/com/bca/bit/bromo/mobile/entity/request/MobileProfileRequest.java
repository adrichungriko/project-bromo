package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class MobileProfileRequest {
    private String customer_id;
}
