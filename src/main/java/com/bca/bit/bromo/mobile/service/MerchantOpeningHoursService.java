package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.MerchantOpeningHours;
import com.bca.bit.bromo.mobile.repo.MerchantOpeningHoursRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;

@Service
public class MerchantOpeningHoursService {
    @Autowired
    MerchantOpeningHoursRepo merchantOpeningHoursRepo;

    public ArrayList<MerchantOpeningHours> getByMerchantId(UUID merchantId){
        ArrayList<MerchantOpeningHours> l_openHours = new ArrayList<>();
        if(merchantOpeningHoursRepo.findByMerchantId(merchantId).iterator().hasNext()){
            l_openHours.addAll((ArrayList) merchantOpeningHoursRepo.findByMerchantId(merchantId));
        }

        return l_openHours;
    }
}
