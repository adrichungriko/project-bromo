package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class LoginRequest {
    private String card_no;
    private String device_data;
}
