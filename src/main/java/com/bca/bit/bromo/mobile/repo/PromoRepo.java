package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.jpa.Promo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

public interface PromoRepo extends CrudRepository<Promo, UUID> {

	List<Promo> findByIsShowSlider(String isShow);
	Promo findByPromoId(UUID promoId);
	Iterable<Promo> findByPromoNameIgnoreCaseContaining(String promoName);
}
