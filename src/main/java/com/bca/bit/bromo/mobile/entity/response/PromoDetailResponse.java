package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.mobile.entity.jpa.Promo;
import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;
import org.apache.kafka.common.protocol.types.Field;

@Data
public class PromoDetailResponse extends BaseResponse {
    private PromoDetailResponseOutputSchema output_schema;

    public PromoDetailResponse(Promo promo, String errorCode){
        super(errorCode);
        output_schema = new PromoDetailResponseOutputSchema(promo);
    }

    public PromoDetailResponse(String errorCode){
        super(errorCode);
    }

    @Data
    public static class PromoDetailResponseOutputSchema{
        private Promo promo_detail;

        public PromoDetailResponseOutputSchema(Promo promo_detail) {
            this.promo_detail = promo_detail;
        }
    }
}
