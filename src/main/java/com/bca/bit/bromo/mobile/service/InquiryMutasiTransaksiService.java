package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.response.InquiryMutasiTransaksiResponse;
import com.bca.bit.bromo.mobile.entity.jpa.CustomerWallet;
import com.bca.bit.bromo.mobile.entity.jpa.User;
import com.bca.bit.bromo.mobile.repo.CustomerWalletRepo;
import com.bca.bit.bromo.mobile.repo.UserRepo;
import com.bca.bit.bromo.utility.configuration.HashCode;
import com.bca.bit.bromo.utility.configuration.URL;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.UUID;

@Service
public class InquiryMutasiTransaksiService {

    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("InquiryMutasiCustomer");

    @Autowired
    private CustomerWalletRepo customerWalletRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private URL url;

    @Autowired
    private HashCode hashCode;

    public InquiryMutasiTransaksiResponse doMutasi(String customerID, String fintech_name){
        InquiryMutasiTransaksiResponse inquiryMutasiTransaksiResponse = new InquiryMutasiTransaksiResponse();
        InquiryMutasiTransaksiResponse.InquiryMutasiTransaksiOutputSchema outputSchema = new InquiryMutasiTransaksiResponse.InquiryMutasiTransaksiOutputSchema();
        InquiryMutasiTransaksiResponse.ErrorSchema errorSchema = new InquiryMutasiTransaksiResponse.ErrorSchema();
        InquiryMutasiTransaksiResponse.ErrorSchema.ErrorMessage errorMessage = new InquiryMutasiTransaksiResponse.ErrorSchema.ErrorMessage();
        RestTemplate restTemplate = new RestTemplate();
        String fintech_URL = url.getFintechURL();
        Gson gson = new Gson();
        User user = new User();
        CustomerWallet customerWallet = new CustomerWallet();
        String fintechID ="";
        String code = hashCode.getHashCode();
        long start = System.currentTimeMillis();

        logger.info("--------------- Start Inquiry Mutasi Transaksi Service ----------------");
        logger.debug(code + "Get User Detail using customer ID");

        customerWallet = customerWalletRepo.findByCustomerID(UUID.fromString(customerID));


        logger.debug(code + "Check Fintech yang mau di inquiry Mutasi: " + fintech_name);
        logger.debug(code + "Ambil fintech ID dari Database");

        if(fintech_name.equals("DANA")){
            fintechID = customerWallet.getDanaID().isEmpty() ? "" : customerWallet.getDanaID();
        }else if (fintech_name.equals("OVO")){
            fintechID = customerWallet.getOvoID().isEmpty() ? "" : customerWallet.getOvoID();
        }else if (fintech_name.equals("Gopay")){
            fintechID = customerWallet.getGopayID().isEmpty() ? "" : customerWallet.getGopayID();
        }else if (fintech_name.equals("Sakuku")){
            fintechID = customerWallet.getSakukuID().isEmpty() ? "" : customerWallet.getSakukuID();
        }else {
            errorMessage.setEnglish("Wrong Fintech Name");
            errorMessage.setIndonesian("Nama Fintech Salah");
            errorSchema.setError_code("BIT-300");
            errorSchema.setError_message(errorMessage);
            inquiryMutasiTransaksiResponse.setError_schema(errorSchema);
            return inquiryMutasiTransaksiResponse;
        }

        if (fintechID == ""){
            errorMessage.setIndonesian("Fintech ID Tidak ada");
            errorMessage.setEnglish("Unknown Fintech ID");
            errorSchema.setError_message(errorMessage);
            errorSchema.setError_code("BIT-300");
            inquiryMutasiTransaksiResponse.setError_schema(errorSchema);
            return inquiryMutasiTransaksiResponse;
        }
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(fintech_URL).path("/fintech").path("/inquiry").path("/statement").queryParam("fintech_id", fintechID).queryParam("fintech_name", fintech_name);

        logger.debug(code + "Get Data from API Fintech");

        ResponseEntity<String> response = restTemplate.getForEntity(builder.toUriString(), String.class);

        logger.debug(code + "Checking output: " + response.getBody());

        inquiryMutasiTransaksiResponse = gson.fromJson(response.getBody(), InquiryMutasiTransaksiResponse.class);
        outputSchema = inquiryMutasiTransaksiResponse.getOutput_schema();
        outputSchema.setCustomer_id(customerID);
        inquiryMutasiTransaksiResponse.setOutput_schema(outputSchema);

        logger.info("--------------- Finish Inquiry Mutasi Transaksi Service ---------------");
        logger.info("Service Time: " + (System.currentTimeMillis() - start));

        return inquiryMutasiTransaksiResponse;

    }

    public InquiryMutasiTransaksiResponse doDummyMutasi(String customerID, String fintechName){
        InquiryMutasiTransaksiResponse inquiryMutasiTransaksiResponse = new InquiryMutasiTransaksiResponse();
        String url = "https://dummy-fintech.herokuapp.com/fintech/inquiry/statement";
        RestTemplate restTemplate = new RestTemplate();
        Gson gson = new Gson();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url).queryParam("fintech_id" , "081220192019").queryParam("fintech_name" , "OVO");
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(builder.toUriString(), String.class);
        inquiryMutasiTransaksiResponse = gson.fromJson(responseEntity.getBody(), InquiryMutasiTransaksiResponse.class);

        return inquiryMutasiTransaksiResponse;
    }
}
