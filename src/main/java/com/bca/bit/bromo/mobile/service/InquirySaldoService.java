package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.response.fintechapiresponse.InquirySaldoResponseFintechAPI;
import com.bca.bit.bromo.mobile.entity.jpa.CustomerWallet;
import com.bca.bit.bromo.mobile.repo.CustomerWalletRepo;
import com.bca.bit.bromo.utility.configuration.HashCode;
import com.bca.bit.bromo.utility.configuration.URL;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bca.bit.bromo.mobile.entity.response.InquirySaldoResponse;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

@Service
public class InquirySaldoService {

    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("InquriySaldoUser");

    @Autowired
    private CustomerWalletRepo customerWalletRepo;

    @Autowired
    private HashCode hashCode;

    @Autowired
    private URL url;

    public InquirySaldoResponse doInquiry(String customerID){
        InquirySaldoResponseFintechAPI inquirySaldoResponseFintechAPI = new InquirySaldoResponseFintechAPI();
        InquirySaldoResponseFintechAPI.OutputSchema outputSchemaAPI = new InquirySaldoResponseFintechAPI.OutputSchema();
        InquirySaldoResponse inquirySaldoResponse = new InquirySaldoResponse();
        InquirySaldoResponse.InquirySaldoOutputSchema outputSchema = new InquirySaldoResponse.InquirySaldoOutputSchema();
        InquirySaldoResponse.InquirySaldoOutputSchema.balance balance = new InquirySaldoResponse.InquirySaldoOutputSchema.balance();
        Hashtable<String, String> hashtable = new Hashtable<>();
        List<InquirySaldoResponse.InquirySaldoOutputSchema.balance> resultList = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        InquirySaldoResponse.ErrorSchema errorSchema = new InquirySaldoResponse.ErrorSchema();
        InquirySaldoResponse.ErrorSchema.ErrorMessage errorMessage = new InquirySaldoResponse.ErrorSchema.ErrorMessage();

        Gson gson = new Gson();
        String code = hashCode.getHashCode();
        CustomerWallet customerWallet = new CustomerWallet();
        String URL = url.getFintechURL();
        long start = System.currentTimeMillis();
        String fintechID = "";
        String fintechName = "";

        logger.info("----------------- Start Inquiry Saldo Service -----------------");
        logger.debug(code + "Get User Detail Dari Database");

        customerWallet = customerWalletRepo.findByCustomerID(UUID.fromString(customerID));

        logger.debug(code + "Checking the user: " + customerWallet.getCustomerID());

        logger.debug(code + "Checking what Fintech User has Bind");

        fintechID = customerWallet.getDanaID() == null ? "" :  customerWallet.getDanaID();
        hashtable.put("DANA", fintechID);
        fintechID = customerWallet.getSakukuID() == null  ? "" :  customerWallet.getSakukuID();
        hashtable.put("Sakuku", fintechID);
        fintechID =  customerWallet.getGopayID() == null  ? "" :  customerWallet.getGopayID();
        hashtable.put("Gopay", fintechID);
        fintechID =  customerWallet.getOvoID() == null  ? "" :  customerWallet.getOvoID();
        hashtable.put("OVO", fintechID);

        logger.debug(code + "Inquiry Saldo dari Fintech API");

        if (hashtable.get("DANA") == ""){
            logger.debug(code + "Ga ke Bind Dana");
        }else {
            fintechName = "DANA";

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL).path("/fintech").path("/inquiry").path("/balance").queryParam("fintech_id", hashtable.get("DANA")).queryParam("fintech_name", fintechName);
            ResponseEntity<String> response = restTemplate.getForEntity(builder.toUriString(), String.class);

            logger.debug(code + "Checking output: " + response.getBody());

            inquirySaldoResponseFintechAPI = gson.fromJson(response.getBody(), InquirySaldoResponseFintechAPI.class);
            outputSchemaAPI = inquirySaldoResponseFintechAPI.getOutput_schema();
            balance.setFintech_id(outputSchemaAPI.getFintech_id());
            balance.setFintech_name(outputSchemaAPI.getFintech_name());
            balance.setBalance(outputSchemaAPI.getBalance());
            balance.setFintech_cust_name(outputSchemaAPI.getFintech_cust_name());
            resultList.add(balance);
        }

        if (hashtable.get("Sakuku") == ""){
            logger.debug(code + "Ga ke Bind Sakuku");
        }else {
            fintechName = "Sakuku";

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL).path("/fintech").path("/inquiry").path("/balance").queryParam("fintech_id", hashtable.get("Sakuku")).queryParam("fintech_name", fintechName);
            ResponseEntity<String> response = restTemplate.getForEntity(builder.toUriString(), String.class);

            logger.debug(code + "Checking output: " + response.getBody());

            inquirySaldoResponseFintechAPI = gson.fromJson(response.getBody(), InquirySaldoResponseFintechAPI.class);
            outputSchemaAPI = inquirySaldoResponseFintechAPI.getOutput_schema();

            balance = new InquirySaldoResponse.InquirySaldoOutputSchema.balance();
            balance.setFintech_id(outputSchemaAPI.getFintech_id());
            balance.setFintech_name(outputSchemaAPI.getFintech_name());
            balance.setBalance(outputSchemaAPI.getBalance());
            balance.setFintech_cust_name(outputSchemaAPI.getFintech_cust_name());
            resultList.add(balance);
        }

        if (hashtable.get("Gopay") == ""){
            logger.debug(code + "Ga ke Bind Gopay");
        }else {
            fintechName = "Gopay";

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL).path("/fintech").path("/inquiry").path("/balance").queryParam("fintech_id", hashtable.get("Gopay")).queryParam("fintech_name", fintechName);
            ResponseEntity<String> response = restTemplate.getForEntity(builder.toUriString(), String.class);

            logger.debug(code + "Checking output: " + response.getBody());

            inquirySaldoResponseFintechAPI = gson.fromJson(response.getBody(), InquirySaldoResponseFintechAPI.class);
            outputSchemaAPI = inquirySaldoResponseFintechAPI.getOutput_schema();

            balance = new InquirySaldoResponse.InquirySaldoOutputSchema.balance();
            balance.setFintech_id(outputSchemaAPI.getFintech_id());
            balance.setFintech_name(outputSchemaAPI.getFintech_name());
            balance.setBalance(outputSchemaAPI.getBalance());
            balance.setFintech_cust_name(outputSchemaAPI.getFintech_cust_name());
            resultList.add(balance);
        }

        if (hashtable.get("OVO") == ""){
            logger.debug(code + "Ga ke Bind OVO");
        }else {
            fintechName = "OVO";
            
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL).path("/fintech").path("/inquiry").path("/balance").queryParam("fintech_id", hashtable.get("OVO")).queryParam("fintech_name", fintechName);
            ResponseEntity<String> response = restTemplate.getForEntity(builder.toUriString(), String.class);

            logger.debug(code + "Checking output: " + response.getBody());

            inquirySaldoResponseFintechAPI = gson.fromJson(response.getBody(), InquirySaldoResponseFintechAPI.class);
            outputSchemaAPI = inquirySaldoResponseFintechAPI.getOutput_schema();

            balance = new InquirySaldoResponse.InquirySaldoOutputSchema.balance();
            balance.setFintech_id(outputSchemaAPI.getFintech_id());
            balance.setFintech_name(outputSchemaAPI.getFintech_name());
            balance.setBalance(outputSchemaAPI.getBalance());
            balance.setFintech_cust_name(outputSchemaAPI.getFintech_cust_name());
            resultList.add(balance);
        }

        logger.debug(code + "Checking the size of resultList: " + resultList.size());
        //logger.debug(code + "Check the data in resultList: " + resultList.get(0));

        if(resultList.size() >= 1){
            errorMessage.setEnglish("Success");
            errorMessage.setIndonesian("Berhasil");
            errorSchema.setError_code("BIT-200");
            errorSchema.setError_message(errorMessage);
            outputSchema.setCustomer_id(customerID);
            outputSchema.setSaldo(resultList);
            inquirySaldoResponse.setError_schema(errorSchema);
            inquirySaldoResponse.setOutput_schema(outputSchema);
        }else{
            errorMessage.setIndonesian("Tidak ada Fintech");
            errorMessage.setEnglish("No Fintech avaiable");
            errorSchema.setError_message(errorMessage);
            errorSchema.setError_code("BIT-300");
            inquirySaldoResponse.setError_schema(errorSchema);
        }

        logger.debug(code + "Checking the response: " + inquirySaldoResponse.getOutput_schema());

        logger.info("----------------- Finish Inquiry Saldo Service -----------------");
        logger.info("Service Time: " + (System.currentTimeMillis() - start));

        return inquirySaldoResponse;

    }

}
