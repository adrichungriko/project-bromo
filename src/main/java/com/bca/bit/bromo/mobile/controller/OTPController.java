package com.bca.bit.bromo.mobile.controller;

import com.bca.bit.bromo.mobile.entity.request.OTPRequest;
import com.bca.bit.bromo.mobile.entity.response.OTPResponse;
import com.bca.bit.bromo.mobile.service.OTPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Hashtable;
import java.util.UUID;

@RestController
@RequestMapping("/mobile")
public class OTPController {

    @Autowired
    private OTPService otpService;


    @PostMapping("/dummy/verify/otp")
    public OTPResponse doDummyOTP(@RequestBody OTPRequest otpRequest){
        OTPResponse otpResponse = new OTPResponse();
        otpResponse = otpService.doDummyOTP(otpRequest.getRequest_id(), otpRequest.getOtp());

        return otpResponse;
    }


    @PostMapping("/verify/otp")
    public OTPResponse doOTP(@RequestBody OTPRequest otpRequest){
        OTPResponse otpResponse = new OTPResponse();
        OTPResponse.ErrorSchema errorSchema = new OTPResponse.ErrorSchema();
        OTPResponse.ErrorSchema.ErrorMessage errorMessage = new OTPResponse.ErrorSchema.ErrorMessage();
        if (otpRequest.getRequest_id() == null || otpRequest.getRequest_id().isEmpty() || otpRequest.getOtp() == null || otpRequest.getOtp().isEmpty()){
            errorMessage.setEnglish("Missing input");
            errorSchema.setError_message(errorMessage);
            errorSchema.setError_code("BIT-404");
        }else{
            try {
                otpResponse = otpService.doOTP(otpRequest.getRequest_id(), otpRequest.getOtp());
            }catch (Exception ex){
                errorMessage.setEnglish(ex.getMessage());
                errorSchema.setError_code("BIT_499");
                errorSchema.setError_message(errorMessage);
            }
        }
        return otpResponse;
    }

    @PostMapping("/send/otp")
    public String doSendOTP(@RequestParam String phoneNum){
        Hashtable hashtable = otpService.doGenerateOTP(phoneNum, UUID.randomUUID().toString(), false);
        String test = hashtable.get("otp").toString();
        return test;
    }


}
