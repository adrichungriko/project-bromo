package com.bca.bit.bromo.mobile.entity.jpa;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Data
@Table(name = "ms_login_session", schema = "app_bromo")
public class LoginSession {

    @Id
    @Column(name = "session_id")
    private String sessionID;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "date_login")
    private Timestamp dateLogin;

    @Column(name = "date_logout")
    private Timestamp dateLogout;
}
