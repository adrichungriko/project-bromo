package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.jpa.Promo;
import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

import java.util.List;
import java.util.Optional;

@Data
public class MerchantPromoResponse extends BaseResponse {
    private MerchantPromoResponseOutputSchema output_schema;

    public MerchantPromoResponse(){}
    public MerchantPromoResponse(String merchantID, List<Promo> promoList, String errorCode){
        super(errorCode);
        this.output_schema = new MerchantPromoResponseOutputSchema(merchantID, promoList);
    }
    public MerchantPromoResponse(String errorCode){
        super(errorCode);
    }

    @Data
    public static class MerchantPromoResponseOutputSchema{
        private String merchant_id;
        private List<Promo> listPromo;

        public MerchantPromoResponseOutputSchema(String merchant_id, List<Promo> listPromo) {
            this.merchant_id = merchant_id;
            this.listPromo = listPromo;
        }
    }
}
