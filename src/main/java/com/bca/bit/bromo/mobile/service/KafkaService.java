package com.bca.bit.bromo.mobile.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaService {
	
	@Autowired
    private KafkaTemplate<String,String> kafkaTemplate;
	
	/*
	@Autowired
    private KafkaTemplate<String,ListAllTrxCustomer> kafkaTemplate2;*/

	public void putMessage(String topic,String listTrx) {
		kafkaTemplate.send(topic, listTrx);
	}
	
	/*
	public void putMessage2(String topic,ListAllTrxCustomer listTrx) {
		kafkaTemplate.send(topic, listTrx);
	}*/

}
