package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.Customer;
import com.bca.bit.bromo.mobile.entity.jpa.CustomerWallet;
import com.bca.bit.bromo.mobile.entity.jpa.User;
import com.bca.bit.bromo.mobile.entity.response.LoginResponse;
import com.bca.bit.bromo.mobile.entity.response.LogoutResponse;
import com.bca.bit.bromo.mobile.entity.response.backendresponse.BackEndRegisterResponse;
import com.bca.bit.bromo.mobile.repo.CustomerRepo;
import com.bca.bit.bromo.mobile.repo.CustomerWalletRepo;
import com.bca.bit.bromo.mobile.repo.UserRepo;
import com.bca.bit.bromo.utility.configuration.HashCode;
import com.bca.bit.bromo.utility.configuration.URL;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bca.bit.bromo.mobile.entity.response.RegisterResponse;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class MobileRegisterService {

    static org.apache.log4j.Logger logger = Logger.getLogger("MobileRegisterService");
    static String avatarMale = "avatar_male.png";
    static String avatarFemale = "avatar_female.png";

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private OTPService otpService;

    @Autowired
    private HashCode hashCode;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private CustomerWalletRepo customerWalletRepo;

    public String doGenerateCustID(String cred_no, String fullName, String deviceData, String phoneNum, String AccountNumber, String Gender) {
        String customerID = "";
        User user = new User();
        Customer customer = new Customer();
        CustomerWallet customerWallet = new CustomerWallet();
        Date date = new Date();
        long start = System.currentTimeMillis();
        String code = hashCode.getHashCode();

        logger.info("----------- Start Generate Cust ID Service ----------");
        logger.debug(code + "Check Database for all user");


        logger.debug(code + "Check User pernah terdaftar atau engga");
        Iterable<Customer> customerIterable = customerRepo.findAll();
        List<Customer> customerList = Lists.newArrayList(customerIterable);


        for (Customer output: customerList){
            String device = output.getDeviceData() == null ? "" : output.getDeviceData();

            if (output.getPhoneNumber().equals(phoneNum)){
                logger.error(code + "Phone number sudah terdaftar");
                customerID = "phoneNum";
                return customerID;
            }else if (output.getCardNumber().equals(cred_no)){
                logger.error(code + "Nomor Kartu Sudah terdaftar");
                customerID = "credNo";
                return customerID;
            } else if (device.equals(deviceData)){
                logger.error(code + "Device Sudah Terdaftar");
                customerID = "deviceData";
                return  customerID;
            }else if (output.getAccountNumber().equals(AccountNumber)){
                logger.error(code + "No Rek Sudah terdaftar");
                customerID = "AccountNumber";
                return customerID;
            }
        }

        logger.debug(code + "Get customerID: " + customerID);
        logger.debug(code + "Masukin ke database");

        customer.setAccountNumber(AccountNumber);
        customer.setCardNumber(cred_no);
        customer.setDateCreated(date);
        customer.setDateUpdated(date);
        customer.setDeviceData(deviceData);
        customer.setFullName(fullName);
        customer.setGender(Gender);
        if(Gender.equals("Male")){
            customer.setCustomerImage(avatarMale);
        }else if (Gender.equals("Female")){
            customer.setCustomerImage(avatarFemale);
        }else {
            logger.error(code + "Wrong gender");
        }

        customer.setPhoneNumber(phoneNum);
        Customer cust = customerRepo.save(customer);

        customerID = cust.getCustomerID().toString();

        customerWallet.setCustomerID(UUID.fromString(customerID));
        customerWalletRepo.save(customerWallet);

        logger.info("---------- Finish Generate Cust ID Service -------------");
        logger.info("Service Time: " + (System.currentTimeMillis() - start));

        return customerID;
    }

    public RegisterResponse doRegister(String client_id, String phoneNum, String credNo, String fullName, String deviceData, String AccountNumber, String Gender) {
        RegisterResponse registerResp = new RegisterResponse();
        RegisterResponse.RegisterResponseOutputSchema registOutputSchema = new RegisterResponse.RegisterResponseOutputSchema();
        RegisterResponse.ErrorSchema.ErrorMessage registErrorMessage = new RegisterResponse.ErrorSchema.ErrorMessage();
        RegisterResponse.ErrorSchema registErrorSchema = new RegisterResponse.ErrorSchema();
        String customerID = "";
        String requestID = "";
        String code = hashCode.getHashCode();
        User user = new User();
        Date date = new Date();
        Hashtable<String, String> hashResult = new Hashtable<>();

        logger.info("---------- Start Register Service ----------");
        long start = System.currentTimeMillis();
        logger.debug(code + "Checking if Card No allowed: " + client_id);

        if (client_id.equals("Allowed")) {

            logger.debug(code + "Card No Allowed, Start registering");
            logger.debug(code + "Generate Customer ID");

            customerID = doGenerateCustID(credNo, fullName, deviceData, phoneNum, AccountNumber, Gender);


            logger.debug(code + "Check customerID: " + customerID);

            if (customerID.equals("phoneNum")) {
                logger.error(code + "phoneNum Sudah Terdaftar");

                registErrorMessage.setEnglish("phoneNum Already Register");
                registErrorMessage.setIndonesian("phoneNum Sudah terdaftar");
                registErrorSchema.setError_code("BIT-300");
                registErrorSchema.setError_message(registErrorMessage);
                registOutputSchema.setVerify(false);
                registerResp.setError_schema(registErrorSchema);
                registerResp.setOutput_schema(registOutputSchema);
            } else if (customerID.equals("credNo")) {
                logger.error(code + "credNo Sudah Terdaftar");

                registErrorMessage.setEnglish("credNo Already Register");
                registErrorMessage.setIndonesian("credNo Sudah terdaftar");
                registErrorSchema.setError_code("BIT-300");
                registErrorSchema.setError_message(registErrorMessage);
                registOutputSchema.setVerify(false);
                registerResp.setError_schema(registErrorSchema);
                registerResp.setOutput_schema(registOutputSchema);
            }else if (customerID.equals("deviceData")) {
                logger.error(code + "deviceData Sudah Terdaftar");

                registErrorMessage.setEnglish("deviceData Already Register");
                registErrorMessage.setIndonesian("deviceData Sudah terdaftar");
                registErrorSchema.setError_code("BIT-300");
                registErrorSchema.setError_message(registErrorMessage);
                registOutputSchema.setVerify(false);
                registerResp.setError_schema(registErrorSchema);
                registerResp.setOutput_schema(registOutputSchema);
            }else if (customerID.equals("AccountNumber")) {
                logger.error(code + "AccountNumber Sudah Terdaftar");

                registErrorMessage.setEnglish("AccountNumber Already Register");
                registErrorMessage.setIndonesian("AccountNumber Sudah terdaftar");
                registErrorSchema.setError_code("BIT-300");
                registErrorSchema.setError_message(registErrorMessage);
                registOutputSchema.setVerify(false);
                registerResp.setError_schema(registErrorSchema);
                registerResp.setOutput_schema(registOutputSchema);
            }else {
                logger.debug(code + "Add customer to user database");

                user.setUserDetailId(UUID.fromString(customerID));
                user.setDateCreated(date);
                user.setDateUpdated(date);
                user.setUpdatedBy("mobile");
                user.setCreatedBy("mobile");
                user.setActive(true);
                user.setUserRole("customer");
                user.setUserCredential(phoneNum);
                user.setPasswordHash("OTP");

                userRepo.save(user);

                logger.debug(code + "Generate OTP");

                hashResult = otpService.doGenerateOTP(phoneNum, customerID, true);

                registErrorSchema.setError_code("BIT-200");
                registErrorMessage.setEnglish("Success");
                registErrorMessage.setIndonesian("Berhasil");
                registOutputSchema.setRequest_id(hashResult.get("requestID"));
                registOutputSchema.setOtp(hashResult.get("otp"));
                registOutputSchema.setVerify(true);
                registErrorSchema.setError_message(registErrorMessage);
                registerResp.setError_schema(registErrorSchema);
                registerResp.setOutput_schema(registOutputSchema);
            }
        } else {
            logger.error(code + "Card No Not allowed");

            registErrorSchema.setError_code("BIT-404");
            registErrorMessage.setEnglish("Transaction Rejected");
            registErrorMessage.setIndonesian("Transaksi Ditolak");
            registErrorSchema.setError_message(registErrorMessage);
            registerResp.setError_schema(registErrorSchema);
            registOutputSchema.setVerify(false);
            registerResp.setOutput_schema(registOutputSchema);
            registerResp.setError_schema(registErrorSchema);
        }

        logger.info("---------- Finish Register Service --------");
        logger.info("Service time: " + (System.currentTimeMillis() - start));

        return registerResp;
    }

    public BackEndRegisterResponse doCheckCardStatus(String cardNo) {
        BackEndRegisterResponse backEndRegisterResponse = new BackEndRegisterResponse();
        BackEndRegisterResponse.OutputSchema outputSchema = new BackEndRegisterResponse.OutputSchema();
        BackEndRegisterResponse.ErrorSchema.ErrorMessage errorMessage = new BackEndRegisterResponse.ErrorSchema.ErrorMessage();
        BackEndRegisterResponse.ErrorSchema errorSchema = new BackEndRegisterResponse.ErrorSchema();
        RestTemplate restTemplate = new RestTemplate();
        Gson gson = new Gson();
        User user = new User();
        long start = System.currentTimeMillis();
        String code = hashCode.getHashCode();
        URL url = new URL();
        UriComponents URL = UriComponentsBuilder.fromUriString(url.getCoreBankURL()).path("/corebank").path("/inquiry").path("/card").queryParam("card={keyword}").buildAndExpand(cardNo);

        logger.info("-------- Start Check Card No Service ---------");
        logger.debug(code + "Getting data from core bank regarding status of card no: " + cardNo);
        logger.debug(code + "Checking the URL: " + URL.toUriString());

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(URL.toUriString(), String.class);

        logger.info("Checking output: " + responseEntity.getBody());

        backEndRegisterResponse = gson.fromJson(responseEntity.getBody(), BackEndRegisterResponse.class);

        logger.debug(code + "Status Card: " + backEndRegisterResponse.getOutput_schema().getStatus());

        logger.debug(code + "New User confirmed");
        outputSchema.setStatus(backEndRegisterResponse.getOutput_schema().getStatus());
        outputSchema.setCard_no(cardNo);
        outputSchema.setGender(backEndRegisterResponse.getOutput_schema().getGender());
        outputSchema.setName(backEndRegisterResponse.getOutput_schema().getName());
        outputSchema.setStatus(backEndRegisterResponse.getOutput_schema().getStatus());
        outputSchema.setAccount_no(backEndRegisterResponse.getOutput_schema().getAccount_no());
        outputSchema.setGender(backEndRegisterResponse.getOutput_schema().getGender());
        backEndRegisterResponse.setOutput_schema(outputSchema);

        logger.info("-------- End Check Card No Service --------");
        logger.info("Service time: " + (System.currentTimeMillis() - start));

        return backEndRegisterResponse;
    }

    public LoginResponse doLogin(String cardNo, String device_data){
        LoginResponse loginResponse = new LoginResponse();
        String customerID = "";
        Customer customer = new Customer();
        String code = hashCode.getHashCode();

        Date date = new Date();
        long start = System.currentTimeMillis();
        logger.info("------------------- Start Login Service ----------------");
        logger.debug(code + "Get Customer Data using card No");

        customer = customerRepo.findByCardNumber(cardNo);

        logger.debug(code + "Checking the output: " + customer.getCardNumber());
        logger.debug(code + "Check if the device data is not empty");

        if (!(customer.getDeviceData() == null)){
            logger.error(code + "Device Data is not empty");
            loginResponse = new LoginResponse("", false, "BIT-303");
            return loginResponse;
        }else{
            logger.debug(code + "Add the new device data to database");
            customer.setDeviceData(device_data);
            customer.setDateUpdated(date);
            customerRepo.save(customer);
            loginResponse = new LoginResponse(customer.getCustomerID().toString(), true, "BIT-200");
        }

        logger.info("---------------------- Finish Login Service --------------------------");
        logger.info("Service Time: " + (System.currentTimeMillis() - start));

        return loginResponse;
    }

    public LogoutResponse doLogout(String customerID, String deviceData){
        LogoutResponse logoutResponse = new LogoutResponse();
        Customer customer = new Customer();
        String code = hashCode.getHashCode();

        Date date = new Date();
        long start = System.currentTimeMillis();

        logger.info("------------------- Start Log out Service ----------------");
        logger.debug(code + "Get Data From Database");
        logger.debug(code + "Check input: " + customerID);

        customer = customerRepo.findByCustomerID(UUID.fromString(customerID));

        logger.debug(code + "Check if its the same device data");

        if (customer.getDeviceData().equals(deviceData)) {

            logger.debug(code + "Set device data to Null");

            customer.setDateUpdated(date);
            customer.setDeviceData(null);
            customerRepo.save(customer);
            logoutResponse = new LogoutResponse(customerID,true,"BIT-200");
        }else {
            logoutResponse = new LogoutResponse(customerID, false, "BIT-402");
        }



        logger.info("------------------- Finish Logout Service ---------------");
        logger.info("Service Time: " + (System.currentTimeMillis() - start));

        return logoutResponse;

    }

    public RegisterResponse doTestingRegister(String phoneNum, String credNo, String fullName, String deviceData, String accountNum, String gender){
        Customer customer = new Customer();
        User user = new User();
        BackEndRegisterResponse backEndRegisterResponse = doCheckCardStatus(credNo);
        RegisterResponse registerResponse = new RegisterResponse();
        RegisterResponse.ErrorSchema errorSchema = new RegisterResponse.ErrorSchema();


        if (backEndRegisterResponse.getOutput_schema().getStatus().equals("Allowed")) {

            customer.setCustomerImage("avatar_male.png");
            customer.setDeviceData(deviceData);
            customer.setPhoneNumber(phoneNum);
            customer.setGender(gender);
            customer.setAccountNumber(accountNum);
            customer.setCardNumber(credNo);
            customer.setFullName(fullName);

            Customer out = customerRepo.save(customer);

            user.setPasswordHash("Testing");
            user.setUpdatedBy("Mobile");
            user.setUserRole("Customer Testing");
            user.setUserDetailId(out.getCustomerID());
            user.setUserCredential(phoneNum);
            user.setActive(true);
            userRepo.save(user);

            errorSchema.setError_code("200");
            registerResponse.setError_schema(errorSchema);
        }else {
            errorSchema.setError_code("400");
            registerResponse.setError_schema(errorSchema);
        }

        return registerResponse;
    }
}