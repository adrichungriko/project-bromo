package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.User;
import com.bca.bit.bromo.mobile.entity.response.PasswordResponse;
import com.bca.bit.bromo.mobile.repo.UserRepo;
import com.bca.bit.bromo.utility.configuration.HashCode;
import com.bca.bit.bromo.utility.message.MessageMapEnglish;
import com.bca.bit.bromo.utility.message.MessageMapIndonesian;
import com.bca.bit.bromo.utility.message.ReturnCode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class PasswordService {

    static Logger logger = Logger.getLogger("PasswordService");

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private HashCode hashCode;

    public PasswordResponse doDummyEncrypt(String customerID, String password){
        PasswordResponse passwordResponse = new PasswordResponse(true, password, "BIT-200");

        return passwordResponse;
    }

    public PasswordResponse doEncrypt(String customerID, String password){
        PasswordResponse passwordResponse = new PasswordResponse();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String code = hashCode.getHashCode();
        long start = System.currentTimeMillis();

        logger.info("--------------- Start Password Encryption Service ---------------------");
        logger.debug(code + "Start Encrypting Using BCrypt Password Encoder Library");

        String hashedPassword = passwordEncoder.encode(password);

        logger.debug(code + "Finish Encrypting");

        passwordResponse = new PasswordResponse(true, customerID, "BIT-200");

        logger.debug(code + "Save the result to the database");

        User user = userRepo.findByuserDetailId(UUID.fromString(customerID));

        logger.debug(code + "Check the hashed password: " + hashedPassword);


        user.setPasswordHash(hashedPassword);
        user.setDateUpdated(date);
        user.setUpdatedBy("Mobile");
        userRepo.save(user);

        logger.info("--------------- Finish Password Encryption Service ---------------------");
        logger.info(code + "Service Time: " + (System.currentTimeMillis() - start));

        return passwordResponse;
    }

    public PasswordResponse doVerify(String customerID, String password){
        PasswordResponse passwordResponse = new PasswordResponse();
        BCrypt bCrypt = new BCrypt();
        long start = System.currentTimeMillis();
        String code = hashCode.getHashCode();
        boolean verify;

        logger.info("--------------- Start Password Verification Service --------------------");
        logger.debug(code + "Find the Customer using the Customer ID: " + customerID);

        User user = userRepo.findByuserDetailId(UUID.fromString(customerID));

        if (user == null){
            logger.error(code + "User Not Found");
            passwordResponse = new PasswordResponse(false, customerID, "BIT-410");
            return passwordResponse;
        }else {
            logger.debug(code + "User Found, Start Verification");
            verify = bCrypt.checkpw(password, user.getPasswordHash());
        }

        if(verify) {
            logger.debug(code + "Verification Success");

            passwordResponse = new PasswordResponse(true, customerID, "BIT-200");
        }else {
            logger.debug(code + "Wrong Password");

            passwordResponse = new PasswordResponse(false, customerID, "BIT-402");
        }

        return  passwordResponse;
    }

    public PasswordResponse doDummyVerify(String customerID, String password){
        PasswordResponse passwordResponse = new PasswordResponse(true, customerID, "BIT-200");
        return  passwordResponse;
    }

    public PasswordResponse doChangePassword(String customerID, String newPassword){
        PasswordResponse passwordResponse = new PasswordResponse();
        String code = hashCode.getHashCode();
        User user = new User();
        MessageMapIndonesian messageMapIndonesian = new MessageMapIndonesian();
        MessageMapEnglish messageMapEnglish = new MessageMapEnglish();
        ReturnCode returnCode = new ReturnCode();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedpassword = "";
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date =  new Date();
        boolean verify;
        long start = System.currentTimeMillis();

        logger.info(code + "----------------------- Start Service Change Password ----------------------");
        logger.debug(code + "Find the user using customer ID");

        user = userRepo.findByuserDetailId(UUID.fromString(customerID));

        logger.debug(code + "Hashed the new password");
        hashedpassword = passwordEncoder.encode(newPassword);

        logger.debug(code + "Set the new hashed password");

        user.setPasswordHash(hashedpassword);
        user.setDateUpdated(date);
        user.setUpdatedBy("mobile");

        logger.debug(code + "Add Data to database");

        userRepo.save(user);

        logger.debug(code + "Success");

        passwordResponse = new PasswordResponse(true, customerID, "BIT-200");

        logger.info("----------------------- Finish Change Password Service ------------------");
        logger.info("Service time: " + (System.currentTimeMillis() - start));


        return passwordResponse;

    }
}
