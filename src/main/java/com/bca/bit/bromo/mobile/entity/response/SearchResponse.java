package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.mobile.entity.model.SearchResponseModel;
import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

import java.util.ArrayList;

@Data
public class SearchResponse extends BaseResponse {
    private SearchResponseOutputSchema output_schema;

    public SearchResponse(ArrayList<SearchResponseModel> l_searchResult, String error_code){
        super(error_code);
        output_schema = new SearchResponseOutputSchema(l_searchResult);
    }
    public SearchResponse(String error_code){
        super(error_code);
    }

    @Data
    private class SearchResponseOutputSchema{
        public SearchResponseOutputSchema(ArrayList<SearchResponseModel> l_searchResult){
            merchant = l_searchResult;
        }

        private ArrayList<SearchResponseModel> merchant;
    }
}
