package com.bca.bit.bromo.mobile.repo;

import com.bca.bit.bromo.mobile.entity.jpa.CustomerWallet;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface CustomerWalletRepo extends PagingAndSortingRepository<CustomerWallet, UUID> {
    CustomerWallet findByCustomerID(UUID customerID);
}
