package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.CategoryMerchant;
import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.jpa.MerchantPromo;
import com.bca.bit.bromo.mobile.entity.jpa.Promo;
import com.bca.bit.bromo.mobile.repo.MerchantPromoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;

@Service(value = "merchantPromoService")
public class MerchantPromoService {
    @Autowired
    MerchantPromoRepo merchantPromoRepo;

    public ArrayList<MerchantPromo> getAllMerchantByPromo(ArrayList<Promo> l_promo){
        ArrayList<UUID> promoId = new ArrayList<>();
        ArrayList<MerchantPromo>l_merchPromo = new ArrayList<>();

        for (Promo promo: l_promo) {
            promoId.add(promo.getPromoId());
        }
        Iterable<MerchantPromo>i_merchantPromo = merchantPromoRepo.findAllByPromoId(promoId);
        if(i_merchantPromo.iterator().hasNext()) {
            for (MerchantPromo merchant : i_merchantPromo) {
                l_merchPromo.add(merchant);
            }
        }

        return l_merchPromo;
    }

    public ArrayList<MerchantPromo> getPromoByMerchant(Merchant l_merchant){
        ArrayList<UUID> merchantId = new ArrayList<>();
        ArrayList<MerchantPromo> l_merchPromo = new ArrayList<>();


        merchantId.add(l_merchant.getMerchantId());

        Iterable<MerchantPromo>i_merchantPromo = merchantPromoRepo.findAllByMerchantId(merchantId);

        if(i_merchantPromo.iterator().hasNext()){
            for (MerchantPromo merchant : i_merchantPromo) {
                l_merchPromo.add(merchant);
            }
        }

        return l_merchPromo;
    }

    public ArrayList<MerchantPromo> getAllByMerchantId(ArrayList<UUID> merchantId){
        ArrayList<MerchantPromo> l_merchantPromo;
        l_merchantPromo = (ArrayList<MerchantPromo>)merchantPromoRepo.findAllByMerchantId(merchantId);

        return l_merchantPromo;
    }
}
