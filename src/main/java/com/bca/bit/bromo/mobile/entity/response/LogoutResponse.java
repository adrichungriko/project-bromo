package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

@Data
public class LogoutResponse extends BaseResponse {
    private LogoutResponseOutputSchema output_schema;

    public LogoutResponse(){
    }
    public LogoutResponse(String customerID, boolean verify, String errorCode){
        super(errorCode);
        this.output_schema = new LogoutResponseOutputSchema (customerID, verify);
    }

    public LogoutResponse(String errorCode){
        super(errorCode);
    }

    public LogoutResponse(String errorCode, String message){
        super(errorCode, message);
    }

    @Data
    public static class LogoutResponseOutputSchema{
        private String customer_id;
        private boolean verify;

        public LogoutResponseOutputSchema(String customer_id, boolean verify) {
            this.customer_id = customer_id;
            this.verify = verify;
        }
    }
}
