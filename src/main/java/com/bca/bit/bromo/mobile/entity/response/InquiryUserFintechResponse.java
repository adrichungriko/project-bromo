package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.utility.common.BaseResponse;

import lombok.Data;

@Data
public class InquiryUserFintechResponse extends BaseResponse{
	
	private InquiryUserFintechResponseOutputSchema output_schema;
	
	public InquiryUserFintechResponse(String error_code) {
		super(error_code);
	}
	
	public InquiryUserFintechResponse(String error_code,String fintech_name, String fintech_id, String customer_fintech_name) {
		super(error_code);
		output_schema = new InquiryUserFintechResponseOutputSchema(fintech_name, fintech_id, customer_fintech_name);
	}
	
	@Data
	private class InquiryUserFintechResponseOutputSchema{
		
		public InquiryUserFintechResponseOutputSchema(String fintech_name,String fintech_id,String customer_fintech_name) {
			this.customer_fintech_name = customer_fintech_name;
			this.fintech_id = fintech_id;
			this.fintech_name = fintech_name;
		}
		
		private String fintech_name;
		private String fintech_id;
		private String customer_fintech_name; 
	}

}
