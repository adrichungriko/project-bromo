package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

import java.util.UUID;

@Data
public class MerchantRecommendationRequest {
    private String userLat;
    private String userLong;
    private String customerId;
}
