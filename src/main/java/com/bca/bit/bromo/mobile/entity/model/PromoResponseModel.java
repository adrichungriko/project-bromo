package com.bca.bit.bromo.mobile.entity.model;

import com.bca.bit.bromo.mobile.entity.jpa.Promo;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;

@Data
public class PromoResponseModel {
    private Promo promo;
    private ArrayList<MerchantResponseModel> merchant;
}
