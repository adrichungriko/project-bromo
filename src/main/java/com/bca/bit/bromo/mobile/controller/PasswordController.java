package com.bca.bit.bromo.mobile.controller;

import com.bca.bit.bromo.mobile.entity.request.PasswordRequest;
import com.bca.bit.bromo.mobile.entity.response.PasswordResponse;
import com.bca.bit.bromo.mobile.service.PasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mobile")
public class PasswordController {

    @Autowired
    private PasswordService passwordService;

    @PostMapping("/dummy/encrypt/pin")
    public PasswordResponse dummyEncryptResponse(@RequestBody PasswordRequest passwordRequest){
        PasswordResponse passwordResponse = new PasswordResponse();
        String user_id = passwordRequest.getCustomer_id().isEmpty() ? "" : passwordRequest.getCustomer_id();
        String password = passwordRequest.getPassword().isEmpty() ? "" : passwordRequest.getPassword();
        String missingfield = "";

        try{
            if(user_id == ""){
                missingfield = "CustomerID";
                throw new Exception();
            } else if (password == ""){
                missingfield = "Password";
                throw new Exception();
            } else{
                passwordResponse = passwordService.doDummyEncrypt(user_id, password);
            }
        } catch (Exception ex){
            passwordResponse = new PasswordResponse(false, passwordRequest.getCustomer_id(), "BIT-404");
        }
        return passwordResponse;
    }

    @PostMapping("/encrypt/pin")
    public PasswordResponse encryptResponse(@RequestBody PasswordRequest passwordRequest){
        PasswordResponse passwordResponse = new PasswordResponse();
        String user_id = passwordRequest.getCustomer_id().isEmpty() ? "" : passwordRequest.getCustomer_id();
        String password = passwordRequest.getPassword().isEmpty() ? "" : passwordRequest.getPassword();
        String missingfield = "";

        try{
            if(user_id == ""){
                missingfield = "CustomerID";
                throw new Exception();
            } else if (password == ""){
                missingfield = "Password";
                throw new Exception();
            } else{
                passwordResponse = passwordService.doEncrypt(user_id, password);
            }
        } catch (Exception ex){
        	ex.printStackTrace();
            passwordResponse = new PasswordResponse(false, passwordRequest.getCustomer_id(), "BIT-404");
        }
        return passwordResponse;
    }

    @PostMapping("/dummy/verify/pin")
    public PasswordResponse dummyVerifyResponse(@RequestBody PasswordRequest passwordRequest){
        PasswordResponse passwordResponse = new PasswordResponse();
        String customer_id = passwordRequest.getCustomer_id().isEmpty() ? "" : passwordRequest.getCustomer_id();
        String password = passwordRequest.getPassword().isEmpty()  ? "" : passwordRequest.getPassword();
        String missingfield = "";
        try {
            if(customer_id == ""){
                missingfield = "CustomerID";
                throw new Exception();
            }else if (password == ""){
                missingfield = "password";
                throw new Exception();
            }else
                passwordResponse = passwordService.doDummyVerify(customer_id, password);
        }catch (Exception ex){
            passwordResponse = new PasswordResponse(false, customer_id, "BIT-404");

        }
        return passwordResponse;
    }

    @PostMapping("/verify/pin")
    public PasswordResponse verifyResponse(@RequestBody PasswordRequest passwordRequest){
        PasswordResponse PasswordResponse = new PasswordResponse();
        String customer_id = passwordRequest.getCustomer_id().isEmpty() ? "" : passwordRequest.getCustomer_id();
        String password = passwordRequest.getPassword().isEmpty()  ? "" : passwordRequest.getPassword();
        String missingfield = "";
        try {
            if(customer_id == ""){
                missingfield = "CustomerID";
                throw new Exception();
            }else if (password == ""){
                missingfield = "password";
                throw new Exception();
            }else
                PasswordResponse = passwordService.doVerify(customer_id, password);
        }catch (Exception ex){
            PasswordResponse = new PasswordResponse(false, customer_id, "BIT-404");
        }
        return PasswordResponse;
    }

    @PostMapping("/change/pin")
    public PasswordResponse changePasswordResponse (@RequestBody PasswordRequest passwordRequest){
        PasswordResponse passwordResponse = new PasswordResponse();
        if (passwordRequest.getCustomer_id() == null || passwordRequest.getCustomer_id().isEmpty() || passwordRequest.getPassword() == null
        || passwordRequest.getPassword().isEmpty()){
            passwordResponse = new PasswordResponse("BIT-404");
        }else {
            try {
                passwordResponse = passwordService.doChangePassword(passwordRequest.getCustomer_id(), passwordRequest.getPassword());
            }catch (Exception ex){
                passwordResponse = new PasswordResponse("BIT-404", ex.getMessage());
            }
        }
        return passwordResponse;
    }

}
