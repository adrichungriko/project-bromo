package com.bca.bit.bromo.mobile.entity.response;

import lombok.Data;

@Data
public class OTPResponse {
    private ErrorSchema error_schema;
    private OTPResponseOutputSchema output_schema;

    @Data
    public static class ErrorSchema{
        private String error_code;
        private ErrorMessage error_message;

        @Data
        public static class ErrorMessage{
            private String english;
            private String indonesian;
        }
    }

    @Data
    public static class OTPResponseOutputSchema{
        private String customer_id;
        private boolean status;
    }
}
