package com.bca.bit.bromo.mobile.entity.model;

import lombok.Data;

@Data
public class UserInterestResponseModel {
    private String user_interest_image;
    private String user_interest_name;
    private String user_interest_id;

    public UserInterestResponseModel(String image,String name,String id) {
        this.user_interest_image = image;
        this.user_interest_id = id;
        this.user_interest_name = name;
    }
}