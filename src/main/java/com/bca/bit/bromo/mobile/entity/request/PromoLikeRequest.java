package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class PromoLikeRequest {
    private String customer_id;
    private String promo_id;
    private String search;
    private Boolean like;
    private Boolean thumbnail;
}
