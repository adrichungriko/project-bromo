package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.CategoryMerchant;
import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.jpa.MerchantCategory;
import com.bca.bit.bromo.mobile.repo.MerchantCategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service(value = "merchantCategoryService")
public class MerchantCategoryService {
    @Autowired
    MerchantCategoryRepo merchantCategoryRepo;

    public ArrayList<MerchantCategory> getMerchantByCategoryId(Integer catId){
        ArrayList<MerchantCategory> l_merchant;
        ArrayList<Integer> l_catId = new ArrayList<>();
        l_catId.add(catId);
        l_merchant = (ArrayList<MerchantCategory>) merchantCategoryRepo.findAllByCategoryId(l_catId);

        return l_merchant;
    }

    public ArrayList<MerchantCategory> getCategoryByMerchant(Merchant merchant){
        ArrayList<MerchantCategory> l_merchantCategory = new ArrayList<>();
        Iterable<MerchantCategory> i_merchantCategory = merchantCategoryRepo.findByMerchantId(merchant.getMerchantId());

        for(MerchantCategory merchantCategory : i_merchantCategory){
            l_merchantCategory.add(merchantCategory);
        }

        return l_merchantCategory;
    }

    public ArrayList<MerchantCategory> getAllMerchantByCategory(ArrayList<CategoryMerchant> catMerch){
        ArrayList<Integer> catId = new ArrayList<>();
        ArrayList<MerchantCategory> l_merchantCat = new ArrayList<>();

        for (CategoryMerchant _catMerch:catMerch) {
            catId.add(_catMerch.getCategoryId());
        }
        System.out.println("List Category Id : " + catId);

        Iterable<MerchantCategory> i_merchantCat = merchantCategoryRepo.findAllByCategoryId(catId);
        System.out.println(i_merchantCat);

        if(i_merchantCat.iterator().hasNext()){
            for (MerchantCategory merchCat: i_merchantCat) {
                l_merchantCat.add(merchCat);
            }
        }
        else {
            l_merchantCat = null;
        }

        return l_merchantCat;
    }
}
