package com.bca.bit.bromo.mobile.entity.response;

import java.util.List;

import lombok.Data;

@Data
public class RegisterResponse{

    private ErrorSchema error_schema;
    private RegisterResponseOutputSchema output_schema;

    @Data
    public static class ErrorSchema{
        private String error_code;
        private ErrorMessage error_message;

        @Data
        public static class ErrorMessage{
            private String english;
            private String indonesian;
        }
    }

      @Data
      public static class RegisterResponseOutputSchema{
    	  private String request_id;
          private boolean verify;
          private String otp;
      }

}
