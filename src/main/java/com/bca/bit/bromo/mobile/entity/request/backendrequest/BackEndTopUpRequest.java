package com.bca.bit.bromo.mobile.entity.request.backendrequest;

import lombok.Data;

@Data
public class BackEndTopUpRequest {
    private String card;
    private String virtualAccount;
    private int amountDebit;
}
