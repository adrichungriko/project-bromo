package com.bca.bit.bromo.mobile.entity.jpa;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@Table(name = "ms_customer", schema = "app_bromo")
public class Customer {

    @Id
    @GeneratedValue(generator = "uuid")
    @Column(name = "customer_id")
    private UUID customerID;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "device_data")
    private String deviceData;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "account_number")
    private String accountNumber;

    @Column(name = "gender")
    private String gender;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created")
    private Date dateCreated;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_updated")
    private Date dateUpdated;

    @Column(name = "customer_image")
    private String customerImage;

    @Column(name = "loyalty_point")
    private double loyaltyPoint;
    
    @ManyToMany
    @JoinTable(
      schema = "app_bromo",
      name = "customer_category", 
      joinColumns = @JoinColumn(name = "customer_id"), 
      inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> listCategoryUserInterest;

}
