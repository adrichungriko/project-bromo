package com.bca.bit.bromo.mobile.controller;

import com.bca.bit.bromo.mobile.entity.request.BindFintechRequest;
import com.bca.bit.bromo.mobile.entity.request.InquiryMutasiTransaksiRequest;
import com.bca.bit.bromo.mobile.entity.request.InquirySaldoRequest;
import com.bca.bit.bromo.mobile.entity.response.BindFintechResponse;
import com.bca.bit.bromo.mobile.entity.response.InquiryMutasiTransaksiResponse;
import com.bca.bit.bromo.mobile.entity.response.InquirySaldoResponse;
import com.bca.bit.bromo.mobile.entity.response.InquiryUserFintechResponse;
import com.bca.bit.bromo.mobile.service.BindFintechService;
import com.bca.bit.bromo.mobile.service.InquiryMutasiTransaksiService;
import com.bca.bit.bromo.mobile.service.InquirySaldoService;
import com.bca.bit.bromo.mobile.service.KafkaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fintech")
public class FintechController {

    @Autowired
    private InquiryMutasiTransaksiService inquiryMutasiTransaksiService;

    @Autowired
    private InquirySaldoService inquirySaldoService;

    @Autowired
    private BindFintechService bindFintechService;
    
    @Autowired
    private KafkaService kafkaService;

    @PostMapping("/mutasi")
    public InquiryMutasiTransaksiResponse inquiryMutasiTransaksi(@RequestBody InquiryMutasiTransaksiRequest inquiryMutasiTransaksiRequest){
        InquiryMutasiTransaksiResponse inquiryMutasiTransaksiResponse = new InquiryMutasiTransaksiResponse();
        InquiryMutasiTransaksiResponse.ErrorSchema errorSchema = new InquiryMutasiTransaksiResponse.ErrorSchema();
        InquiryMutasiTransaksiResponse.ErrorSchema.ErrorMessage errorMessage = new InquiryMutasiTransaksiResponse.ErrorSchema.ErrorMessage();
        String missingField = "";
        String customerID = inquiryMutasiTransaksiRequest.getCustomer_id().isEmpty() || inquiryMutasiTransaksiRequest.getCustomer_id() == null
                ? "" : inquiryMutasiTransaksiRequest.getCustomer_id();
        String fintechName = inquiryMutasiTransaksiRequest.getFintech_name().isEmpty() || inquiryMutasiTransaksiRequest.getFintech_name() == null
                ? "" : inquiryMutasiTransaksiRequest.getFintech_name();
        try {
            if (customerID == ""){
                missingField = "CustomerID";
                throw  new MissingFieldException();
            } else if (fintechName == ""){
                missingField = "FintechName";
                throw new MissingFieldException();
            }
            else{
                inquiryMutasiTransaksiResponse = inquiryMutasiTransaksiService.doMutasi(customerID, fintechName);
            }
        }catch (MissingFieldException ex){
            errorMessage.setIndonesian("tidak ada input difield: " + missingField);
            errorMessage.setEnglish("Missing Data Field: " + missingField);
            errorSchema.setError_message(errorMessage);
            errorSchema.setError_code("BIT-400");
            inquiryMutasiTransaksiResponse.setError_schema(errorSchema);

        } catch (Exception ex){
            errorMessage.setEnglish(ex.getMessage());
            errorSchema.setError_code("BIT-300");
            errorSchema.setError_message(errorMessage);
            inquiryMutasiTransaksiResponse.setError_schema(errorSchema);
        }

        return inquiryMutasiTransaksiResponse;
    }

    @PostMapping("/dummy/mutasi")
    public InquiryMutasiTransaksiResponse DoDummyInquiryMutasiTransaksi(@RequestBody InquiryMutasiTransaksiRequest inquiryMutasiTransaksiRequest){
        InquiryMutasiTransaksiResponse inquiryMutasiTransaksiResponse = new InquiryMutasiTransaksiResponse();
        inquiryMutasiTransaksiResponse = inquiryMutasiTransaksiService.doDummyMutasi(inquiryMutasiTransaksiRequest.getCustomer_id(), inquiryMutasiTransaksiRequest.getFintech_name());
        return inquiryMutasiTransaksiResponse;
    }

    @PostMapping("/saldo")
    public InquirySaldoResponse inquirySaldo(@RequestBody InquirySaldoRequest inquirySaldoRequest){
        InquirySaldoResponse inquirySaldoResponse = new InquirySaldoResponse();
        InquirySaldoResponse.ErrorSchema errorSchema = new InquirySaldoResponse.ErrorSchema();
        InquirySaldoResponse.ErrorSchema.ErrorMessage errorMessage = new InquirySaldoResponse.ErrorSchema.ErrorMessage();
        String customerID = inquirySaldoRequest.getCustomer_id().isEmpty() || inquirySaldoRequest.getCustomer_id() == null
                ? "" : inquirySaldoRequest.getCustomer_id();
        String missingfield = "";

        try {
            if (customerID == "") {
                missingfield = "Customer ID";
                throw new MissingFieldException();
            }
            inquirySaldoResponse = inquirySaldoService.doInquiry(customerID);
        } catch (MissingFieldException e) {
            errorSchema.setError_code("BIT-400");
            errorMessage.setEnglish("Missing Data field: " + missingfield);
            errorMessage.setIndonesian("Tidak ads Data: " + missingfield);
            errorSchema.setError_message(errorMessage);
            inquirySaldoResponse.setError_schema(errorSchema);
        } catch (Exception ex){
            errorSchema.setError_code("BIT-300");
            errorMessage.setEnglish(ex.getMessage());
            errorSchema.setError_message(errorMessage);
            inquirySaldoResponse.setError_schema(errorSchema);
        }
        return inquirySaldoResponse;
    }

    @PostMapping("/bind")
    public BindFintechResponse BindFintech(@RequestBody BindFintechRequest bindFintechRequest){
        BindFintechResponse bindFintechResponse = new BindFintechResponse();
        String customerId = bindFintechRequest.getCustomer_id() == null ? "" : bindFintechRequest.getCustomer_id();
        String fintechId = bindFintechRequest.getFintech_id() == null ? "" : bindFintechRequest.getFintech_id();
        if (customerId.isEmpty() || fintechId.isEmpty()){
            bindFintechResponse = new BindFintechResponse("BIT-404");
        }else {
            try {
                bindFintechResponse = bindFintechService.doBindFintech(bindFintechRequest.getCustomer_id(), bindFintechRequest.getFintech_id(), bindFintechRequest.getFintech_name());
            }catch (Exception ex){
                bindFintechResponse = new BindFintechResponse("BIT-499");
            }
        }
        return bindFintechResponse;
    }

    @PostMapping("/unbind")
    public BindFintechResponse unBindFintech(@RequestBody BindFintechRequest bindFintechRequest){
        BindFintechResponse bindFintechResponse;
        if (bindFintechRequest.getCustomer_id() == null || bindFintechRequest.getCustomer_id().isEmpty()){
            bindFintechResponse = new BindFintechResponse("BIT-404");
        }else {
            try {
                bindFintechResponse = bindFintechService.doUnbindFintech(bindFintechRequest.getCustomer_id(), bindFintechRequest.getFintech_id(), bindFintechRequest.getFintech_name());
            }catch (Exception ex){
            	ex.printStackTrace();
                bindFintechResponse = new BindFintechResponse("BIT-499");
            }
        }
        return bindFintechResponse;
    }
    
    @PostMapping("/user")
    public InquiryUserFintechResponse inquiryUserFintech(@RequestBody BindFintechRequest bindFintechRequest) {
        String customerId = bindFintechRequest.getCustomer_id() == null ? "" : bindFintechRequest.getCustomer_id();
        String fintechName = bindFintechRequest.getFintech_name() == null ? "" : bindFintechRequest.getFintech_name();
        String fintechID = bindFintechRequest.getFintech_id() == null ? "" : bindFintechRequest.getFintech_id();
        InquiryUserFintechResponse inquiryUserFintechResponse;
        if (customerId.isEmpty() || fintechID.isEmpty() || fintechName.isEmpty()){
            inquiryUserFintechResponse = new InquiryUserFintechResponse("BIT-404");
        }else{
            try {
                inquiryUserFintechResponse = bindFintechService.doInquiryFintechUser(bindFintechRequest.getCustomer_id(), bindFintechRequest.getFintech_name(), bindFintechRequest.getFintech_id());
            }catch (Exception ex){
                inquiryUserFintechResponse = new InquiryUserFintechResponse("BIT-499");
            }
        }
    	return inquiryUserFintechResponse;
    }
    
    @PostMapping("/put")
    public void putListTransaction(@RequestParam String topic, @RequestBody String listTrx) {
    	kafkaService.putMessage(topic,listTrx);
    }
    
    /*
    @PostMapping("/testingKafka")
    public void putListTransactionKafka(@RequestParam String topic, @RequestBody ListAllTrxCustomer listTrx) {
    	kafkaService.putMessage(topic,listTrx);
    }*/


    public class MissingFieldException extends Exception{
    }

}
