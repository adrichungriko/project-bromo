package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class TrxPromoUsedRequest {
    private String customer_id;
    private String merchant_id;
    private String promo_id;
    private double amount;
    private String random;
}
