package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.model.MerchantResponseModel;
import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

import java.util.ArrayList;

@Data
public class MerchantResponse extends BaseResponse {
    private MerchantResponseOutputSchema output_schema;

    public MerchantResponse(ArrayList<MerchantResponseModel>l_merchant,String error_code){
        super(error_code);
        output_schema = new MerchantResponseOutputSchema(l_merchant);
    }
    public MerchantResponse(String errorCode){super(errorCode);}

    @Data
    private class MerchantResponseOutputSchema{
        MerchantResponseOutputSchema(ArrayList<MerchantResponseModel> l_merchant){
            merchant = l_merchant;
        }
        private ArrayList<MerchantResponseModel> merchant;
    }
}
