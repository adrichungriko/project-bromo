package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.Customer;
import com.bca.bit.bromo.mobile.entity.jpa.CustomerWallet;
import com.bca.bit.bromo.mobile.entity.jpa.TrxPromoUsed;
import com.bca.bit.bromo.mobile.entity.response.MobileProfileResponse;
import com.bca.bit.bromo.mobile.repo.CustomerRepo;
import com.bca.bit.bromo.mobile.repo.CustomerWalletRepo;
import com.bca.bit.bromo.mobile.repo.TrxPromoUsedRepo;
import com.bca.bit.bromo.utility.configuration.HashCode;
import org.apache.kafka.common.protocol.types.Field;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MobileProfileService {

    static Logger logger = Logger.getLogger("MobileProfileService");

    @Autowired
    private CustomerWalletRepo customerWalletRepo;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private HashCode hashCode;

    @Autowired
    private TrxPromoUsedRepo trxPromoUsedRepo;

    public MobileProfileResponse doProfile(String customerID){
        MobileProfileResponse mobileProfileResponse = new MobileProfileResponse();
        Customer customer = new Customer();
        CustomerWallet customerWallet = new CustomerWallet();
        TrxPromoUsed trxPromoUsed = new TrxPromoUsed();
        int counter = 0;
        long start = System.currentTimeMillis();
        String count = "";
        String promoUsed = "";
        String code = hashCode.getHashCode();
        logger.info("------------------- Start Mobile Profile Service ------------------");
        logger.debug(code + "get Custommer data from database");

        customer = customerRepo.findByCustomerID(UUID.fromString(customerID));
        customerWallet = customerWalletRepo.findByCustomerID(UUID.fromString(customerID));
        List<TrxPromoUsed> trxPromoUsedList = trxPromoUsedRepo.findByCustomerID(UUID.fromString(customerID));

        logger.debug(code + "Get Customer transaction data from Database");

        //harusnya connect ke database lgi
        //di dummy dulu
        if (customerWallet.getSakukuID() == null){
            logger.debug(code + "No Sakuku Fintech");
        }else {
            counter += 1;
        }

        if (customerWallet.getGopayID() == null){
            logger.debug(code + "No Gopay Fintech");
        }else {
            counter += 1;
        }

        if (customerWallet.getDanaID() == null){
            logger.debug(code + "No Dana Fintech");
        }else {
            counter += 1;
        }

        if (customerWallet.getOvoID() == null){
            logger.debug(code + "No OVO Fintech");
        }else {
            counter += 1;
        }

        logger.debug(code + "Amount of e wallet: " + counter);

        count = Integer.toString(counter);

        if (trxPromoUsedList.isEmpty()){
             promoUsed = "0";
        }else {
             promoUsed = Integer.toString(trxPromoUsedList.size());
        }

        String cardNo = customer.getCardNumber();
        String subCardNo = cardNo.substring(12);
        double loyalty = customer.getLoyaltyPoint();
        logger.debug(code + "Check result: " + subCardNo);
        logger.debug(code + "Set response");

        mobileProfileResponse = new MobileProfileResponse(customerID, customer.getFullName(), customer.getPhoneNumber(), count, promoUsed
                , promoUsed, customer.getCustomerImage(),subCardNo, loyalty, "BIT-200");

        return mobileProfileResponse;
    }

    public MobileProfileResponse doDummyProfile(String customerID){
        MobileProfileResponse mobileProfileResponse = new MobileProfileResponse(customerID,"Rudy Putra", "123456789", "4", "30", "50","avatar_male.png","0000",99999, "BIT-200");

        return mobileProfileResponse;
    }

}
