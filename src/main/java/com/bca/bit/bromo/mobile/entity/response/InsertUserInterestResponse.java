package com.bca.bit.bromo.mobile.entity.response;

import java.util.List;

import com.bca.bit.bromo.utility.common.BaseResponse;

import lombok.Data;

@Data
public class InsertUserInterestResponse extends BaseResponse{

	private InsertUserInterestOutputSchema output_schema;
	
	public InsertUserInterestResponse(String error_code) {
		super(error_code);
	}
	
	public InsertUserInterestResponse(String error_code,String customer_id,List<String> list_user_interest_name) {
		super(error_code);
		this.output_schema = new InsertUserInterestOutputSchema(customer_id, list_user_interest_name);
	}

	public InsertUserInterestResponse(String errorCode, String message){
		super(errorCode, message);
	}
	
	
	@Data
	private class InsertUserInterestOutputSchema{
		private String customer_id;
		private List<String> list_user_interest_name;
		
		public InsertUserInterestOutputSchema(String customer_id, List<String> listUserInterestName) {
			this.customer_id = customer_id;
			this.list_user_interest_name = listUserInterestName;
		}
	}
	
}
