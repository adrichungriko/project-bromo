package com.bca.bit.bromo.mobile.entity.response;

import lombok.Data;

import java.util.List;

@Data
public class InquirySaldoResponse {
    private ErrorSchema error_schema;
    private InquirySaldoOutputSchema output_schema;

    @Data
    public static class ErrorSchema{
        private String error_code;
        private ErrorMessage error_message;

        @Data
        public static class ErrorMessage{
            private String english;
            private String indonesian;
        }
    }

    @Data
    public static class InquirySaldoOutputSchema{
        private String customer_id;
        private List<balance> saldo;

        @Data
        public static class balance {
            private String fintech_id;
            private String fintech_name;
            private String balance;
            private String fintech_cust_name;
        }
    }

}
