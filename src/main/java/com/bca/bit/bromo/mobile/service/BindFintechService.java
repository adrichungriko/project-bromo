package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.Customer;
import com.bca.bit.bromo.mobile.entity.jpa.CustomerWallet;
import com.bca.bit.bromo.mobile.entity.request.fintechrequest.BindFintechAPIRequest;
import com.bca.bit.bromo.mobile.entity.response.BindFintechResponse;
import com.bca.bit.bromo.mobile.entity.response.InquiryUserFintechResponse;
import com.bca.bit.bromo.mobile.entity.response.fintechapiresponse.BindFintechAPIResponse;
import com.bca.bit.bromo.mobile.entity.response.fintechapiresponse.InquiryUserResponseFintechAPI;
import com.bca.bit.bromo.mobile.repo.CustomerRepo;
import com.bca.bit.bromo.mobile.repo.CustomerWalletRepo;
import com.bca.bit.bromo.utility.configuration.Constant;
import com.bca.bit.bromo.utility.configuration.HashCode;
import com.bca.bit.bromo.utility.configuration.URL;
import com.bca.bit.bromo.utility.message.ReturnCode;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class BindFintechService {
    static Logger logger = Logger.getLogger("BindFintechService");

    @Autowired
    private CustomerWalletRepo customerWalletRepo;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private HashCode hashCode;

    @Autowired
    private URL url;

    public BindFintechResponse doBindFintech(String customerID, String fintech_id, String fintech_name) {
        BindFintechResponse bindFintechResponse = new BindFintechResponse();
        BindFintechAPIRequest bindFintechAPIRequest;
        BindFintechAPIResponse bindFintechAPIResponse = new BindFintechAPIResponse();
        CustomerWallet customerWallet = new CustomerWallet();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSSZ");
        Date date = java.util.Calendar.getInstance().getTime();
        RestTemplate restTemplate = new RestTemplate();
        Customer customer = new Customer();
        String fintechID = fintech_id;
        Gson gson = new Gson();
        String status = "Allowed";
        Constant constant = new Constant();
        String code = hashCode.getHashCode();
        HttpEntity<BindFintechAPIRequest> requestEntity;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url.getFintechURL()).path("/fintech")
                .path("/inquiry").path("/verify").path("/bind");
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> response;
        long start = System.currentTimeMillis();

        logger.info("----------------------- Start Binding Fintech Service -----------------");

        logger.debug(code + "Set Up Fintech URL");
        logger.debug(code + "Check if the Customer is in database");

        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);

        customer = customerRepo.findByCustomerID(UUID.fromString(customerID));

        if (customer.getCustomerID().toString().isEmpty() || (customer.getCustomerID().toString() == null)) {
            logger.error(code + "Customer is not available");
            bindFintechResponse = new BindFintechResponse(customerID, "", false, "BIT-410");
        } else {
            customerWallet = customerWalletRepo.findByCustomerID(UUID.fromString(customerID));
            switch (fintech_name) {
                case "DANA":
                    if (customerWallet.getDanaID() == null) {
                        logger.debug(code + "Check Ke fintech untuk DANA");
                        bindFintechAPIRequest = new BindFintechAPIRequest(fintech_id, fintech_name, constant.getBROMOAPP(),
                                customerID);
                        requestEntity = new HttpEntity<BindFintechAPIRequest>(bindFintechAPIRequest, headers);
                        logger.debug(code + "Check URL: " + builder.toUriString());
                        logger.debug(code + "Check Request Entity: " + requestEntity.toString());
                        response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, requestEntity,
                                String.class);
                        logger.debug(code + "Check response: " + response.getBody());

                        bindFintechAPIResponse = gson.fromJson(response.getBody(), BindFintechAPIResponse.class);

                        if (bindFintechAPIResponse.getOutput_schema().getSuccess_flag().equals("success")
                                && bindFintechAPIResponse.getOutput_schema().getStatus_customer().equals("active")) {
                            logger.debug(code + "Masukin Data ke database");
                            customerWallet.setDanaID(fintechID);
                            logger.debug(code + "masukin ke database customer wallet");
                            customerWalletRepo.save(customerWallet);
                            bindFintechResponse = new BindFintechResponse(fintech_id,
                                    bindFintechAPIResponse.getOutput_schema().getStatus_customer() + " & "
                                            + bindFintechAPIResponse.getOutput_schema().getSuccess_flag(),
                                    true, "BIT-200");
                        } else {
                            logger.error(
                                    code + "Fintech ID " + bindFintechAPIResponse.getOutput_schema().getStatus_customer());
                            bindFintechResponse = new BindFintechResponse(fintech_id,
                                    bindFintechAPIResponse.getOutput_schema().getStatus_customer() + " & "
                                            + bindFintechAPIResponse.getOutput_schema().getSuccess_flag(),
                                    false, "BIT-402");
                        }
                    } else {
                        logger.error(code + "Dana ID sudah pernah di bind");
                        bindFintechResponse = new BindFintechResponse(customerID, "", false, "BIT-303");
                    }

                    break;

                case "OVO":
                    if (customerWallet.getOvoID() == null) {
                        logger.debug(code + "Check ke fintech untuk OVO");
                        bindFintechAPIRequest = new BindFintechAPIRequest(fintech_id, fintech_name, constant.getBROMOAPP(),
                                customerID);
                        requestEntity = new HttpEntity<BindFintechAPIRequest>(bindFintechAPIRequest, headers);
                        logger.debug(code + "Check URL: " + builder.toUriString());
                        logger.debug(code + "Check Request Entity: " + requestEntity);
                        response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, requestEntity,
                                String.class);
                        logger.debug(code + "Check response: " + response.getBody());
                        bindFintechAPIResponse = gson.fromJson(response.getBody(), BindFintechAPIResponse.class);

                        if (bindFintechAPIResponse.getOutput_schema().getSuccess_flag().equals("success")
                                && bindFintechAPIResponse.getOutput_schema().getStatus_customer().equals("active")) {
                            logger.debug(code + "Masukin Data ke database");
                            customerWallet.setOvoID(fintechID);
                            logger.debug(code + "masukin ke database customer wallet");
                            customerWalletRepo.save(customerWallet);
                            bindFintechResponse = new BindFintechResponse(fintech_id,
                                    bindFintechAPIResponse.getOutput_schema().getStatus_customer() + " & "
                                            + bindFintechAPIResponse.getOutput_schema().getSuccess_flag(),
                                    true, "BIT-200");
                        } else {
                            logger.error(code + "Fintech ID Not Allowed");
                            bindFintechResponse = new BindFintechResponse(fintech_id,
                                    bindFintechAPIResponse.getOutput_schema().getStatus_customer() + " & "
                                            + bindFintechAPIResponse.getOutput_schema().getSuccess_flag(),
                                    false, "BIT-402");
                        }
                    } else {
                        logger.error(code + "OVO ID sudah pernah di bind");
                        bindFintechResponse = new BindFintechResponse(fintech_id, "", false, "BIT-303");
                    }

                    break;

                case "Gopay":
                    if (customerWallet.getGopayID() == null) {
                        logger.debug(code + "Check ke fintech untuk Gopay");
                        bindFintechAPIRequest = new BindFintechAPIRequest(fintech_id, fintech_name, constant.getBROMOAPP(),
                                customerID);
                        requestEntity = new HttpEntity<BindFintechAPIRequest>(bindFintechAPIRequest, headers);
                        logger.debug(code + "Check URL: " + builder.toUriString());
                        logger.debug(code + "Check Request Entity: " + requestEntity.toString());
                        response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, requestEntity,
                                String.class);
                        logger.debug(code + "Check response: " + response.getBody());
                        bindFintechAPIResponse = gson.fromJson(response.getBody(), BindFintechAPIResponse.class);

                        if (bindFintechAPIResponse.getOutput_schema().getSuccess_flag().equals("success")
                                && bindFintechAPIResponse.getOutput_schema().getStatus_customer().equals("active")) {
                            logger.debug(code + "Masukin Data ke database");
                            customerWallet.setGopayID(fintechID);
                            logger.debug(code + "masukin ke database customer wallet");
                            customerWalletRepo.save(customerWallet);
                            bindFintechResponse = new BindFintechResponse(customerID,
                                    bindFintechAPIResponse.getOutput_schema().getStatus_customer() + " & "
                                            + bindFintechAPIResponse.getOutput_schema().getSuccess_flag(),
                                    true, "BIT-200");
                        } else {
                            logger.error(code + "Fintech ID Not Allowed");
                            bindFintechResponse = new BindFintechResponse(customerID,
                                    bindFintechAPIResponse.getOutput_schema().getStatus_customer() + " & "
                                            + bindFintechAPIResponse.getOutput_schema().getSuccess_flag(),
                                    false, "BIT-402");
                        }
                    } else {
                        logger.error(code + "Gopay ID sudah pernah di bind");
                        bindFintechResponse = new BindFintechResponse(customerID, "", false, "BIT-303");
                    }

                    break;

                case "Sakuku":
                    if (customerWallet.getSakukuID() == null) {
                        logger.debug(code + "Check ke fintech untuk Sakuku");
                        bindFintechAPIRequest = new BindFintechAPIRequest(fintech_id, fintech_name, constant.getBROMOAPP(),
                                customerID);
                        requestEntity = new HttpEntity<BindFintechAPIRequest>(bindFintechAPIRequest, headers);
                        logger.debug(code + "Check URL: " + builder.toUriString());
                        logger.debug(code + "Check Request Entity: " + requestEntity.toString());
                        response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, requestEntity,
                                String.class);
                        logger.debug(code + "Check response: " + response.getBody());
                        bindFintechAPIResponse = gson.fromJson(response.getBody(), BindFintechAPIResponse.class);

                        if (bindFintechAPIResponse.getOutput_schema().getSuccess_flag().equals("success")
                                && bindFintechAPIResponse.getOutput_schema().getStatus_customer().equals("active")) {
                            logger.debug(code + "Masukin Data ke database");
                            customerWallet.setSakukuID(fintechID);
                            logger.debug(code + "masukin ke database customer wallet");
                            customerWalletRepo.save(customerWallet);
                            bindFintechResponse = new BindFintechResponse(customerID,
                                    bindFintechAPIResponse.getOutput_schema().getStatus_customer() + " & "
                                            + bindFintechAPIResponse.getOutput_schema().getSuccess_flag(),
                                    true, "BIT-200");
                        } else {
                            logger.error(code + "Fintech ID Not Allowed");
                            bindFintechResponse = new BindFintechResponse(customerID,
                                    bindFintechAPIResponse.getOutput_schema().getStatus_customer() + " & "
                                            + bindFintechAPIResponse.getOutput_schema().getSuccess_flag(),
                                    false, "BIT-402");
                        }
                    } else {
                        logger.error(code + "Sakuku ID sudah pernah di bind");
                        bindFintechResponse = new BindFintechResponse(customerID, "", false, "BIT-303");
                    }

                    break;
            }
        }
        return bindFintechResponse;
    }

    public InquiryUserFintechResponse doInquiryFintechUser(String customer_id, String fintech_name, String fintech_id) {

        Gson gson = new Gson();
        RestTemplate restTemplate = new RestTemplate();
        InquiryUserResponseFintechAPI inqResponse;

        try {
            if (!fintech_name.equals("") && !fintech_id.equals("")) {
                UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url.getFintechURL()).path("/fintech")
                        .path("/inquiry").path("/customer").queryParam("fintech_name", fintech_name)
                        .queryParam("fintech_id", fintech_id);
                HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null,
                        String.class);
                inqResponse = gson.fromJson(response.getBody(), InquiryUserResponseFintechAPI.class);

                if (inqResponse.getError_schema().getError_code().equals("BIT-200")) {
                    String customer_name = inqResponse.getOutput_schema().getCustomer_name();

                    return new InquiryUserFintechResponse(ReturnCode.BROMO_SERVICE_SUCCESS, fintech_name, fintech_id,
                            customer_name);
                } else {
                    return new InquiryUserFintechResponse(ReturnCode.BROMO_SERVICE_GENERAL_ERROR);
                }
            } else {
                return new InquiryUserFintechResponse(ReturnCode.BROMO_MISSING_INPUT);
            }
        } catch (Exception e) {
            return new InquiryUserFintechResponse(ReturnCode.BROMO_SERVICE_GENERAL_ERROR);
        }

    }

    public BindFintechResponse doUnbindFintech(String customerId, String fintechId, String fintechName) {
        CustomerWallet customerWallet = new CustomerWallet();
        BindFintechResponse bindFintechResponse;
        BindFintechAPIRequest bindFintechAPIRequest;
        Constant constant = new Constant();
        HttpEntity<BindFintechAPIRequest> requestEntity;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url.getFintechURL()).path("/fintech")
                .path("/inquiry").path("/verify").path("/unbind");
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> response;
        BindFintechAPIResponse bindFintechAPIResponse;
        Gson gson = new Gson();
        RestTemplate restTemplate = new RestTemplate();
        long start = System.currentTimeMillis();
        String code = hashCode.getHashCode();

        logger.info("----------------------------- Start Serfive Unbind Fintech -------------------------------");

        try {
            logger.debug(code + "Hit Api fintech dulu untuk cek hasil");
            bindFintechAPIRequest = new BindFintechAPIRequest(fintechId, fintechName, constant.getBROMOAPP(), customerId);
            requestEntity = new HttpEntity<BindFintechAPIRequest>(bindFintechAPIRequest, headers);
            response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, requestEntity,
                    String.class);

            logger.debug(code + "Checking output: " + response.getBody());
            bindFintechAPIResponse = gson.fromJson(response.getBody(), BindFintechAPIResponse.class);

            if (bindFintechAPIResponse.getOutput_schema().getSuccess_flag().equals("success")
                    && bindFintechAPIResponse.getOutput_schema().getStatus_customer().equals("active")) {

                logger.debug(code + "get data from database");
                customerWallet = customerWalletRepo.findByCustomerID(UUID.fromString(customerId));
                logger.debug(code + "find which fintech wallet to delete");

                switch (fintechName) {
                    case "OVO":
                        if (customerWallet.getOvoID() == null) {
                            logger.error(code + "no ovo ID");
                            return new BindFintechResponse("BIT-410");
                        } else {
                            logger.debug(code + "delete ovo id");
                            customerWallet.setOvoID(null);
                            customerWalletRepo.save(customerWallet);
                        }
                        break;

                    case "DANA":
                        if (customerWallet.getDanaID() == null) {
                            logger.error(code + "no Dana ID");
                            return new BindFintechResponse("BIT-410");
                        } else {
                            logger.debug(code + "Delete Dana id");
                            customerWallet.setDanaID(null);
                            customerWalletRepo.save(customerWallet);
                            break;
                        }
                    case "Sakuku":
                        if (customerWallet.getSakukuID() == null) {
                            logger.error(code + "no sakuku ID");
                            return new BindFintechResponse("BIT-410");
                        } else {
                            logger.debug(code + "Delete sakuku id");
                            customerWallet.setSakukuID(null);
                            customerWalletRepo.save(customerWallet);
                            break;
                        }

                    case "Gopay":
                        if (customerWallet.getGopayID() == null) {
                            logger.error(code + "no Gopay ID");
                            return new BindFintechResponse("BIT-410");
                        } else {
                            logger.debug(code + "Delete Gopay Id");
                            customerWallet.setGopayID(null);
                            customerWalletRepo.save(customerWallet);
                            break;
                        }

                    default:
                        logger.error(code + "no fintech name");
                        return new BindFintechResponse("BIT-400");
                }
                bindFintechResponse = new BindFintechResponse(fintechId, "", true, "BIT-200");
            } else {
         
                bindFintechResponse = new BindFintechResponse("BIT-499");
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
            bindFintechResponse = new BindFintechResponse("BIT-499", ex.getMessage());
        }
        return bindFintechResponse;
    }
}
