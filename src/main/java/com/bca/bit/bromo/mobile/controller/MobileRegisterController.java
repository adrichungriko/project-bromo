package com.bca.bit.bromo.mobile.controller;

import com.bca.bit.bromo.mobile.entity.request.LoginRequest;
import com.bca.bit.bromo.mobile.entity.request.LogoutRequest;
import com.bca.bit.bromo.mobile.entity.request.MobileProfileRequest;
import com.bca.bit.bromo.mobile.entity.response.LoginResponse;
import com.bca.bit.bromo.mobile.entity.response.LogoutResponse;
import com.bca.bit.bromo.mobile.entity.response.MobileProfileResponse;
import com.bca.bit.bromo.mobile.entity.response.backendresponse.BackEndRegisterResponse;
import com.bca.bit.bromo.mobile.service.MobileProfileService;
import com.bca.bit.bromo.utility.generate.QRGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.bca.bit.bromo.mobile.entity.request.RegisterRequest;
import com.bca.bit.bromo.mobile.entity.response.RegisterResponse;
import com.bca.bit.bromo.mobile.service.MobileRegisterService;
import sun.rmi.runtime.Log;

import java.util.UUID;

@RestController
@RequestMapping("/mobile")
public class MobileRegisterController {

    @Autowired
    private MobileRegisterService mobileRegisterSvc;

    @Autowired
    private MobileProfileService mobileProfileService;

    @Autowired
    private QRGenerator qrGenerator;

    @PostMapping("/register")
    public RegisterResponse doRegister(@RequestBody RegisterRequest registerReq) {
        RegisterResponse registerResp = new RegisterResponse();
        RegisterResponse.ErrorSchema errorSchema = new RegisterResponse.ErrorSchema();
        RegisterResponse.ErrorSchema.ErrorMessage errorMessage = new RegisterResponse.ErrorSchema.ErrorMessage();
        BackEndRegisterResponse backEndRegisterResponse = new BackEndRegisterResponse();
        try {
            if (registerReq.getCred_no().isEmpty() || registerReq.getCred_no() == null) {
                errorMessage.setEnglish("missing field");
                errorSchema.setError_message(errorMessage);
                errorSchema.setError_code("BIT-404");
                registerResp.setError_schema(errorSchema);
            } else {
                backEndRegisterResponse = mobileRegisterSvc.doCheckCardStatus(registerReq.getCred_no());
                if (registerReq.getPhone_num().isEmpty() || registerReq.getPhone_num() == null || registerReq.getCred_no().isEmpty() ||
                registerReq.getCred_no() == null || registerReq.getDevice_data().isEmpty() || registerReq.getDevice_data() == null){
                    errorMessage.setEnglish("missing field");
                    errorSchema.setError_message(errorMessage);
                    errorSchema.setError_code("BIT-404");
                    registerResp.setError_schema(errorSchema);
                }else {
                    registerResp = mobileRegisterSvc.doRegister(backEndRegisterResponse.getOutput_schema().getStatus()
                            , registerReq.getPhone_num(), registerReq.getCred_no(), backEndRegisterResponse.getOutput_schema().getName()
                            , registerReq.getDevice_data(), backEndRegisterResponse.getOutput_schema().getAccount_no()
                            , backEndRegisterResponse.getOutput_schema().getGender());
                }
            }
        }catch (Exception ex){
            errorMessage.setEnglish(ex.getMessage());
            errorSchema.setError_code("BIT-499");
            errorSchema.setError_message(errorMessage);
            registerResp.setError_schema(errorSchema);
        }
        return registerResp;
    }

    @PostMapping("/login")
    public LoginResponse doLogin(@RequestBody LoginRequest loginRequest){
        LoginResponse loginResponse = new LoginResponse();
        if (loginRequest.getCard_no().isEmpty() || loginRequest.getCard_no() == null || loginRequest.getDevice_data().isEmpty()
                || loginRequest.getDevice_data() == null) {
            loginResponse = new LoginResponse("BIT-404");
        }else {
            try {
                loginResponse = mobileRegisterSvc.doLogin(loginRequest.getCard_no(), loginRequest.getDevice_data());
            }catch (Exception ex){
                loginResponse = new LoginResponse("BIT-499", ex.getMessage());
            }
        }
        return loginResponse;
    }

    @PostMapping("/logout")
    public LogoutResponse doLogout (@RequestBody LogoutRequest logoutRequest){
        LogoutResponse logoutResponse = new LogoutResponse();
        if (logoutRequest.getCustomer_id().isEmpty() || logoutRequest.getCustomer_id() == null || logoutRequest.getDevice_data().isEmpty() || logoutRequest.getDevice_data() == null){
            logoutResponse = new LogoutResponse("BIT-404");
        }else {
            try {
                logoutResponse = mobileRegisterSvc.doLogout(logoutRequest.getCustomer_id(), logoutRequest.getDevice_data());
            }catch (Exception ex){
                logoutResponse = new LogoutResponse("BIT-499", ex.getMessage());
            }
        }
        return logoutResponse;
    }

    @PostMapping("dummy/register")
    public  RegisterResponse doDummyRegister(@RequestBody RegisterRequest registerRequest) {
        RegisterResponse registerResponse = new RegisterResponse();
        RegisterResponse.RegisterResponseOutputSchema outputSchema = new RegisterResponse.RegisterResponseOutputSchema();
        RegisterResponse.ErrorSchema errorSchema = new RegisterResponse.ErrorSchema();
        RegisterResponse.ErrorSchema.ErrorMessage errorMessage = new RegisterResponse.ErrorSchema.ErrorMessage();
        errorMessage.setIndonesian("Berhasil");
        errorMessage.setEnglish("Success");
        errorSchema.setError_message(errorMessage);
        errorSchema.setError_code("BIT-200");
        outputSchema.setRequest_id(UUID.randomUUID().toString());
        outputSchema.setVerify(true);
        registerResponse.setOutput_schema(outputSchema);
        registerResponse.setError_schema(errorSchema);

        return registerResponse;
    }

    @PostMapping("/profile")
    public MobileProfileResponse doProfile(@RequestBody MobileProfileRequest mobileProfileRequest){
        MobileProfileResponse mobileProfileResponse = new MobileProfileResponse();
        if (mobileProfileRequest.getCustomer_id() == null || mobileProfileRequest.getCustomer_id().isEmpty()){
            mobileProfileResponse = new MobileProfileResponse("BIT-404");
        }else {
            try {
                mobileProfileResponse = mobileProfileService.doProfile(mobileProfileRequest.getCustomer_id());
            }catch (Exception ex){
                mobileProfileResponse = new MobileProfileResponse("BIT-499", ex.getMessage());
            }
        }
        return mobileProfileResponse;
    }

    @PostMapping("/dummy/profile")
    public MobileProfileResponse doDummyProfile(@RequestBody MobileProfileRequest mobileProfileRequest){
        MobileProfileResponse mobileProfileResponse = mobileProfileService.doDummyProfile(mobileProfileRequest.getCustomer_id());
        return mobileProfileResponse;
    }

    @GetMapping("/qr")
    public boolean doQRGenerate (@RequestParam String text){
        boolean ver;
        String output = qrGenerator.generateQRCodeImage(text);
        ver = true;
        qrGenerator.decodeQRCodeString(output);
        return ver;
    }

    @PostMapping("testing/register")
    public RegisterResponse doTestingRegister(@RequestBody RegisterRequest registerRequest){
        return mobileRegisterSvc.doTestingRegister(registerRequest.getPhone_num(), registerRequest.getCred_no()
                , "Testing", registerRequest.getDevice_data(), "123456789",
                "male");
    }

}
