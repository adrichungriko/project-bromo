package com.bca.bit.bromo.mobile.entity.jpa;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="merchant_category", schema = "app_bromo")
@Data
public class MerchantCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name="merchant_id")
    private UUID merchantId;

    @Column(name="category_id")
    private Integer categoryId;
}
