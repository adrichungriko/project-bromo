package com.bca.bit.bromo.mobile.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bca.bit.bromo.mobile.entity.jpa.Category;
import com.bca.bit.bromo.mobile.entity.jpa.Customer;
import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.jpa.MerchantCategory;
import com.bca.bit.bromo.mobile.entity.jpa.Promo;
import com.bca.bit.bromo.mobile.entity.response.InitPrepareHomeResponse;
import com.bca.bit.bromo.mobile.entity.response.InquirySaldoResponse;
import com.bca.bit.bromo.mobile.entity.response.InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MySlider;
import com.bca.bit.bromo.mobile.entity.response.InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard;
import com.bca.bit.bromo.mobile.entity.response.InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyWallet;
import com.bca.bit.bromo.mobile.entity.response.InquirySaldoResponse.InquirySaldoOutputSchema.balance;
import com.bca.bit.bromo.mobile.repo.CategoryRepo;
import com.bca.bit.bromo.mobile.repo.MerchantCategoryRepo;
import com.bca.bit.bromo.mobile.repo.MerchantRepo;
import com.bca.bit.bromo.mobile.repo.PromoRepo;
import com.bca.bit.bromo.utility.configuration.Constant;
import com.bca.bit.bromo.utility.message.ReturnCode;

@Service
public class HomeService {

	@Autowired
	private InquirySaldoService inquirySaldoSvc;

	@Autowired
	private MerchantRepo merchantRepo;

	@Autowired
	private CategoryRepo categoryRepo;

	@Autowired
	private MerchantCategoryRepo merchantCategoryRepo;

	@Autowired
	private PromoRepo promoRepo;

	@Async
	public CompletableFuture<List<MyWallet>> getMyWallet(String customerId) {

		List<MyWallet> listMyWallet = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyWallet>();
		InquirySaldoResponse inqBalanceResp = inquirySaldoSvc.doInquiry(customerId);

		// check ada wallet nya ngga?
		if (inqBalanceResp.getError_schema().getError_code().equals(ReturnCode.BROMO_SERVICE_SUCCESS)) {

			List<balance> listBalanceUser = inqBalanceResp.getOutput_schema().getSaldo();

			// check saldo wallet
			if (inqBalanceResp.getOutput_schema().getSaldo().size() > 0) {
				for (balance b : listBalanceUser) {
					if (b.getBalance() != null) {
						InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyWallet myWallet = new MyWallet();
						myWallet.setBalance(b.getBalance());
						myWallet.setFintech_name(b.getFintech_name());
						myWallet.setFintech_cust_name(b.getFintech_cust_name());
						myWallet.setFintech_cust_id(b.getFintech_id());

						if (b.getFintech_name().equals("DANA")) {
							Merchant merchant = merchantRepo.findByMerchantNameAndMerchantType("DANA", "FINTECH");
							myWallet.setFintech_logo_url(merchant.getMerchantImage());
						} else if (b.getFintech_name().equals("Sakuku")) {
							Merchant merchant = merchantRepo.findByMerchantNameAndMerchantType("SAKUKU", "FINTECH");
							myWallet.setFintech_logo_url(merchant.getMerchantImage());
						} else if (b.getFintech_name().equals("OVO")) {
							Merchant merchant = merchantRepo.findByMerchantNameAndMerchantType("OVO", "FINTECH");
							myWallet.setFintech_logo_url(merchant.getMerchantImage());
						} else if (b.getFintech_name().equals("Gopay")) {
							Merchant merchant = merchantRepo.findByMerchantNameAndMerchantType("GOPAY", "FINTECH");
							myWallet.setFintech_logo_url(merchant.getMerchantImage());
						}
						listMyWallet.add(myWallet);
					}
				}
			}
		}
		return CompletableFuture.completedFuture(listMyWallet);
	}

	@Async
	public CompletableFuture<List<MyViewCard>> getTopMerchantByRecommendation(String userLat, String userLong,
			String customerId) {
		List<MyViewCard> listViewCard = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard>();

		userLat = "6";
		userLong = "106";
		String strUri = new Constant().getURL_MERCHANT_RECOMMENDATION() + "?user_lat=%s&user_lon=%s&user_id=%s";
		final String uri = String.format(strUri, userLat, userLong, customerId);

		try {
			RestTemplate restTemplate = new RestTemplate();

			HashMap m_result = restTemplate.getForObject(uri, HashMap.class);
			ArrayList<String> l_merchantId = (ArrayList<String>) ((HashMap) m_result.get("output_schema"))
					.get("merchant_id");

			for (int i = 0; i < l_merchantId.size(); i++) {

				List<MyViewCard.CategoryMerchant> listCategoryOfMerchant = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard.CategoryMerchant>();
				Merchant m = merchantRepo.findByMerchantId(UUID.fromString(l_merchantId.get(i)));

				MyViewCard myViewCard = new MyViewCard();
				myViewCard.setMerchant_id(m.getMerchantId().toString());
				myViewCard.setMerchant_name(m.getMerchantName());
				myViewCard.setMerchant_logo_url(m.getMerchantImage());

				List<MerchantCategory> mc = (List<MerchantCategory>) merchantCategoryRepo
						.findByMerchantId(m.getMerchantId());

				for (MerchantCategory mc_item : mc) {
					Category cat = categoryRepo.findByCategoryId(mc_item.getCategoryId());

					MyViewCard.CategoryMerchant mcm = new InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MyViewCard.CategoryMerchant();
					mcm.setCategory_id(mc_item.getCategoryId());
					mcm.setCategory_name(cat.getCategoryName());
					listCategoryOfMerchant.add(mcm);
				}

				myViewCard.setMerchant_category(listCategoryOfMerchant);
				listViewCard.add(myViewCard);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return CompletableFuture.completedFuture(listViewCard);
	}

	@Async
	public CompletableFuture<List<MySlider>> getSlider() {

		List<MySlider> listMySlider = new ArrayList<InitPrepareHomeResponse.InitPrepareHomeOutputSchema.MySlider>();
		List<Promo> promolistSlider = promoRepo.findByIsShowSlider("Y");
		for (Promo promoItem : promolistSlider) {
			MySlider mySlider = new MySlider();
			mySlider.setPromo_id(promoItem.getPromoId().toString());
			mySlider.setPromo_name(promoItem.getPromoName());
			mySlider.setSlider_image_url(promoItem.getPicture());
			listMySlider.add(mySlider);
		}

		return CompletableFuture.completedFuture(listMySlider);
	}

}
