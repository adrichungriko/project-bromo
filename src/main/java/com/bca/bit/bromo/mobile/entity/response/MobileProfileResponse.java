package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

@Data
public class MobileProfileResponse extends BaseResponse {
    private MobileProfileResponseOutputSchema output_schema;

    public MobileProfileResponse (){}
    public MobileProfileResponse(String customerID, String name, String phoneNum, String eWallet, String promoUsed, String transactions, String customerImage, String card_no, double loyalty, String errorCode){
        super(errorCode);
        this.output_schema = new MobileProfileResponseOutputSchema(customerID, name, phoneNum, eWallet, promoUsed, transactions, customerImage, card_no, loyalty);
    }
    public MobileProfileResponse(String errorCode){
        super(errorCode);
    }

    public MobileProfileResponse(String errorCode, String message){
        super(errorCode, message);
    }


    @Data
    public static class MobileProfileResponseOutputSchema{
        private String customer_id;
        private String name;
        private String phone_num;
        private String e_wallet;
        private String promo_used;
        private String transactions;
        private String customer_image;
        private String card_no;
        private double loyalty;


        public MobileProfileResponseOutputSchema(String customer_id, String name, String phone_num, String e_wallet, String promo_used, String transactions, String customer_image, String card_no, double loyalty) {
            this.customer_id = customer_id;
            this.name = name;
            this.phone_num = phone_num;
            this.e_wallet = e_wallet;
            this.promo_used = promo_used;
            this.transactions = transactions;
            this.customer_image = customer_image;
            this.card_no = card_no;
            this.loyalty = loyalty;
        }
    }
}
