package com.bca.bit.bromo.mobile.controller;

import com.bca.bit.bromo.mobile.entity.jpa.Promo;
import com.bca.bit.bromo.mobile.entity.model.SearchResponseModel;
import com.bca.bit.bromo.mobile.entity.request.MerchantRecommendationRequest;
import com.bca.bit.bromo.mobile.entity.request.PromoDetailRequest;
import com.bca.bit.bromo.mobile.entity.request.PromoLikeRequest;
import com.bca.bit.bromo.mobile.entity.response.*;
import com.bca.bit.bromo.mobile.service.PromoService;
import com.bca.bit.bromo.utility.configuration.Constant;
import com.bca.bit.bromo.utility.configuration.HashCode;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@RestController
@RequestMapping("/mobile")
public class PromoController {

    @Autowired
    private PromoService promoService;

    @Autowired
    HashCode hashCode;

    private Constant constant = new Constant();

    @GetMapping(value = "promo/explore-merchant", produces = {"application/json"})
    public SearchResponse merchantRecommendation(@RequestParam String userLat, @RequestParam String userLong,@RequestParam String customerId){
        SearchResponse result;
        try {
            ArrayList<SearchResponseModel>l_recommendation = promoService.merchantRecommendation(userLat,userLong,customerId);
            if(l_recommendation.size()>0) {
                result = new SearchResponse(l_recommendation,"BIT-200");
            }
            else {
                result = new SearchResponse(l_recommendation,"BIT-410");
            }
        }
        catch (Exception e){
            result = new SearchResponse("BIT-400");
            e.printStackTrace();
        }
        return result;
    }

    @GetMapping(value = "/promo/search", produces = {"application/json"})
    public SearchResponse searchPromo(@RequestParam String key){
        SearchResponse searchResponse;
        try {
            ArrayList<SearchResponseModel>l_search = promoService.searchPromo(key);
            if(l_search.size()>0){
                searchResponse = new SearchResponse(l_search,"BIT-200");
            }
            else {
                searchResponse = new SearchResponse(l_search,"BIT-410");
            }
        }
        catch (Exception e){
            searchResponse = new SearchResponse("BIT-400");
            e.printStackTrace();
        }

        return searchResponse;
    }

    @GetMapping(value = "/promo/search/dummy", produces = {"application/json"})
    public SearchResponse searchPromoDummy(@RequestParam String key){
        ArrayList<SearchResponseModel> l_srm = new ArrayList<>();
        SearchResponseModel srm1 = new SearchResponseModel();
        SearchResponseModel srm2 = new SearchResponseModel();
        SearchResponseModel srm3 = new SearchResponseModel();

        SearchResponseModel.Promo p1 = new SearchResponseModel.Promo();
        SearchResponseModel.Promo p2 = new SearchResponseModel.Promo();
        SearchResponseModel.Promo p3 = new SearchResponseModel.Promo();

        SearchResponseModel.Category c1 = new SearchResponseModel.Category();
        SearchResponseModel.Category c2 = new SearchResponseModel.Category();
        SearchResponseModel.Category c3 = new SearchResponseModel.Category();
        SearchResponseModel.Category c4 = new SearchResponseModel.Category();
        SearchResponseModel.Category c5 = new SearchResponseModel.Category();

        ArrayList<SearchResponseModel.Category> ac1 = new ArrayList<>();
        ArrayList<SearchResponseModel.Category> ac2 = new ArrayList<>();
        ArrayList<SearchResponseModel.Category> ac3 = new ArrayList<>();

        p1.setPromoId(UUID.fromString("1f614c9e-7521-4c68-aa45-4719785d9bbb"));
        p1.setPromoName("Starbucks Merchandise Discount");
        p1.setCashbackPercent(20.00);
        p2.setPromoId(UUID.fromString("4b192a29-0a08-4b23-abc8-33abc525f38a"));
        p2.setPromoName("Chatime Special 18.000");
        p2.setCashbackPercent(50.00);
        p3.setPromoId(UUID.fromString("386c6ac0-6797-41ca-9315-7ace58ef7f3a"));
        p3.setPromoName("Ovo Cashback Marathon 60%");
        p3.setCashbackPercent(60.00);

        c1.setCategoryName("Food and Beverage");
        c1.setCategoryId(1);
        c2.setCategoryName("Tea");
        c2.setCategoryId(5);
        c3.setCategoryName("Coffee");
        c3.setCategoryId(4);
        c4.setCategoryName("Milk Tea");
        c4.setCategoryId(6);
        c5.setCategoryName("Duck and Chicken");
        c5.setCategoryId(7);

        ac1.add(c2);
        ac1.add(c3);
        ac2.add(c1);
        ac2.add(c2);
        ac2.add(c4);
        ac3.add(c1);
        ac3.add(c5);

        srm1.setMerchantId(UUID.fromString("f75da60b-2dc1-4deb-898c-305820ca0b37"));
        srm1.setMerchantName("Starbucks Coffee");
        srm1.setMerchantImage(null);
        srm1.setPromo(p1);
        srm1.setCategory(ac1);

        srm2.setMerchantId(UUID.fromString("04ef5962-bcd6-448c-a6c3-c935afc1dbe5"));
        srm2.setMerchantName("CHATIME TAMAN ANGGREK");
        srm2.setMerchantImage(null);
        srm2.setPromo(p2);
        srm2.setCategory(ac2);

        srm3.setMerchantId(UUID.fromString("37c61aed-cc34-4acd-abca-4af3b7986d37"));
        srm3.setMerchantName("HOKA HOKA BENTO");
        srm3.setMerchantImage(null);
        srm3.setPromo(p3);
        srm3.setCategory(ac3);

        l_srm.add(srm1);
        l_srm.add(srm2);
        l_srm.add(srm3);
        SearchResponse searchResponse = new SearchResponse(l_srm,"BIT-200");
        return searchResponse;
    }


    @GetMapping(value = "/promo/merchant", produces = {"application/json"})
    public MerchantPromoResponse getPromoByMerchantID(@RequestParam String merchantId, @RequestParam UUID customerId){
        MerchantPromoResponse merchantPromoResponse = promoService.getPromoByMerchantID(merchantId, customerId);

        return merchantPromoResponse;
    }

    @GetMapping(value = "/promo")
    public PromoDetailResponse getPromoDetailByID(@RequestParam String promoId, @RequestParam String customerId, @RequestParam String search){
        PromoDetailResponse promoDetailResponse;
        if (promoId.equals("") || customerId.equals("") || search == null || promoId == null || customerId == null){
            promoDetailResponse = new PromoDetailResponse("BIT-404");
        }else {
            promoDetailResponse = promoService.getPromoDetailById(promoId, customerId, search);
        }
        return promoDetailResponse;
    }

    @PostMapping(value = "/like/promo")
    public PromoLikeResponse doSaveLikePromo(@RequestBody PromoLikeRequest promoLikeRequest){
        String promoId = promoLikeRequest.getPromo_id() == null ? "" : promoLikeRequest.getPromo_id();
        String customerId = promoLikeRequest.getCustomer_id() == null ? "" : promoLikeRequest.getCustomer_id();
        String search = promoLikeRequest.getSearch() == null ? "null" : promoLikeRequest.getSearch();
        PromoLikeResponse promoLikeResponse;
        if (promoId.isEmpty() || customerId.isEmpty() || search.equals("null")){
            promoLikeResponse = new PromoLikeResponse("BIT-404");
        }else {
            promoLikeResponse = promoService.doSavePromoLike(promoId,customerId,search,promoLikeRequest.getLike(), promoLikeRequest.getThumbnail());
        }

        return promoLikeResponse;
    }



}
