package com.bca.bit.bromo.mobile.entity.response;

import lombok.Data;

@Data
public class Response {
    private ErrorSchema error_schema;
    @Data
    public static class ErrorSchema{
        private String error_code;
        private ErrorMessage error_message;

        @Data
        public static class ErrorMessage{
            private String english;
            private String indonesian;
        }
    }
}
