package com.bca.bit.bromo.mobile.service;

import com.bca.bit.bromo.mobile.entity.jpa.CategoryMerchant;
import com.bca.bit.bromo.mobile.entity.jpa.Merchant;
import com.bca.bit.bromo.mobile.entity.jpa.MerchantCategory;
import com.bca.bit.bromo.mobile.entity.jpa.MerchantOpeningHours;
import com.bca.bit.bromo.mobile.entity.model.MerchantResponseModel;
import com.bca.bit.bromo.mobile.repo.MerchantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service(value="merchantService")
public class MerchantService {
    @Autowired
    MerchantRepo merchantRepo;

    @Autowired
    MerchantCategoryService merchantCategoryService;

    @Autowired
    MerchantOpeningHoursService merchantOpeningHoursService;

    public Iterable<Merchant> getMerchantByCategoryId(Integer catId){
        ArrayList<MerchantCategory> l_merchCat = new ArrayList<>();
        ArrayList<UUID> l_merchId = new ArrayList<>();

        l_merchCat = merchantCategoryService.getMerchantByCategoryId(catId);

        for(MerchantCategory mc : l_merchCat) {
            l_merchId.add(mc.getMerchantId());
        }
        return merchantRepo.findAllMerchantCentralByMerchantId(l_merchId);
    }

    public ArrayList<Merchant> getMerchantByCentrailId(UUID merchantId){
        ArrayList<Merchant> l_merchant = new ArrayList<>();
        l_merchant.addAll((ArrayList<Merchant>) merchantRepo.findMerchantByMerchantCentralId(merchantId.toString()));
        l_merchant.add(merchantRepo.findById(merchantId).get());
        return l_merchant;
    }

    public ArrayList<MerchantResponseModel> getMerchantAndOpeningHoursByCentrailId(UUID merchantId){
        ArrayList<MerchantResponseModel> l_merchant = new ArrayList<>();

        for(Merchant merchant:getMerchantByCentrailId(merchantId)){
            MerchantResponseModel o_merchant = new MerchantResponseModel();
            ArrayList<MerchantOpeningHours> moh = new ArrayList<>();
            moh.addAll(merchantOpeningHoursService.getByMerchantId(merchant.getMerchantId()));

            o_merchant.setMerchant(merchant);
            o_merchant.setOpeningHours(moh);
            l_merchant.add(o_merchant);
        }

        return l_merchant;
    }

    public ArrayList<MerchantResponseModel> getMerchantAndOpeningHoursByCatId(Integer catId){
        ArrayList<MerchantResponseModel> l_merchant = new ArrayList<>();

        for(Merchant merchant:getMerchantByCategoryId(catId)){
            MerchantResponseModel o_merchant = new MerchantResponseModel();
            ArrayList<MerchantOpeningHours> moh = new ArrayList<>();
            moh.addAll(merchantOpeningHoursService.getByMerchantId(merchant.getMerchantId()));

            o_merchant.setMerchant(merchant);
            o_merchant.setOpeningHours(moh);
            l_merchant.add(o_merchant);
        }

        return l_merchant;
    }



    public Iterable<Merchant> getAllMerchantById(Iterable<UUID> merchantId){
        return merchantRepo.findAllById(merchantId);
    }

    Optional<Merchant> getMerchantById(UUID merchantId){
        return merchantRepo.findById(merchantId);
    }

    public ArrayList<Merchant> getMerchantByMerchantName(String merchName){
        Iterable<Merchant> i_merchant = merchantRepo.findByMerchantNameIgnoreCaseContaining(merchName);
        ArrayList<Merchant> l_merchant = new ArrayList<>();
        if(i_merchant.iterator().hasNext()) {
            for (Merchant merchant : i_merchant) {
                l_merchant.add(merchant);
            }
        }
        return l_merchant;
    }

    public ArrayList<Merchant> getAllMerchantPromoByMerchantId(ArrayList<UUID> merchantId){
        ArrayList<Merchant> l_merchant = new ArrayList<>();
        Iterable<Merchant> i_merchant = merchantRepo.findMerchantPromoByMerchantId(merchantId);

        if(i_merchant.iterator().hasNext()) {
            for (Merchant merchant : i_merchant) {
                if ("".equals(merchant.getMerchantCentralId()) || merchant.getMerchantCentralId() == null){
                    l_merchant.add(merchant);
                }
                else {
                    l_merchant.add(merchantRepo.findById(UUID.fromString(merchant.getMerchantCentralId())).get());
                }
            }
        }

        //Distinct the merchant list
        Set<Merchant> s_merchant = new HashSet<>(l_merchant);
        l_merchant.clear();
        l_merchant.addAll(s_merchant);

        return l_merchant;
    }
}
