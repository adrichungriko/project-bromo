package com.bca.bit.bromo.mobile.entity.response;

import lombok.Data;

import java.util.List;

@Data
public class InquiryMutasiTransaksiResponse {
    private ErrorSchema error_schema;
    private InquiryMutasiTransaksiOutputSchema output_schema;

    @Data
    public static class ErrorSchema{
        private String error_code;
        private ErrorMessage error_message;

        @Data
        public static class ErrorMessage{
            private String english;
            private String indonesian;
        }
    }

    @Data
    public static class InquiryMutasiTransaksiOutputSchema{
        private String customer_id;
        private String fintech_id;
        private String fintech_name;
        private List<transaksi> payment_list;

        @Data
        public static class transaksi{
            private String trx_id;
            private String trx_date;
            private String desc;
            private String cashback;
            private String total_payment;
            private String top_up;
        }
    }
}
