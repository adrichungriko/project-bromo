package com.bca.bit.bromo.mobile.entity.jpa;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Data
@Table(name="ms_promo", schema = "app_bromo")
public class Promo {


    @Id
    @GeneratedValue(generator = "uuid")
    @Column(name="promo_id")
    private UUID promoId;

    @Column(name="promo_name")
    @NotNull
    private String promoName;

    @Column(name="start_date")
    @NotNull
    private Timestamp startDate;

    @Column(name="end_date")
    @NotNull
    private Timestamp endDate;

    @Column(name="description")
    @NotNull
    private String description;

    @Column(name="created_by")
    @NotNull
    private String createdBy;

    @Column(name="date_created")
    @NotNull
    private Timestamp dateCreated;

    @Column(name="modified_by")
    private String modifiedBy;

    @Column(name="date_modified")
    private Timestamp dateModified;

    @Column(name="is_active")
    @NotNull
    private Boolean isActive;

    @Column(name="picture")
    private String picture;

    @Column(name="promo_type")
    private String promoType;

    @Column(name="min_trx")
    private Double min_trx;

    @Column(name="max_cashback")
    private Double maxCashback;

    @Column(name="cashback_percent")
    private Double cashbackPercent;
    
    @Column(name="is_show_slider")
    private String isShowSlider;

    @Transient
    private Boolean liked;
}
