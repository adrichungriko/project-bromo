package com.bca.bit.bromo.mobile.entity.response;

import com.bca.bit.bromo.mobile.entity.jpa.TrxPromoUsed;
import com.bca.bit.bromo.utility.common.BaseResponse;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class TrxPromoUsedResponse extends BaseResponse {
    public TrxPromoUsedResponseOutputSchema output_schema;

    public TrxPromoUsedResponse(String merchantName, String merchantId, String transactionDate
                                , String transactionId, String promoName
                                ,String promoId,String customer_id, boolean verify, String errorCode){
        super(errorCode);
        output_schema = new TrxPromoUsedResponseOutputSchema(merchantName, merchantId, transactionDate, transactionId,promoName, promoId, customer_id, verify);
    }

    public TrxPromoUsedResponse(String errorCode){
        super(errorCode);
    }
    public TrxPromoUsedResponse(String errorCode, String message){
        super(errorCode, message);
    }
    public TrxPromoUsedResponse(){}

    @Data
    public static class TrxPromoUsedResponseOutputSchema{
        private String merchant_name;
        private String merchant_id;
        private String transaction_date;
        private String transaction_id;
        private String promo_name;
        private String promo_id;
        private String customer_id;
        private boolean verify;

        public TrxPromoUsedResponseOutputSchema(String merchant_name, String merchant_id, String transaction_date, String transaction_id, String promo_name, String promo_id, String customer_id, boolean verify) {
            this.merchant_name = merchant_name;
            this.merchant_id = merchant_id;
            this.transaction_date = transaction_date;
            this.transaction_id = transaction_id;
            this.promo_name = promo_name;
            this.promo_id = promo_id;
            this.customer_id = customer_id;
            this.verify = verify;
        }
    }
}
