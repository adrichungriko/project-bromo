package com.bca.bit.bromo.mobile.entity.response;

import java.util.List;

import com.bca.bit.bromo.mobile.entity.model.UserInterestResponseModel;
import com.bca.bit.bromo.utility.common.BaseResponse;

import lombok.Data;

@Data
public class InquiryUserInterestResponse extends BaseResponse{
	private InquiryUserInterestOutputSchema output_schema;
	
	public InquiryUserInterestResponse(String error_code) {
		super(error_code);
	}
	
	public InquiryUserInterestResponse(String error_code,List<UserInterestResponseModel> listInterest) {
		super(error_code);
		output_schema = new InquiryUserInterestOutputSchema(listInterest);
	}

	
	@Data
	private class InquiryUserInterestOutputSchema{
		private List<UserInterestResponseModel> list_user_interest;
		
		public InquiryUserInterestOutputSchema(List<UserInterestResponseModel> listInterest) {
			this.list_user_interest = listInterest;
		}

	}

	
}
