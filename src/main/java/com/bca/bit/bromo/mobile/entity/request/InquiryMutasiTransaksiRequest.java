package com.bca.bit.bromo.mobile.entity.request;

import lombok.Data;

@Data
public class InquiryMutasiTransaksiRequest {
    private String customer_id;
    private String fintech_name;
}
